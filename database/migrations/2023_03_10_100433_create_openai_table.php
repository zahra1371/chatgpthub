<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('openai', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable();
            $table->string('title');
            $table->text('description');
            $table->string('slug')->nullable();
            $table->boolean('active')->default(1);
            $table->text('questions')->nullable();
            $table->text('image')->nullable();
            $table->boolean('premium')->default(0);
            $table->string('type')->default('text');
            $table->text('prompt')->nullable();
            $table->boolean('custom_template')->default(0);
            $table->boolean('tone_of_voice')->default(0);
            $table->string('color')->nullable();
            $table->text('filters')->nullable();
            $table->text('package')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('openai');
    }
};
