@extends('layout.app')

@section('title')
    هوش من | تائید ورود
@endsection

@section('content')
    <div class="relative my-auto py-10">
        <div class="container px-3">
            <div class="-mx-3 flex justify-center">
                <div class="w-full px-3 xs:w-4/5 sm:w-3/5 md:w-1/2 lg:w-2/5 xl:w-1/3">
                    <div
                        class="w-full rounded-lg border border-slate-200 bg-white p-6 pt-5 dark:border-slate-800 dark:bg-slate-950">
                        <div class="mb-2">
                            <h3 class="mb-1 text-xl font-bold text-slate-700 dark:text-white"> {{__('Verify Mobile')}} </h3>
                            <p class="text-sm text-slate-500 dark:text-slate-400"> {{__('We sent a code to')}}
                                <strong>{{$mobile}}</strong> {{__('Sent')}}
                            </p>
                        </div>
                        <form action="{{route('verify')}}" method="post" id="verify-form">
                            <input type="hidden" name="code_time" value="{{$token_time}}">
                            @csrf
                            <div class="py-2">
                                <label for="passwordCR1"
                                       class="mb-2 inline-flex w-full cursor-pointer items-center justify-between text-sm font-bold text-slate-600 dark:text-slate-200"> {{__('Enter Code')}}
                                    <button type="button" id="resend-code"
                                            class="text-xs text-blue-500 hover:text-blue-700 hidden"
                                            href="">{{__('Resend Code')}}</button>
                                    <span id="timer" class="text-blue-500"><span id="second"></span> ثانیه تا ارسال مجدد</span>
                                </label>
                                <div class="js-auto-input-change flex gap-x-3" dir="ltr">
                                    <input name="digits[0]"
                                           class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-center text-sm text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 focus:dark:border-slate-800 disabled:dark:bg-slate-900"
                                           id="passwordCR1" type="text" inputmode="numeric" maxlength="1"/>
                                    <input name="digits[1]"
                                           class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-center text-sm text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 focus:dark:border-slate-800 disabled:dark:bg-slate-900"
                                           id="passwordCR2" type="text" inputmode="numeric" maxlength="1"/>
                                    <input name="digits[2]"
                                           class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-center text-sm text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 focus:dark:border-slate-800 disabled:dark:bg-slate-900"
                                           id="passwordCR3" type="text" inputmode="numeric" maxlength="1"/>
                                    <input name="digits[3]"
                                           class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-center text-sm text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 focus:dark:border-slate-800 disabled:dark:bg-slate-900"
                                           id="passwordCR4" type="text" inputmode="numeric" maxlength="1"/>
                                    <input name="digits[4]"
                                           class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-center text-sm text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 focus:dark:border-slate-800 disabled:dark:bg-slate-900"
                                           id="passwordCR5" type="text" inputmode="numeric" maxlength="1"/>
                                    <input name="digits[5]"
                                           class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-center text-sm text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 focus:dark:border-slate-800 disabled:dark:bg-slate-900"
                                           id="passwordCR6" type="text" inputmode="numeric" maxlength="1"/>
                                </div>
                                <span class="error error-digits text-red-600"></span>

                            </div>
                            <div class="pt-3">
                                <button type="button" id="verify-btn"
                                        class="inline-flex w-full items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800"> {{__('Confirm Code')}} </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div>
@endsection

@section('scripts')
    <script>
        $('#verify-btn').click(function (e) {
            e.preventDefault()
            registerForm($('#verify-form'),$(this))
        })

        let timer = function (date) {
            let timer = date;
            setInterval(function () {
                if (--timer < 0) {
                    timer = 0;
                    $('#resend-code').removeClass('hidden')
                    $('#timer').addClass('hidden')
                }
                $('#second').text(timer)
            }, 1000);
        }

        @if(!$token_time)
            $('#resend-code').removeClass('hidden')
            $('#timer').addClass('hidden')
        @else
            let second = 120 - $('input[name=code_time]').val();
            $('#timer').removeClass('hidden')
            timer(second)
        @endif

        $('#resend-code').click(function () {
            let formData = {
                "_token": "{{ csrf_token() }}",
                action: "{{route('resend-code')}}",
                method: "get"
            }
            ajax(formData)
        })
    </script>
@endsection
