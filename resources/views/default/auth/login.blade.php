@extends('layout.app')

@section('title')
    هوش من |  ورود
@endsection

@section('content')
    <div class="relative my-auto py-10">
        <div class="container px-3">
            <div class="-mx-3 flex justify-center">
                <div class="w-full px-3 xs:w-4/5 sm:w-3/5 md:w-1/2 lg:w-2/5 xl:w-1/3">
                    <div class="bg-orange-200 rounded-lg my-2">
                        <p class="text-orange-500 p-3">در صورتی که قبلا با ایمیل ثبت نام کرده اید، برای ورود با موبایل باید ایمیل خود را وارد کنید. در غیر این صورت ایمیل را خالی بگذارید.</p>
                    </div>
                    <div class="w-full rounded-lg border border-slate-200 bg-white p-6 pt-5 dark:border-slate-800 dark:bg-slate-950">
                        <div class="mb-2">
                            <h3 class="mb-1 text-xl font-bold text-slate-700 dark:text-white"> {{__('Sign in')}} </h3>
                        </div>
                        <form action="{{route('login')}}" method="post" id="login-form">
                            @csrf
                            <div class="py-2">
                                <label for="mobile" class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200"> {{__('Mobile')}} </label>
                                <div class="relative isolate flex w-full">
                                    <input class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500" placeholder="شماره موبایل شما، مثال : 0913111111" id="mobile" name="mobile" />
                                </div>
                                <span class="error error-mobile text-red-600"></span>
                            </div>
                            <div class="py-2">
                                <label for="email" class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200"> {{__('Email')}} </label>
                                <div class="relative isolate flex w-full">
                                    <input class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500" placeholder="ایمیل خود را وارد کنید." id="email" name="email" />
                                </div>
                                <span class="error error-email text-red-600"></span>
                            </div>
                            <div class="pt-3">
                                <button type="button" id="login-btn" class="inline-flex w-full items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                    {{__('Continue')}} </button>
                            </div>
                        </form>
                        <div class="text-center mb-4 mt-5">
                            <h6 class="text-center text-[11px] font-bold uppercase tracking-wider text-slate-400">
                                {{__("Don't have account yet?")}} </h6>
                            <a class="text-blue-600 text-sm" href="{{'register'}}">ثبت نام کنید</a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div>
@endsection

@section('scripts')
    <script>
        $('#login-btn').click(function (e) {
            e.preventDefault()
            loginForm($('#login-form'),$(this))
        })
    </script>
@endsection

