@extends('layout.app')

@section('title')
    هوش من |  {{__('AI Chat Bot')}}
@endsection

@section('content')
    <div class="relative px-1 pt-10 pb-5">
        <div class="container px-3">
            <div
                class="flex-grow-1 relative isolate flex max-h-[calc(100vh-theme(space.52))] min-h-[calc(100vh-theme(space.52))] w-full overflow-hidden rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950">
                <div id="convoAside"
                     class="peer absolute z-20 flex h-full w-72 -translate-x-full flex-col border-e border-slate-200 bg-white duration-300 dark:border-slate-800 dark:bg-slate-950 max-lg:transition-all lg:static lg:h-auto lg:w-1/4 lg:!translate-x-0 rtl:translate-x-full [&.active]:translate-x-0">
                    <div class="flex h-16 items-center border-b border-slate-200 px-6 py-4 dark:border-slate-800">
                        <h3 class="text-lg font-bold text-slate-700 dark:text-white"> {{__('Previous Chats')}} </h3>
                    </div>
                    <div
                        class="h-full max-h-full flex-grow overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-600 dark:scrollbar-track-slate-800">
                        <div class="grid grid-cols-1 gap-5">
                            @php $i=0 @endphp
                            @foreach($list as $item)
                                @php
                                    $message=count($item->messages)>0?count($item->messages)>1?$item->messages[1]->input:$item->messages[0]->output:'';
                                @endphp
                                <a href="#" onclick="return openChatAreaContainer({{$item->id}});">
                                    <div id="chat_{{$item->id}}"
                                         class="@if(++$i ===1) active @endif relative isolate flex cursor-pointer items-center before:absolute before:-inset-2 before:-z-10 before:rounded-md before:transition-all before:duration-300 before:content-[''] hover:before:bg-slate-50 hover:before:dark:bg-slate-800 [&.active]:before:bg-slate-100 [&.active]:before:dark:bg-slate-800">
                                        <img src="{{asset('images/chat.png')}}" height="40" width="30" alt="">
                                        <div class="ms-4">
                                            <h4 class="line-clamp-1 text-sm text-slate-600 dark:text-slate-200">
                                                {{$message}}
                                            </h4>
                                            <div
                                                class="mt-1 text-xs text-slate-500 dark:text-slate-400"> {{$item->updated_at->diffForHumans()}}</div>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="mt-auto px-6 pb-6 pt-4">
                        <a href="" onclick="return startNewChat({{$category->id}},'fa')"
                           class="inline-flex w-full items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                            <div class="h-4">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-full">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M2.25 12.76c0 1.6 1.123 2.994 2.707 3.227 1.068.157 2.148.279 3.238.364.466.037.893.281 1.153.671L12 21l2.652-3.978c.26-.39.687-.634 1.153-.67 1.09-.086 2.17-.208 3.238-.365 1.584-.233 2.707-1.626 2.707-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0 0 12 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018Z"></path>
                                </svg><!-- chat-bubble-bottom-center - outline - heroicons  -->
                            </div>
                            <span>{{__('New Conversation')}}</span>
                        </a>
                    </div>
                </div>
                <!-- chat-aside -->
                <div data-target="#convoAside"
                     class="class-toggle absolute inset-0 z-10 hidden bg-slate-950 bg-opacity-20 peer-[.active]:block lg:!hidden"></div>
                <!-- chat-aside-overlay -->
                <div class="z-0 flex w-full flex-col lg:w-3/4">
                    <div class="flex h-full flex-col justify-stretch">
                        <div
                            class="h-15 flex justify-between border-b border-slate-200 px-6 py-3 gap-2 dark:border-slate-800">
                            <div class="flex items-center">
                                <div
                                    class="inline-flex h-9 w-9 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-800">
                                    <img src="{{asset("images/bots/$category->image.png")}}" alt="General Bot"/>
                                </div>
                                <div class="ms-3">
                                    <h4 class="line-clamp-1 text-xs font-bold text-slate-600 dark:text-slate-200">
                                        {{__($category->name)}} </h4>
                                </div>
                            </div>
                            <div class="flex items-center">
                                <a href="javascript:void(0);" onclick="return startNewChat({{$category->id}},'fa')"
                                   class="class-toggle -my-1 inline-flex h-8 w-8 items-center justify-center overflow-hidden rounded-full text-slate-400 transition-all hover:bg-slate-200 hover:text-slate-600 lg:hidden [&.active]:bg-slate-200 [&.active]:text-slate-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                         viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                         stroke-linecap="round" stroke-linejoin="round">
                                        <path d="M12 5l0 14"></path>
                                        <path d="M5 12l14 0"></path>
                                    </svg>
                                </a>
                                <button
                                    class="class-toggle -my-1 inline-flex h-8 w-8 items-center justify-center overflow-hidden rounded-full text-slate-400 transition-all hover:bg-slate-200 hover:text-slate-600 lg:hidden [&.active]:bg-slate-200 [&.active]:text-slate-600">

                                    <svg stroke-width="1.5" class="size-5" xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor" fill="none"
                                         stroke-linecap="round" stroke-linejoin="round">
                                        <path d="M21 12a9 9 0 1 0 -9 9"></path>
                                        <path d="M3.6 9h16.8"></path>
                                        <path d="M3.6 15h8.4"></path>
                                        <path d="M11.578 3a17 17 0 0 0 0 18"></path>
                                        <path d="M12.5 3c1.719 2.755 2.5 5.876 2.5 9"></path>
                                        <path d="M18 14v7m-3 -3l3 3l3 -3"></path>
                                    </svg>
                                </button>
                                <button type="button" id="show_export_btns"
                                        class="class-toggle -my-1 inline-flex h-8 w-8 items-center justify-center overflow-hidden rounded-full text-slate-400 transition-all hover:bg-slate-200 hover:text-slate-600 lg:hidden [&.active]:bg-slate-200 [&.active]:text-slate-600">
                                    <svg stroke-width="1.5" class="size-6" xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor" fill="none"
                                         stroke-linecap="round" stroke-linejoin="round">
                                        <path
                                            d="M9 5h-2a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h3m9 -9v-5a2 2 0 0 0 -2 -2h-2"></path>
                                        <path
                                            d="M13 17v-1a1 1 0 0 1 1 -1h1m3 0h1a1 1 0 0 1 1 1v1m0 3v1a1 1 0 0 1 -1 1h-1m-3 0h-1a1 1 0 0 1 -1 -1v-1"></path>
                                        <path
                                            d="M9 3m0 2a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v0a2 2 0 0 1 -2 2h-2a2 2 0 0 1 -2 -2z"></path>
                                    </svg>
                                </button>
                                <button data-target="#convoAside"
                                        class="class-toggle -my-1 inline-flex h-8 w-8 items-center justify-center overflow-hidden rounded-full text-slate-400 transition-all hover:bg-slate-200 hover:text-slate-600 lg:hidden [&.active]:bg-slate-200 [&.active]:text-slate-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke-width="1.5" stroke="currentColor" class="h-5">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                              d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"/>
                                    </svg><!-- bars-3 - outline - heroicons  -->
                                </button>
                            </div>

                        </div>
                        <div
                            class="conversation-area flex-grow overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-600 dark:scrollbar-track-slate-800">
                            <div class="chats-container grid grid-cols-1 gap-4">
                                @foreach ($chat->messages as $message)
                                    @php
                                        $bot_image=$chat->category->image;
                                    @endphp
                                    @if($message->input !== null)
                                        <div class="flex-row-reverse flex items-end gap-2">
                                            <div
                                                class="inline-flex h-10 w-10 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-700">
                                                <img src="{{asset('images/nouser.webp')}}" alt="user image"/>
                                            </div>
                                            <div class="bg-blue-50 dark:bg-blue-950  max-w-md rounded-md px-4 py-3">
                                                <p class="text-sm text-slate-500 dark:text-slate-300">{{$message->input}}</p>
                                            </div>
                                        </div>
                                    @endif
                                    @if($message->output != null)
                                        <div class=" flex items-end gap-2">
                                            <div
                                                class="inline-flex h-10 w-10 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-700">
                                                <img src="{{asset("images/bots/$bot_image.png")}}"
                                                     alt="{{"$chat->$category->image robot image"}}"/>
                                            </div>
                                            <div class=" bg-slate-100 dark:bg-slate-900 max-w-md rounded-md px-4 py-3">
                                                <p class="bot-chat-content text-sm text-slate-500 dark:text-slate-300"> {!! $message->output !!} </p>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($message->outputImage != null && $message->outputImage != '')
                                        <div class="lqd-chat-image-bubble mb-2 content-end gap-2 lg:ms-auto">
                                            <div
                                                class="inline-flex h-10 w-10 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-700">
                                                <img src="{{asset("images/bots/$bot_image.png")}}"
                                                     alt="{{"$chat->$category->image robot image"}}"/>
                                            </div>
                                            <div class="mb-2 flex w-4/5 justify-start rounded-3xl text-heading-foreground dark:text-heading-foreground md:w-1/2">
                                                <a
                                                    data-fslightbox="gallery"
                                                    data-type="image"
                                                    href="{{asset($message->outputImage)}}"
                                                >
                                                    <img
                                                        class="img-content rounded-2xl"
                                                        loading="lazy"
                                                        src="{{asset($message->outputImage)}}"
                                                    />
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card -->
        </div>
        <!-- container -->
    </div>
    <div class="px-5">
        <form action="">
            @csrf
            <input id="category_id" type="hidden" value="{{ $category->id }}"/>
            <input id="chat_id" type="hidden" value="{{ isset($chat) ? $chat->id : null }}"/>
            <input id="chatbot_id" type="hidden" value="{{ $category->chatbot_id }}"/>
            @if ($category->prompt_prefix != null)
                <input
                    id="prompt_prefix"
                    type="hidden"
                    value="{{ $category->prompt_prefix }} you will now play a character and respond as that character (You will never break character). Your name is {{ $category->human_name }} but do not introduce by yourself as well as greetings."
                >
            @else
                <input
                    id="prompt_prefix"
                    type="hidden"
                    value=""
                >
            @endif
            <div class="flex items-center gap-2">
                <button
                    class="inline-flex items-center justify-center rounded-md bg-blue-600 p-1 text-white transition-all hover:bg-blue-800"
                    id="send_message_button">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor" class="h-7">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M6 12 3.269 3.125A59.769 59.769 0 0 1 21.485 12 59.768 59.768 0 0 1 3.27 20.875L5.999 12Zm0 0h7.5"/>
                    </svg><!-- paper-airplane - outline - heroicons  -->
                </button>
                <button
                    class="inline-flex items-center justify-center rounded-md bg-blue-600 p-1 text-white transition-all hover:bg-blue-800 hidden"
                    id="stop_button">
                    <svg stroke-width="1.5" class="size-6 h-6" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 24 24" stroke="currentColor" fill="none" stroke-linecap="round"
                         stroke-linejoin="round">
                        <path d="M8 13v-7.5a1.5 1.5 0 0 1 3 0v6.5"></path>
                        <path d="M11 5.5v-2a1.5 1.5 0 1 1 3 0v8.5"></path>
                        <path d="M14 5.5a1.5 1.5 0 0 1 3 0v6.5"></path>
                        <path
                            d="M17 7.5a1.5 1.5 0 0 1 3 0v8.5a6 6 0 0 1 -6 6h-2h.208a6 6 0 0 1 -5.012 -2.7a69.74 69.74 0 0 1 -.196 -.3c-.312 -.479 -1.407 -2.388 -3.286 -5.728a1.5 1.5 0 0 1 .536 -2.022a1.867 1.867 0 0 1 2.28 .28l1.47 1.47"></path>
                    </svg>
                </button>
                <textarea
                    class="z-10 h-12 w-full rounded-md border-slate-200 bg-white text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                    placeholder="سوال خود را اینجا بنویسید" id="prompt"></textarea>
{{--                <button--}}
{{--                    class="inline-flex items-center justify-center rounded-md bg-blue-600 py-2 px-1 text-white transition-all hover:bg-blue-800">--}}
{{--                    <svg stroke-width="1.5" class="size-6" xmlns="http://www.w3.org/2000/svg" width="16" height="16"--}}
{{--                         viewBox="0 0 24 24" stroke="currentColor" fill="none" stroke-linecap="round"--}}
{{--                         stroke-linejoin="round">--}}
{{--                        <path d="M9 2m0 3a3 3 0 0 1 3 -3h0a3 3 0 0 1 3 3v5a3 3 0 0 1 -3 3h0a3 3 0 0 1 -3 -3z"></path>--}}
{{--                        <path d="M5 10a7 7 0 0 0 14 0"></path>--}}
{{--                        <path d="M8 21l8 0"></path>--}}
{{--                        <path d="M12 17l0 4"></path>--}}
{{--                    </svg>--}}
{{--                </button>--}}
{{--                <button--}}
{{--                    class="inline-flex items-center justify-center rounded-md bg-blue-600 py-2 px-1 text-white transition-all hover:bg-blue-800">--}}
{{--                    <svg stroke-width="1.5" class="size-5" xmlns="http://www.w3.org/2000/svg" width="16" height="16"--}}
{{--                         viewBox="0 0 24 24" stroke="currentColor" fill="none" stroke-linecap="round"--}}
{{--                         stroke-linejoin="round">--}}
{{--                        <path--}}
{{--                            d="M15 7l-6.5 6.5a1.5 1.5 0 0 0 3 3l6.5 -6.5a3 3 0 0 0 -6 -6l-6.5 6.5a4.5 4.5 0 0 0 9 9l6.5 -6.5"></path>--}}
{{--                    </svg>--}}
{{--                </button>--}}
            </div>
        </form>
    </div>

    <template id="chat_user_bubble">
        <div class="flex-row-reverse flex items-end gap-2">
            <div
                class="inline-flex h-10 w-10 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-700">
                <img src="{{asset('images/nouser.webp')}}" alt="User Avatar"/>
            </div>
            <div class="bg-blue-50 dark:bg-blue-950  max-w-md rounded-md px-4 py-3">
                <p class="chat-content text-sm text-slate-500 dark:text-slate-300"> Hi, chatbot. What&#39;s your
                    name? </p>
            </div>
        </div>
    </template>

    <template id="chat_ai_bubble">
        <div class=" flex items-end gap-2">
            <div
                class="inline-flex h-10 w-10 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-700">
                <img id="bot_image" src="" alt=""/>
            </div>
            <div class=" bg-slate-100 dark:bg-slate-900 max-w-md rounded-md px-4 py-3">
                <p class="chat-content text-sm text-slate-500 dark:text-slate-300"></p>
            </div>
        </div>
    </template>

    <template id="chat_bot_image_bubble">
        <div class="lqd-chat-image-bubble mb-2 flex content-end gap-2 lg:ms-auto">
            <div class="mb-2 flex w-4/5 justify-start rounded-3xl text-heading-foreground dark:text-heading-foreground md:w-1/2">
                <a
                    data-fslightbox="gallery"
                    data-type="image"
                    href="#"
                >
                    <img
                        class="img-content rounded-2xl"
                        loading="lazy"
                    />
                </a>
            </div>
        </div>
    </template>
@endsection

@section('scripts')
    <script>
        var chatid = @json($list)[0]?.id;
        const stream_type = 'backend';
        const openai_model = 'gpt-4';
        const category = @json($category);
        const prompt_prefix = document.getElementById("prompt_prefix").value;

        var messages = [];
        var training = [];

        @if ($chat_completions != null)
            training = @json($chat_completions);
        @endif

        messages.push({
            role: "assistant",
            content: prompt_prefix
        });

        @if ($lastThreeMessage != null)
            @foreach ($lastThreeMessage as $entry)
            message = {
            role: "user",
            content: @json($entry->input)
        };
        messages.push(message);
        message = {
            role: "assistant",
            content: @json($entry->output)
        };
        messages.push(message);
        @endforeach
        @endif
    </script>
    <script>
        $('#send_message_button').click(function () {
            if ({{auth()->user()->remaining_words <=0 && auth()->user()->remaining_words !=-1}}) {
                toastr.error('تعداد کلمات شما صفر میباشد، لطفا اشتراک تهیه کنیnnnnد')
                return false
            }
        })
    </script>
    @if(auth()->user()->remaining_words <=0 && auth()->user()->remaining_words !=-1)
        <script>
            toastr.error('تعداد کلمات شما صفر میباشد، لطفا اشتراک تهیه کنید')
        </script>
    @else
        <script src="{{ asset('assets/js/openai_chat.js') }}"></script>
    @endif
@endsection
