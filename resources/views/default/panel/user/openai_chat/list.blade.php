@extends('layout.app')

@section('title')
    هوش من |  {{__('AI Chat Bot')}}
@endsection

@section('content')
    <div class="relative px-3 pt-10 pb-5">
        <div class="container px-3">
            <div
                class="flex-grow-1 relative isolate flex max-h-[calc(100vh-theme(space.52))] min-h-[calc(100vh-theme(space.52))] w-full overflow-hidden rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950">
                <!-- chat-aside-overlay -->
                <div class="z-0 flex w-full flex-col">
                    <div
                        class="flex h-16 items-center justify-between border-slate-200 px-6 py-4 dark:border-slate-800">
                        <h3 class="text-lg font-bold text-slate-700 dark:text-white"> {{__('Choose a bot')}} </h3>
                    </div>
                    <div
                        class="h-full max-h-full flex-grow overflow-auto p-6">
                        <div class="grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4">
                            @foreach($aiList as $list)
                                <a href="{{route('dashboard.user.openai.chat.chat',$list->slug)}}"
                                   class="cursor-pointer rounded-lg border border-slate-200 px-4 py-6 text-center transition-all duration-300 hover:bg-slate-100 dark:border-slate-800 hover:dark:bg-slate-900">
                                    <div class="inline-flex h-14 w-14 flex-shrink-0 overflow-hidden rounded-full">
                                        <img src="{{asset("images/bots/$list->image.png")}}" alt=""/>
                                    </div>
                                    <div class="mt-2">
                                       <div class="mt-1 text-xs text-slate-500 dark:text-slate-400"> {{__("$list->description")}}</div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- card -->
        </div>
        <!-- container -->
    </div>
@endsection
