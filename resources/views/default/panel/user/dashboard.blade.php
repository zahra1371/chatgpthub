@extends('layout.app')

@section('title')
    هوش من |  داشبورد
@endsection

@section('content')
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="-mx-3 mb-7 flex items-center justify-between">
                <div class="px-3">
                    <ul class="inline-flex items-center gap-2 text-xs font-medium text-slate-500 dark:text-slate-300">
                        <li>
                            <h2 class="mb-2 text-xl font-bold text-slate-700 dark:text-white">
                                <a href="{{route('dashboard.user.index')}}" class="text-blue-500 hover:text-blue-700">
                                    داشبورد </a>
                            </h2>
                        </li>
                    </ul>
                </div>
                <div class="px-3">
                    <a href="{{route('dashboard.user.openai.list')}}"
                       class="inline-flex items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                        {{__('ALL Writers List')}} </a>
                </div>
            </div>
            <!-- Page Head -->
            <div class="-m-3 flex flex-wrap">
                <div class="w-full p-3 xs:w-1/2 lg:w-1/2">
                    <div class="h-full rounded-md border border-slate-800 bg-slate-800 p-5">
                        <div class="relative isolate flex h-full flex-col">
                            <div class="absolute end-0 top-0"></div>
                            <div class="mb-auto">
                                <div
                                        class="mt-2 text-sm font-bold text-white"> {{auth()->user()->name}} {{__('Dear')}}</div>
                                <h6 class="w-max bg-gradient-to-r from-blue-300 to-pink-500 bg-clip-text text-xl font-bold text-transparent">
                                    {{__('Welcome')}} </h6>
                            </div>
                            <div class="mt-4 flex gap-x-6">
                                <div>
                                    <div class="mt-1 text-xs text-slate-300">{{__('Remaining Words')}}</div>
                                    <div
                                            class="mt-1 text-base font-bold text-slate-100"> {{Auth::user()->remaining_words == -1?'بدون محدودیت':number_format((int) Auth::user()->remaining_words)}}</div>
                                </div>
                                <div>
                                    <div class="mt-1 text-xs text-slate-300">{{__('Remaining Images')}}</div>
                                    <div
                                            class="mt-1 text-base font-bold text-slate-100">{{Auth::user()->remaining_images == -1?'بدون محدودیت' :number_format((int) Auth::user()->remaining_images) }}</div>
                                </div>
                                <div>
                                    <div class="mt-1 text-xs text-slate-300">{{__('Remaining Product Back')}}</div>
                                    <div
                                            class="mt-1 text-base font-bold text-slate-100">{{ number_format((int) Auth::user()->remaining_product_back) }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- card -->
                </div>
                <!-- col -->
                <div class="w-full p-3 xs:w-1/2 lg:w-1/2">
                    <div
                            class="h-full rounded-md border border-slate-200 bg-white p-5 dark:border-slate-800 dark:bg-slate-950">
                        <div class="relative isolate flex flex-col">
                            <div class="absolute top-0">
                                @if (Auth::user()->activePlan() != null)
                                    <div class="flex gap-3">
                                        <small class="py-0.5">طرح فعال شما:</small>
                                        <div
                                                class="rounded bg-gradient-to-r from-blue-600 to-pink-500 px-2 py-0.5 text-[11px] font-medium text-white">
                                            {{ __('plan') }} {{ getSubscriptionName() }}
                                        </div>
                                        <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.payment.subscription')) }}"
                                           class="inline-flex items-center justify-center gap-3 rounded-md bg-blue-600 px-3 py-0.5 text-[11px] font-medium text-white transition-all hover:bg-blue-800">ارتقا
                                            طرح</a>
                                    </div>

                                @endif
                            </div>

                            <div class="ms-1 mt-4">
                                @if (Auth::user()->activePlan() != null)
                                    <div class="mt-5 flex gap-x-6">
                                        <div>
                                            <div class="mt-1 text-xs text-slate-500">{{__('Word Tokens Used')}}</div>
                                            <div
                                                    class="mt-1 text-base font-bold text-slate-500"> {{number_format(Auth::user()->activePlan()->total_words - Auth::user()->remaining_words)}}</div>
                                        </div>
                                        <div>
                                            <div class="mt-1 text-xs text-slate-500">{{__('Images Generated')}}</div>
                                            <div
                                                    class="mt-1 text-base font-bold text-slate-500">{{ number_format(Auth::user()->activePlan()->total_images - Auth::user()->remaining_images) }}</div>
                                        </div>
                                    </div>
                                @else
                                    <p class="-ms-0.5 flex items-baseline gap-x-2 mt-5">
                                        {{ __('You have no subscription at the moment. Please select a subscription plan or a token pack.') }}
                                    </p>
                                    <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.payment.subscription')) }}"
                                       class="inline-flex items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-1 text-sm font-medium text-white transition-all hover:bg-blue-800 mt-1">خرید
                                        طرح</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- card -->
                </div>
                <!-- col -->
                <div class="w-full p-3 xs:w-1/2 lg:w-1/3">
                    <div
                            class="h-full rounded-md border border-slate-200 bg-white p-5 dark:border-slate-800 dark:bg-slate-950">
                        <div class="relative isolate flex flex-col">
                            <div class="absolute end-0 top-0"></div>
                            <div class="relative -z-10 -mb-8 h-16 opacity-30">
                                <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="h-full">
                                    <path class="fill-blue-300"
                                          d="M3 5C3 3.34315 4.34315 2 6 2H15.7574C16.553 2 17.3161 2.31607 17.8787 2.87868L20.1213 5.12132C20.6839 5.68393 21 6.44699 21 7.24264V19C21 20.6569 19.6569 22 18 22H6C4.34315 22 3 20.6569 3 19V5Z"/>
                                    <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M7 11C7 10.4477 7.44772 10 8 10H16C16.5523 10 17 10.4477 17 11C17 11.5523 16.5523 12 16 12H8C7.44772 12 7 11.5523 7 11Z"/>
                                    <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M7 15C7 14.4477 7.44772 14 8 14H12C12.5523 14 13 14.4477 13 15C13 15.5523 12.5523 16 12 16H8C7.44772 16 7 15.5523 7 15Z"/>
                                    <path class="fill-blue-600"
                                          d="M17.7071 2.70711L20.2929 5.29289C20.7456 5.74565 21 6.35971 21 7H18C16.8954 7 16 6.10457 16 5V2C16.6403 2 17.2544 2.25435 17.7071 2.70711Z"/>
                                </svg>
                            </div>
                            <div class="ms-1 mt-2">
                                <p class="-ms-0.5 flex items-baseline gap-x-2">
                                    <span class="text-4xl font-bold tracking-tight text-blue-500">ربات</span>
                                    <span
                                            class="text-sm font-semibold leading-6 tracking-wide text-slate-500 dark:text-slate-400">چت هوش من</span>
                                </p>
                                <a href="{{route('dashboard.user.openai.chat.chat','ai-chat-bot')}}"
                                   class="text-sm font-medium text-blue-600 hover:text-blue-800">من به همه سوالاتت جواب
                                    میدم!</a>
                            </div>
                        </div>
                    </div>
                    <!-- card -->
                </div>
                <!-- col -->
                <div class="w-full p-3 xs:w-1/2 lg:w-1/3">
                    <div
                            class="h-full rounded-md border border-slate-200 bg-white p-5 dark:border-slate-800 dark:bg-slate-950">
                        <div class="relative isolate flex flex-col">
                            <div class="absolute end-0 top-0"></div>
                            <div class="relative -z-10 -mb-8 h-16 opacity-30">
                                <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                                     class="h-full">
                                    <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M19.7866 14C20.9581 14 22 13.1714 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C13.1714 22 14 20.9581 14 19.7866V18C14 15.7909 15.7909 14 18 14H19.7866Z"/>
                                    <circle class="fill-blue-600" cx="11.5" cy="6" r="1.5"/>
                                    <circle class="fill-blue-600" cx="17.5" cy="9.5" r="1.5"/>
                                    <circle class="fill-blue-600" cx="6.5" cy="10" r="1.5"/>
                                    <circle class="fill-blue-600" cx="8" cy="16.5" r="1.5"/>
                                </svg>
                            </div>
                            <div class="ms-1 mt-2">
                                <p class="-ms-0.5 flex items-baseline gap-x-2">
                                    <span class="text-4xl font-bold tracking-tight text-blue-500">ربات</span>
                                    <span
                                            class="text-sm font-semibold leading-6 tracking-wide text-slate-500 dark:text-slate-400">عکس ساز هوش من</span>
                                </p>

                                <a href="{{route('dashboard.user.openai.generator.workbook','graphics')}}"
                                   class="text-sm font-medium text-blue-600 hover:text-blue-800">من ایده هات رو به عکس
                                    تبدیل میکنم!</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3 xs:w-1/2 lg:w-1/3">
                    <div
                            class="h-full rounded-md border border-slate-200 bg-white p-5 dark:border-slate-800 dark:bg-slate-950">
                        <div class="relative isolate flex flex-col">
                            <div class="absolute end-0 top-0"></div>
                            <div class="relative -z-10 -mb-8 h-16 opacity-30">
                                <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="h-full">
                                    <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M13 5H16V7H13V5ZM18 7V5H19C19.5523 5 20 5.44771 20 6V7H18ZM13 9H17H20V15H17H13V9ZM16 17H13V19H16V17ZM18 19V17H20V18C20 18.5523 19.5523 19 19 19H18ZM11 5H8V7H11V5ZM11 9H7H4V15H7H11V9ZM11 17H8V19H11V17ZM12 21H19C20.6569 21 22 19.6569 22 18V16V8V6C22 4.34315 20.6569 3 19 3H5C3.34315 3 2 4.34315 2 6V8V16V18C2 19.6569 3.34315 21 5 21H12ZM6 5H5C4.44772 5 4 5.44772 4 6V7H6V5ZM4 18V17H6V19H5C4.44772 19 4 18.5523 4 18Z"></path>
                                    <rect class="fill-blue-600" x="4" y="9" width="7" height="6"></rect>
                                    <rect class="fill-blue-600" x="13" y="9" width="7" height="6"></rect>
                                </svg>
                            </div>
                            <div class="ms-1 mt-2">
                                <p class="-ms-0.5 flex items-baseline gap-x-2">
                                    <span class="text-4xl font-bold tracking-tight text-blue-500">ربات</span>
                                    <span
                                            class="text-sm font-semibold leading-6 tracking-wide text-slate-500 dark:text-slate-400">ویدئو ساز هوش من</span>
                                </p>

                                <a href="{{route('dashboard.user.openai.generator.workbook','graphics')}}"
                                   class="text-sm font-medium text-blue-600 hover:text-blue-800">من عکس هات رو به ویدئو
                                    تبدیل میکنم!</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3">
                    <div
                            class="h-full rounded-md border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950">
                        <div class="relative isolate flex flex-col">
                            <div class="flex items-center justify-between gap-x-4 p-5">
                                <h6 class="text-md font-bold text-slate-700 dark:text-white"> {{__('Recent Documents')}} </h6>
                                <a href="{{route('dashboard.user.openai.documents.all')}}"
                                   class="text-sm font-medium text-blue-600 hover:text-blue-800">{{__('View All')}}</a>
                            </div>
                            <div
                                    class="overflow-x-auto scrollbar-thin scrollbar-track-slate-200 scrollbar-thumb-slate-600 dark:scrollbar-track-slate-800">
                                <table
                                        class="w-full table-auto border-collapse border-t border-slate-200 text-sm dark:border-slate-800">
                                    <thead class="text-slate-600 dark:text-slate-200">
                                    <tr>
                                        <th class="px-5 py-2 text-start">{{__('Document')}}</th>
                                        <th class="px-5 py-2 text-start">{{__('Created')}}</th>
                                        <th class="sticky end-0 bg-white dark:bg-slate-950"></th>
                                    </tr>
                                    @foreach ($documents as $entry)
                                        @php
                                            $url='images/'.$entry->generator->filters.'.png'
                                        @endphp
                                        @if ($entry->generator != null)
                                            <tr>
                                                <td class="border-t border-slate-200 px-5 py-3 dark:border-slate-800">
                                                    <div class="flex items-center">
                                                            <span class="block h-6 w-6">
                                                                <img src="{{asset($url)}}" alt="">
                                                            </span>
                                                        <div class="ms-3">
                                                            <span
                                                                    class="block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> {{ __($entry->generator->title) }} </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="border-t border-slate-200 px-5 py-3 dark:border-slate-800">
                                                    <span
                                                            class="block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200">{{jdate_from_gregorian($entry->created_at)}}</span>
                                                    <span
                                                            class="block text-[11px] font-medium text-slate-500 dark:text-slate-400">{{jdate_from_gregorian($entry->created_at,'H:i:s')}}</span>
                                                </td>
                                                <td class="sticky end-0 border-t border-slate-200 bg-white px-5 py-3 dark:border-slate-800 dark:bg-slate-950">
                                                    <ul class="flex justify-end gap-2">
                                                        <li>
                                                            <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.openai.documents.single', $entry->slug)) }}"
                                                               class="inline-flex items-center justify-center rounded-full bg-slate-200 p-2 text-slate-600 transition-all hover:bg-blue-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-blue-600 hover:dark:text-white">
                                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                                     viewBox="0 0 24 24" stroke-width="1.5"
                                                                     stroke="currentColor" class="h-3 w-3">
                                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                                          d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z"/>
                                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                                          d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"/>
                                                                </svg><!-- eye - outline - heroicons  -->
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <!-- table -->
                            </div>
                        </div>
                    </div>
                    <!-- card -->
                </div>
                <!-- col -->
            </div>
        </div>
        <!-- container -->
    </div>
@endsection
