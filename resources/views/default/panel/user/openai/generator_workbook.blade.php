@php
    $voice_tones = ['Professional', 'Funny', 'Casual', 'Excited', 'Witty', 'Sarcastic', 'Feminine', 'Masculine', 'Bold', 'Dramatic', 'Grumpy', 'Secretive'];
    $creativity_levels = [
        '0.25' => 'Economic',
        '0.5' => 'Average',
        '0.75' => 'Good',
        '1' => 'Premium'];
@endphp

@extends('layout.app')

@section('title')
    هوش من |  {{__('ALL Writers List')}}
@endsection

@section('content')
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="flex flex-wrap items-start gap-8 xl:flex-nowrap">
                <div
                    class="w-full rounded-lg border border-slate-200 bg-white px-7 py-6 dark:border-slate-800 dark:bg-slate-950 xl:w-96">
                    <ul class="tab-nav -mx-4 flex flex-wrap xl:mx-0">
                        @php $i=0 @endphp
                        @foreach($templates as $template)
                            <li class="tab-item w-full px-4 xs:w-1/2 xs:py-1 sm:w-1/3 lg:w-1/4 xl:w-full xl:px-0 xl:py-0">
                                <a href="#" data-target="#{{$template->slug}}" data-slug="{{$template->slug}}"
                                   data-title="{{__($template->title)}}" data-id="{{$template->id}}"
                                   data-filter="{{$template->filters}}"
                                   data-type="{{$template->type}}"
                                   class="{{$i==0?'active':''}} template-slug tab-toggle relative isolate flex text-slate-500 before:content-[''] dark:text-slate-400 [&.active]:text-blue-600 [&.active]:before:absolute [&.active]:before:-inset-x-3 [&.active]:before:inset-y-0 [&.active]:before:-z-10 [&.active]:before:rounded-md [&.active]:before:bg-blue-100 [&.active]:before:dark:bg-blue-950">
                                    <div class="flex items-center py-2">
                                        <div class="me-3 h-5">
                                            <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                                                 class="h-full">
                                                <path class="fill-blue-300"
                                                      d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                                                <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                                      d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                                                <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                                      d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                                            </svg>
                                        </div>
                                        <span class="text-sm font-medium"> {{__($template->title)}} </span>
                                    </div>
                                </a>
                            </li>
                            @php $i++ @endphp
                        @endforeach
                    </ul>
                </div>
                <div class="tab-content flex-grow-1 w-full">
                    @if($first_template->filters === 'instagram')
                        @include('panel.user.openai.instagram')
                    @elseif($first_template->filters === 'youtube')
                        @include('panel.user.openai.youtube')
                    @elseif($first_template->filters === 'website')
                        @include('panel.user.openai.website')
                    @elseif($first_template->filters === 'general')
                        @include('panel.user.openai.general')
                    @elseif($first_template->filters === 'voiceover')
                        @include('panel.user.openai.voiceover')
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/js/functions/openai_generator_workbook.js')}}"></script>
    <script src="{{ asset('assets/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/js/tinymce-theme-handler.js') }}"></script>

    <script>
        let openai_id
        let filter
        let type
        $(document).ready(function () {
            filter = '{!! __($first_template->filters) !!}';
            openai_id = '{!! __($first_template->id) !!}';
            type = '{!! __($first_template->type) !!}';

            $('.template-slug').click(function () {
                filter = $(this).data('filter')
                openai_id = $(this).data('id')
                type = $(this).data('type')
            })
        })

        function sendOpenaiGeneratorForm(elm, ev) {
            @if(auth()->user()->remaining_words <=0 && auth()->user()->remaining_words !=-1)
            toastr.error('تعداد کلمات شما صفر میباشد، لطفا اشتراک تهیه کنید.')
            return false
            @endif
            ev?.preventDefault();
            ev?.stopPropagation();
            let post_type = $(elm).data('slug')
            if (tinymce.get(`${post_type}-editor`))
                tinymce.get(`${post_type}-editor`).hide()
            $('#content-area').removeClass('hidden')
            $('.content_text').text('در حال آماده سازی ...')
            $('.generator_button').text('لطفا منتظر بمون...')
            $('.generator_button').attr('disabled', true)


            let formData = new FormData();

            formData.append('post_type', post_type);
            formData.append('openai_id', openai_id);

            if (type === 'text' || type === 'rss' || type === 'youtube') {
                formData.append('title', $(`#${post_type} form textarea[name=title]`).val());
                formData.append('text_to_summary', $(`#${post_type} form textarea[name=text_to_summary]`).val());
                formData.append('description', $(`#${post_type} form textarea[name=description]`).val());
                formData.append('maximum_length', $(`#${post_type} form input[name=maximum_length]`).val());
                formData.append('number_of_results', $(`#${post_type} form input[name=number_of_results]`).val());
                formData.append('creativity', $(`#${post_type} form select[name=creativity]`).val());
                formData.append('tone_of_voice', $(`#${post_type} form select[name=tone_of_voice]`).val());
                formData.append('language', $(`#${post_type} form select[name=language]`).val());
            }

            if (type === 'youtube') {
                formData.append('youtube_action', $(`#${post_type} form select[name=youtube_action]`).val());
                formData.append('url', $(`#${post_type} form input[name=url]`).val());
            }

            if (type === 'audio')
                formData.append('file', $('#file').prop('files')[0]);

            if (filter === 'instagram') {
                formData.append('target', $(`#${post_type} form select[name=target]`).val())
                formData.append('duration', $(`#${post_type} form select[name=duration]`).val())
                formData.append('follower_target', $(`#${post_type} form input[name=follower_target]`).val())
                formData.append('style', $(`#${post_type} form select[name=style]`).val())
                formData.append('main_goal', $(`#${post_type} form select[name=main_goal]`).val())
                formData.append('cta', $(`#${post_type} form select[name=cta]`).val())
                formData.append('brand_name', $(`#${post_type} form input[name=brand_name]`).val())
                formData.append('campagin_duration', $(`#${post_type} form input[name=campagin_duration]`).val())
                formData.append('player', $(`#${post_type} form input[name=player]`).val())
                formData.append('gender_target', $(`#${post_type} form select[name=gender_target]`).val())
                formData.append('details', $(`#${post_type} form textarea[name=details]`).val());
                formData.append('details', $(`#${post_type} form textarea[name=reel_details]`).val());
            }

            $.ajax({
                type: "post",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                },
                url: "/dashboard/user/openai/generate",
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (post_type === 'ai_speech_to_text') {
                        toastr.success(`متن فایل صوتی آماده شد.`);
                        $('.generator_button').text('بزن بریم')
                        $('.generator_button').attr('disabled', false)
                        $('#speech-to-text tbody').prepend(`<tr class="border-b">
                            <td>
                                <img src="https://cdn-icons-png.flaticon.com/128/13063/13063633.png" alt="" class="w-9">
                            </td>
                            <td class="py-3">
                                <p class="px-6">${data.userOpenai.output}</p>
                            </td>
                        </tr>`)
                    } else {
                        const message_no = data.data.message_id;
                        const creativity = data.data.creativity;
                        const maximum_length = parseInt(data.data.maximum_length);
                        const prompt = data.data.inputPrompt;
                        return generate(message_no, creativity, maximum_length, prompt, post_type);
                    }
                },
                error: function (data) {
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (index, value) {
                            toastr.error(value);
                        });
                    } else if (data.responseJSON.message) {
                        toastr.error(data.responseJSON.message);
                    }
                    $('.generator_button').text('بزن بریم');
                    $('.generator_button').attr('disabled', false)
                    $('#content-area').removeClass('hidden')
                }
            });
        }
    </script>
@endsection

