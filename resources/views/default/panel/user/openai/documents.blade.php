@extends('layout.app')

@section('title')
    هوش من |  {{__('Documents')}}
@endsection
@section('content')
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="-mx-3 mb-7 flex items-center justify-between">
                <div class="px-3">
                    <h2 class="mb-2 text-xl font-bold text-slate-700 dark:text-white"> {{__('My Documents')}} </h2>
                </div>
            </div>
            <!-- head -->
            <div class="rounded-md border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950">
                <div
                    class="px-6 overflow-x-auto scrollbar-thin scrollbar-track-slate-200 scrollbar-thumb-slate-600 dark:scrollbar-track-slate-800">
                    <table
                        class="w-full table-auto border-collapse border-b border-t border-slate-200 text-sm dark:border-slate-800">
                        <thead class="text-slate-600 dark:text-slate-200">
                        <tr>
                            <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950">{{__('File')}} </th>
                            <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950">
                                {{__('Words Used')}} </th>
                            <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950">
                                {{__('Date')}} </th>
                            <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950">{{__('Action')}}</th>
                        </tr>
                        </thead>
                        <tbody id="documents-body">
                        @foreach($items as $item)
                            @if($item->generator->filters !== 'graphics')
                                @php
                                    $url='images/'.$item->generator->filters.'.png'
                                @endphp
                                <tr>
                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                        <div class="flex w-60 items-center sm:w-auto">
                                                <span class="block h-6 w-6">
                                                    <img src="{{asset($url)}}" alt="">
                                                </span>
                                            <div class="ms-3">
                                                <div
                                                    class="line-clamp-2 text-xs font-bold text-slate-600 dark:text-slate-200">{{\Illuminate\Support\Str::limit($item->output,20)}}</div>
                                                <div
                                                    class="block text-[11px] font-medium text-slate-500 dark:text-slate-400">
                                                    {{__($item->generator->title)}} </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                            <span class="text-xs text-slate-500 dark:text-slate-400">
                                                <strong>{{$item->credits}} </strong> کلمه </span>
                                    </td>
                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                    <span
                                        class="block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> {{jdate_from_gregorian($item->created_at)}} </span>
                                        <span class="block text-[11px] font-medium text-slate-500 dark:text-slate-400"> {{jdate_from_gregorian($item->created_at,'H:i:s')}} </span>
                                    </td>
                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                        <ul class="flex justify-end gap-2">
                                            <li>
                                                <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.openai.documents.single', $item->slug)) }}"
                                                   title="مشاهده" class="inline-flex items-center justify-center rounded-full bg-slate-200 p-2 text-slate-600 transition-all hover:bg-blue-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-blue-600 hover:dark:text-white">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                         stroke-width="1.5" stroke="currentColor" class="h-3 w-3">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z"/>
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"/>
                                                    </svg><!-- eye - outline - heroicons  -->
                                                </a>
                                            </li>
                                            <li>
                                                <button title="حذف" type="button" href="" data-slug="{{$item->slug}}" onclick="deleteDocument(this)"
                                                   class="inline-flex items-center justify-center rounded-full bg-rose-100 p-2 text-rose-600 transition-all hover:bg-rose-600 hover:text-white dark:bg-rose-950 dark:text-rose-200 hover:dark:bg-rose-600 hover:dark:text-white">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                         stroke-width="1.5" stroke="currentColor" class="h-3 w-3">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"/>
                                                    </svg><!-- trash - outline - heroicons  -->
                                                </button>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                    <div class="my-4" id="pagination-links">
                        {!! $items->links('pagination::tailwind') !!}
                    </div>
                </div>
                <!-- body -->
            </div>
            <!-- card -->
        </div><!-- container -->
    </div>
@endsection

@section('scripts')
    <script src="{{asset('assets/js/functions/translation.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '#pagination-links a', function (event) {
                event.preventDefault();

                var page = $(this).attr('href').split('page=')[1];
                fetch_data(page);
            });

            function fetch_data(page) {
                $.ajax({
                    url: "/dashboard/user/openai/documents/all?page=" + page,
                    success: function (data) {
                        $('#documents-body').html('');
                        $.each(data.items.data, function (index, document) {
                            if(document.generator.filters !== 'graphics'){
                                let url=`${window.location.origin}/images/${document.generator.filters}.png`;
                                $('#documents-body').append(`
                                <tr>
                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                        <div class="flex w-60 items-center sm:w-auto">
                                                <span class="block h-6 w-6">
                                                    <img src="${url}" alt="">
                                                </span>
                                            <div class="ms-3">
                                                <div
                                                    class="line-clamp-2 text-xs font-bold text-slate-600 dark:text-slate-200">${document.output?document.output.slice(0,20):'بدون محتوا'}</div>
                                                <div
                                                    class="block text-[11px] font-medium text-slate-500 dark:text-slate-400">
                                                    ${getTranslation(document.generator.title)} </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                            <span class="text-xs text-slate-500 dark:text-slate-400">
                                                <strong>${document.credits} </strong> کلمه </span>
                                    </td>
                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                    <span
                                        class="block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> ${new Date(document.created_at).toLocaleDateString('fa-IR-u-nu-latn',{ year: 'numeric', month: 'long', day: 'numeric' }) } </span>
                                        <span class="block text-[11px] font-medium text-slate-500 dark:text-slate-400"> ${new Date(document.created_at).toLocaleTimeString('en-US',{hour12:false,timeZone: "Asia/Tehran"})} </span>
                                    </td>
                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                        <ul class="flex justify-end gap-2">
                                            <li>
                                                <a href="${window.location.origin}/dashboard/user/openai/documents/single/${document.slug}"
                                                   title="مشاهده" class="inline-flex items-center justify-center rounded-full bg-slate-200 p-2 text-slate-600 transition-all hover:bg-blue-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-blue-600 hover:dark:text-white">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                         stroke-width="1.5" stroke="currentColor" class="h-3 w-3">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z"/>
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"/>
                                                    </svg><!-- eye - outline - heroicons  -->
                                                </a>
                                            </li>
                                            <li>
                                                <button title="حذف" type="button" href="" data-slug="${document.slug}" onclick="deleteDocument(this)"
                                                   class="inline-flex items-center justify-center rounded-full bg-rose-100 p-2 text-rose-600 transition-all hover:bg-rose-600 hover:text-white dark:bg-rose-950 dark:text-rose-200 hover:dark:bg-rose-600 hover:dark:text-white">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                         stroke-width="1.5" stroke="currentColor" class="h-3 w-3">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"/>
                                                    </svg><!-- trash - outline - heroicons  -->
                                                </button>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            `);
                            }

                        });

                        $('#pagination-links').html(data.links);
                    }
                });
            }
        });

        function deleteDocument(elm){
            const currenturl = window.location.href;
            const server = currenturl.split('/')[0];
            Swal.fire({
                    title: "هشدار!",
                    text: 'مطمئنی میخوای حذفش کنی؟',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "نه! ولش کن",
                    confirmButtonText: "بله"
                },
            ).then((result) => {
                if (result.isConfirmed) {
                    const delete_url =
                        `${server}/dashboard/user/openai/documents/delete/${$(elm).data('slug')}`;

                    $.ajax({
                        type: "get",
                        url: delete_url,
                        success: function (data) {
                            Swal.fire({
                                title: "حله!",
                                text: 'محتوایی که ساخته بودی حذف شد!',
                                icon: "success",
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "ممنون!"
                            }).then(() => {
                                window.location=server+'/'+data.url
                            });
                        },
                        error: function () {
                            Swal.fire({
                                title: "متاسفم!",
                                text: 'به دلیل مشکلی نشد حذف کنم!',
                                icon: "error",
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "ممنون!"
                            });
                        }
                    });
                }
            })
        }
    </script>
@endsection
