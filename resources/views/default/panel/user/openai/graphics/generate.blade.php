@extends('layout.app')

@section('title')
    هوش من |  {{__('ALL Writers List')}}
@endsection
@section('content')
    <div class="py-10">
        <div class="relative px-3 py-10">
            <div class="container px-3">
                <div class="mb-7">
                    <div class="p-3 bg-rose-100 text-center">
                        <p class="mb-2 text-sm text-rose-600 dark:text-white"> عکس ها فقط تا دو هفته توی پنلت میمونه و
                            بعدش حذف میشن.</p>
                    </div>
                </div>
                <div class="flex flex-wrap items-start gap-8 xl:flex-nowrap">
                    <div
                        class="w-full rounded-lg border border-slate-200 bg-white px-7 py-6 dark:border-slate-800 dark:bg-slate-950 xl:w-96">
                        <ul class="tab-nav -mx-4 flex flex-wrap xl:mx-0">
                            @php $i=0 @endphp
                            @foreach($templates as $template)
                                <li class="tab-item w-full px-4 xs:w-1/2 xs:py-1 sm:w-1/3 lg:w-1/4 xl:w-full xl:px-0 xl:py-0">
                                    <a href="#" data-target="#{{$template->slug}}" data-slug="{{$template->slug}}"
                                       data-id="{{$template->id}}"
                                       data-title="{{__($template->title)}}"
                                       class="{{$i==0?'active':''}} template-slug tab-toggle relative isolate flex text-slate-500 before:content-[''] dark:text-slate-400 [&.active]:text-blue-600 [&.active]:before:absolute [&.active]:before:-inset-x-3 [&.active]:before:inset-y-0 [&.active]:before:-z-10 [&.active]:before:rounded-md [&.active]:before:bg-blue-100 [&.active]:before:dark:bg-blue-950">
                                        <div class="flex items-center py-2">
                                            <div class="me-3 h-5">
                                                <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                                                     class="h-full">
                                                    <path class="fill-blue-300"
                                                          d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                                                    <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                                          d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                                                    <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                                          d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                                                </svg>
                                            </div>
                                            <span class="text-sm font-medium"> {{__($template->title)}} </span>
                                        </div>
                                    </a>
                                </li>
                                @php $i++ @endphp
                            @endforeach
                        </ul>
                    </div>
                    <div class="tab-content flex-grow-1 w-full">
                        <div class="tab-panel active hidden [&.active]:block" id="ai_image_generator">
                            <div
                                class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
                                <div
                                    class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
                                    <div
                                        class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                                        <h5 id="title"
                                            class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('AI Image Generator')}} </h5>
                                        <!-- col -->
                                        <form action="">
                                            <input type="hidden" id="type" value="image">
                                            <div class="-mx-3 flex flex-wrap">
                                                <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                                                    <label for="description"
                                                           class="mb-4 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                                        متن ایده</label>
                                                    <div
                                                        class="rounded-md border border-slate-200 p-1 dark:border-slate-800 sm:flex-nowrap">
                                                        <div class="relative flex w-full flex-grow sm:w-auto">
                                            <textarea
                                                class="mb-0 w-full resize-none rounded-md border-0 bg-white px-3 py-3 text-sm text-slate-600 placeholder:text-slate-400 focus:border-0 focus:shadow-none focus:outline-none focus:ring-0 disabled:bg-slate-100 disabled:text-slate-400 dark:bg-slate-950 dark:text-slate-200"
                                                rows="1" placeholder="ایده ت رو باید اینجا بنویسی :)"
                                                id="description" name="description"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                                                    <label for="creativity"
                                                           class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                                        {{__('Art Style')}} </label>
                                                    <select
                                                        class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                                        id="image_style" name="image_style">

                                                        <option>{{__('None')}}</option>
                                                        <option value="3d_render">{{__('3D Render')}}</option>
                                                        <option value="anime">{{__('Anime')}}</option>
                                                        <option
                                                            value="ballpoint_pen">{{__('Ballpoint Pen Drawing')}}</option>
                                                        <option value="cartoon">{{__('Cartoon')}}</option>
                                                        <option value="clay">{{__('Clay')}}</option>
                                                        <option value="contemporary">{{__('Contemporary')}}</option>
                                                        <option value="cubism">{{__('Cubism')}}</option>
                                                        <option value="cyberpunk">{{__('Cyberpunk')}}</option>
                                                        <option value="glitchcore">{{__('Glitchcore')}}</option>
                                                        <option value="impressionism">{{__('Impressionism')}}</option>
                                                        <option value="isometric">{{__('Isometric')}}</option>
                                                        <option value="line">{{__('Line Art')}}</option>
                                                        <option value="low_poly">{{__('Low Poly')}}</option>
                                                        <option value="minimalism">{{__('Minimalism')}}</option>
                                                        <option value="modern">{{__('Modern')}}</option>
                                                        <option value="origami">{{__('Origami')}}</option>
                                                        <option value="pencil">{{__('Pencil')}}</option>
                                                        <option value="pixel">{{__('Pixel')}}</option>
                                                        <option value="pointillism">{{__('Pointillism')}}</option>
                                                        <option value="pop">{{__('Pop')}}</option>
                                                        <option value="realistic">{{__('Realistic')}}</option>
                                                        <option value="renaissance">{{__('Renaissance')}}</option>
                                                        <option value="retro">{{__('Retro')}}</option>
                                                        <option value="steampunk">{{__('Steampunk')}}</option>
                                                        <option value="sticker">{{__('Sticker')}}</option>
                                                        <option value="ukiyo">{{__('Ukiyo')}}</option>
                                                        <option value="vaporwave">{{__('Vaporwave')}}</option>
                                                        <option value="vector">{{__('Vector')}}</option>
                                                        <option value="watercolor">{{__('Watercolor')}}</option>
                                                    </select>
                                                </div>
                                                <!-- col -->
                                                <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                                                    <label for="image_lighting"
                                                           class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                                        {{__('Lightning Style')}} </label>
                                                    <select
                                                        class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                                        id="image_lighting" name="image_lighting">
                                                        <option>{{__('None')}}</option>
                                                        <option value="natural">{{__('Natural')}}</option>
                                                        <option value="warm">{{__('Warm')}}</option>
                                                        <option value="cold">{{__('Cold')}}</option>
                                                        <option value="neon">{{__('Neon')}}</option>
                                                        <option value="ambient">{{__('Ambient')}}</option>
                                                        <option value="backlight">{{__('Backlight')}}</option>
                                                        <option value="cinematic">{{__('Cinematic')}}</option>
                                                        <option value="dramatic">{{__('Dramatic')}}</option>
                                                        <option value="foggy">{{__('Foggy')}}</option>
                                                        <option value="studio">{{__('Studio')}}</option>
                                                    </select>
                                                </div>
                                                <!-- col -->
                                                <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                                                    <label for="size"
                                                           class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                                        {{__('Image size')}} </label>
                                                    <select
                                                        class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                                        id="size" name="size">
                                                        <option value="1024x1024">{{__('Post')}}</option>
                                                        <option value="1792x1024">{{__('youtube')}}</option>
                                                        <option value="1024x1792">{{__('Story')}}</option>
                                                    </select>
                                                </div>
                                                <!-- col -->
                                                <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                                                    <label for="image_mood"
                                                           class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                                        {{__('Mood')}} </label>
                                                    <select
                                                        class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                                        id="image_mood" name="image_mood">
                                                        <option>{{__('None')}}</option>
                                                        <option value="aggressive">{{__('Aggressive')}}</option>
                                                        <option value="angry">{{__('Angry')}}</option>
                                                        <option value="boring">{{__('Boring')}}</option>
                                                        <option value="bright">{{__('Bright')}}</option>
                                                        <option value="calm">{{__('Calm')}}</option>
                                                        <option value="cheerful">{{__('Cheerful')}}</option>
                                                        <option value="chilling">{{__('Chilling')}}</option>
                                                        <option value="colorful">{{__('Colorful')}}</option>
                                                        <option value="dark">{{__('Dark')}}</option>
                                                        <option value="neutral">{{__('Neutral')}}</option>
                                                    </select>
                                                </div>

                                                <div class="px-3 py-2">
                                                    <button type="button" id="openai_generator_button"
                                                            onclick="sendOpenaiGeneratorForm(this);"
                                                            data-slug="ai_image_generator"
                                                            class="openai_generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                                        {{__('Lets go')}} </button>
                                                    <br>
                                                    <div id="buy-alert-area"
                                                         class="hidden bg-rose-100 text-center mt-2 rounded">
                                                        <span id="time-alert" class="text-xs text-rose-600 p-2"></span>
                                                        <br>
                                                        <a href="{{route('dashboard.user.payment.subscription')}}"
                                                           id="buy-plan"
                                                           class="hidden inline-flex rounded-full bg-rose-600 px-5 py-2 my-2 text-xs font-medium text-white transition-all hover:bg-rose-700-800">
                                                            تهیه اشتراک</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </form>
                                        <!-- col -->
                                    </div>
                                </div>
                                <div class="w-full p-6 lg:w-3/5 lg:p-10">
                                    <div
                                        class="flex h-full w-full items-center justify-center text-center waiting-area">
                                        <div class="flex flex-col items-center py-2">
                                            <div class="h-36">
                                                <img class="h-full"
                                                     src="{{asset('images/illustration/blank-image.svg')}}"
                                                     alt=""/>
                                            </div>
                                            <div class="mt-5 max-w-xs">
                                                <p class="result-text text-xl text-slate-500 dark:text-slate-400">
                                                    عکسی
                                                    که میخوای توصیف
                                                    کن و دکمه بزن بریم رو فشار بده </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="js-lightbox-gallery gap-7 main-image-area hidden">
                                        <div class="relative group">
                                            <img class="rounded-lg main-image"
                                                 src=""
                                                 alt="">
                                            <div
                                                class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                                                <a class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                                                   href="" download="" title="دانلود عکس">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                         viewBox="0 0 24 24"
                                                         stroke-width="1.5" stroke="currentColor" class="h-4">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                                                    </svg><!-- arrow-down-tray - outline - heroicons -->
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-panel hidden [&.active]:block" id="ai_video">
                            <div
                                class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
                                <div
                                    class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
                                    <div
                                        class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                                        <h5 id="title"
                                            class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('AI Video Generator')}} </h5>
                                        <!-- col -->
                                        <form action="">
                                            <input type="hidden" id="type" value="image">
                                            <div class="-mx-3 flex flex-wrap">
                                                <div class="flex w-full flex-col gap-5"
                                                     ondrop="dropHandler(event, 'img2img_src');"
                                                     ondragover="dragOverHandler(event);">
                                                    <label
                                                        class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                                        for="img2img_src">
                                                        <div class="flex flex-col items-center justify-center py-6">
                                                            <svg stroke-width="1.5" class="size-11 mb-4"
                                                                 xmlns="http://www.w3.org/2000/svg" width="24"
                                                                 height="24" viewBox="0 0 24 24" stroke="currentColor"
                                                                 fill="none" stroke-linecap="round"
                                                                 stroke-linejoin="round">
                                                                <path
                                                                    d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                                                <path d="M9 15l3 -3l3 3"></path>
                                                                <path d="M12 12l0 9"></path>
                                                            </svg>
                                                            <p class="mb-1 text-sm font-semibold">
                                                                تصویر رو اینجا انتخاب کن. فقط سایزهای 1024x576،
                                                                576x1024، 768x768 قابل تبدیل هستند.
                                                            </p>

                                                            <p class="file-name mb-0 text-2xs">
                                                                (فرمت عکس باید png با jpg باشه)
                                                            </p>
                                                        </div>

                                                        <input class="hidden" id="img2img_src" type="file"
                                                               accept=".png, .jpg, .jpeg"
                                                               onchange="handleFileSelect('img2img_src')">
                                                    </label>
                                                </div>

                                                <div class="px-3 py-2">
                                                    <button type="button" id="video_generator_button"
                                                            data-slug="ai_video"
                                                            onclick="sendOpenaiGeneratorForm(this);"
                                                            class="inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                                        {{__('Lets go')}} </button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- col -->
                                    </div>
                                </div>
                                <div class="w-full p-6 lg:w-3/5 lg:p-10">
                                    <div
                                        class="flex h-full w-full items-center justify-center text-center waiting-area">
                                        <div class="flex flex-col items-center py-2">
                                            <div class="h-36">
                                                <img class="h-full"
                                                     src="{{asset('images/illustration/blank-image.svg')}}"
                                                     alt=""/>
                                            </div>
                                            <div class="mt-5 max-w-xs">
                                                <p class="result-text text-xl text-slate-500 dark:text-slate-400">
                                                    عکسی
                                                    که میخوای به ویدئو تبدیل بشه انتخاب کن
                                                    و دکمه بزن بریم رو فشار بده </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="js-lightbox-gallery gap-7 main-video-area hidden">
                                        <div class="relative group">
                                            <figure
                                                class="lqd-video-result-fig relative mb-3 aspect-square overflow-hidden rounded-lg shadow-md transition-all group-hover:-translate-y-1 group-hover:scale-105 group-hover:shadow-lg"
                                                data-lqd-skeleton-el>
                                                <video
                                                    class="lqd-video-result-video h-full w-full object-cover object-center"
                                                    loading="lazy">
                                                    <source class="main-video"
                                                            src=""
                                                            loading="lazy"
                                                            type="video/mp4">
                                                </video>
                                                <div
                                                    class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                                                    <a class="lqd-video-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                                                       href="" download="" title="دانلود فیلم">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                             viewBox="0 0 24 24"
                                                             stroke-width="1.5" stroke="currentColor" class="h-4">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                                                        </svg><!-- arrow-down-tray - outline - heroicons -->
                                                    </a>
                                                    <a class="lqd-video-result-play flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                                                       href="" data-fslightbox="video-gallery" title="پخش فیلم">
                                                        <svg class="h-4" xmlns="http://www.w3.org/2000/svg"
                                                             viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                                                             fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                            <path
                                                                d="M6 4v16a1 1 0 0 0 1.524 .852l13 -8a1 1 0 0 0 0 -1.704l-13 -8a1 1 0 0 0 -1.524 .852z"
                                                                stroke-width="0" fill="currentColor"></path>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-panel hidden [&.active]:block" id="ai_product_image">
                            <div
                                class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
                                <div
                                    class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
                                    <div
                                        class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                                        <h5 id="title"
                                            class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('Product Image Generator')}} </h5>
                                        <small
                                            class="inline-flex mb-4 p-1 rounded text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">
                                            {{__('Here, you can upload photos of your products and create attractive backgrounds for them. You can either create a custom background for the image or use pre-made backgrounds. Note that you can`t do both (write a description and select an image) at the same time.')}}
                                        </small>
                                        <!-- col -->
                                        <form action="">
                                            <input type="hidden" id="type" value="image">
                                            <div class="-mx-3 flex flex-wrap">
                                                <div class="flex w-full flex-col gap-5"
                                                     ondrop="dropHandler(event, 'product_image');"
                                                     ondragover="dragOverHandler(event);">
                                                    <label
                                                        class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                                        for="product_image">
                                                        <div class="flex flex-col items-center justify-center py-6">
                                                            <svg stroke-width="1.5" class="size-11 mb-4"
                                                                 xmlns="http://www.w3.org/2000/svg" width="24"
                                                                 height="24" viewBox="0 0 24 24" stroke="currentColor"
                                                                 fill="none" stroke-linecap="round"
                                                                 stroke-linejoin="round">
                                                                <path
                                                                    d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                                                <path d="M9 15l3 -3l3 3"></path>
                                                                <path d="M12 12l0 9"></path>
                                                            </svg>
                                                            <p class="mb-1 text-sm font-semibold">
                                                                تصویر رو اینجا انتخاب کن. عکس باید با
                                                                فرمت png یا jpg یا jpeg و حجم عکس باید کمتر از 10 مگابایت باشه.
                                                            </p>
                                                            <p class="file-name mb-0 text-2xs">
                                                            </p>
                                                        </div>

                                                        <input class="hidden" id="product_image" type="file"
                                                               onchange="handleFileSelect('product_image')"
                                                               accept=".png, .jpg, .jpeg">
                                                    </label>
                                                </div>
                                                {{--                                                <div class="grid grid-cols-2 gap-3 sm:grid-cols-2 mt-5">--}}
                                                {{--                                                    <label--}}
                                                {{--                                                        class="relative flex h-full border border-slate-200 rounded cursor-pointer flex-col has-[:checked]:cursor-default">--}}
                                                {{--                                                        <img alt="" class="flex-shrink-0 rounded-t-md"--}}
                                                {{--                                                             src="{{asset('images/backgrounds/sample.png')}}">--}}
                                                {{--                                                        <span--}}
                                                {{--                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white">نمونه عکس درست </span>--}}
                                                {{--                                                    </label>--}}

                                                {{--                                                    <label--}}
                                                {{--                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default">--}}
                                                {{--                                                        <img alt="" class="flex-shrink-0 rounded-t-md"--}}
                                                {{--                                                             src="{{asset('images/backgrounds/wrong.jpg')}}">--}}
                                                {{--                                                        <span--}}
                                                {{--                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> نمونه عکس نادرست </span>--}}
                                                {{--                                                    </label>--}}
                                                {{--                                                </div>--}}
                                                <div class="w-full py-2">
                                                    <label for="description"
                                                           class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                                        {{__('Description')}}
                                                    </label>
                                                    <textarea
                                                        class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                                        placeholder="اینجا بنویس بکگراندت چی باشه و چطوری باشه."
                                                        rows="4" id="product_description" name="description"></textarea>
                                                </div>

                                                <p class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                                    یا از اینجا بکگراند نمونه انتخاب کن:</p>

                                                <div class="grid grid-cols-3 gap-3 sm:grid-cols-3 mt-5">
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="surprise-me">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="surprise-me" value="Surprise me">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/surprise-me.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('surprise me')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Studio">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Studio" value="Studio">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Studio.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Studio')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Silk">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Silk" value="Silk">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Silk.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Silk')}} </span>
                                                    </label>
                                                </div>
                                                <div id="additional-items"
                                                     class="hidden grid grid-cols-3 gap-3 sm:grid-cols-3 mt-5">
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Cafe">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Cafe" value="Cafe">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Cafe.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Cafe')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Tabletop">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Tabletop" value="Tabletop">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Tabletop.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Tabletop')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Light-Wood">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Light-Wood" value="Light Wood">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Light-Wood.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Light Wood')}} </span>
                                                    </label>

                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Dark-Wood">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Dark-Wood" value="Dark Wood">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Dark-Wood.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Dark Wood')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Marble">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Marble" value="Marble">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Marble.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Marble')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Park-Bench">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Park-Bench" value="Park Bench">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Park-Bench.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Park Bench')}} </span>
                                                    </label>

                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Outdoors">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Outdoors" value="Outdoors">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Outdoors.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Outdoors')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Roses">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Roses" value="Roses">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Roses.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Roses')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Lavender">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Lavender" value="Lavender">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Lavender.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Lavender')}} </span>
                                                    </label>

                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Meadow">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Meadow" value="Meadow">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Meadow.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Meadow')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Dried-Flowers">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Dried-Flowers"
                                                               value="Dried Flowers">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Dried-Flowers.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Dried Flowers')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Baby's-Breath">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Baby's-Breath"
                                                               value="Baby's Breath">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Babys-Breath.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Baby`s Breath')}} </span>
                                                    </label>

                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Kitchen">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Kitchen" value="Kitchen">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Kitchen.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Kitchen')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Fruits">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Fruits" value="Fruits">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Fruits.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Fruits')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Nature">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Nature" value="Nature">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Nature.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Nature')}} </span>
                                                    </label>

                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Beach">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Beach" value="Beach">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Beach.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Beach')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Bathroom">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Bathroom" value="Bathroom">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Bathroom.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Bathroom')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Skyscraper">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Skyscraper" value="Skyscraper">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Skyscraper.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Skyscraper')}} </span>
                                                    </label>

                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Gifts">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Gifts" value="Gifts">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Gifts.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Gifts')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Christmas">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Christmas" value="Christmas">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Christmas.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Christmas')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Wedding">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Wedding" value="Wedding">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Wedding.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Wedding')}} </span>
                                                    </label>

                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Halloween">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Halloween" value="Halloween">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Halloween.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Halloween')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Paint">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Paint" value="Paint">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Paint.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Paint')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Fire">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Fire" value="Fire">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Fire.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Fire')}} </span>
                                                    </label>

                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Water">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Water" value="Water">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Water.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Water')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Gold">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Gold" value="Gold">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Gold.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Gold')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Pebbles">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Pebbles" value="Pebbles">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Pebbles.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Pebbles')}} </span>
                                                    </label>

                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Snow">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Snow" value="Snow">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Snow.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Snow')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Necklace">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Necklace" value="Necklace">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Necklace.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Necklace')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Modern-Interior">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Modern-Interior"
                                                               value="Modern Interior">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Modern-Interior.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Modern Interior')}} </span>
                                                    </label>

                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Minimalist-Interior">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Minimalist-Interior"
                                                               value="Minimalist Interior">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Minimalist-Interior.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Minimalist Interior')}} </span>
                                                    </label>
                                                    <label
                                                        class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                                        for="Luxury-Interior">
                                                        <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                                               name="imageStyle" id="Luxury-Interior"
                                                               value="Luxury Interior">
                                                        <img alt="" class="flex-shrink-0 rounded-t-md"
                                                             src="{{asset('images/backgrounds/Luxury-Interior.webp')}}">
                                                        <span
                                                            class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Luxury Interior')}} </span>
                                                    </label>
                                                </div>

                                                <!-- دکمه نمایش بیشتر -->
                                                <div class="text-center mt-4 w-full">
                                                    <button type="button" id="toggle-button"
                                                            class="px-4 py-2 text-xs border border-slate-200 bg-white text-gray-400 rounded">
                                                        نمایش بیشتر
                                                    </button>
                                                </div>
                                                <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                                                    <label for="size"
                                                           class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                                        {{__('Image size')}} </label>
                                                    <select
                                                        class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                                        id="product-size" name="size">
                                                        <option value="1024x1024">{{__('Post')}}</option>
                                                        <option value="1792x1024">{{__('youtube')}}</option>
                                                        <option value="1024x1792">{{__('Story')}}</option>
                                                    </select>
                                                </div>

                                                <div class="px-3 py-2">
                                                    <button type="button" id="product_generator_button"
                                                            data-slug="ai_product_image"
                                                            onclick="sendOpenaiGeneratorForm(this);"
                                                            class="openai_generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                                        {{__('Lets go')}} </button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- col -->
                                    </div>
                                </div>
                                <div class="w-full p-6 lg:w-3/5 lg:p-10">
                                    <div
                                        class="flex h-full w-full items-center justify-center text-center waiting-area">
                                        <div class="flex flex-col items-center py-2">
                                            <div class="h-36">
                                                <img class="h-full"
                                                     src="{{asset('images/illustration/blank-image.svg')}}"
                                                     alt=""/>
                                            </div>
                                            <div class="mt-5 max-w-xs">
                                                <p class="result-text text-xl text-slate-500 dark:text-slate-400">
                                                    عکس محصولت رو انتخاب کن و دکمه بزن بریم رو فشار بده. حواست باشه باید
                                                    عکس بدون بکگراند باشه.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="js-lightbox-gallery gap-7 main-image-area hidden">
                                        <div class="relative group">
                                            <img class="rounded-lg main-image"
                                                 src=""
                                                 alt="">
                                            <div
                                                class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                                                <a class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                                                   href="" download="" title="دانلود عکس">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                         viewBox="0 0 24 24"
                                                         stroke-width="1.5" stroke="currentColor" class="h-4">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                                                    </svg><!-- arrow-down-tray - outline - heroicons -->
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="relative px-3 py-10">
            <div class="container px-3">
                <h5 class="mb-4 text-lg font-bold text-slate-700 dark:text-white results-title"> عکس هایی که ساختی </h5>
                <div
                    class="rounded-lg border border-slate-200 bg-white p-5 dark:border-slate-800 dark:bg-slate-950 xs:p-7">
                    <div class="js-lightbox-gallery grid grid-cols-2 gap-4 xs:grid-cols-3 md:grid-cols-5 results">
                    </div>
                    <!-- grid -->
                </div>
                <!-- card -->
            </div>
            <!-- container -->
        </div>

        <template id="image_result">
            <div class="image-result group relative">
                <img
                    class="image-result-img aspect-[4/3] rounded-md border border-slate-200 object-cover dark:border-slate-800"
                    src="" alt="">
                <div
                    class="absolute bottom-7 start-4 flex translate-x-2 translate-y-2 gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                    <a href="" data-pswp-width="1000" data-pswp-height="1000"
                       class="view-image js-lightbox-toggle flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             stroke-width="2"
                             stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M10 12a2 2 0 1 0 4 0a2 2 0 0 0 -4 0"></path>
                            <path
                                d="M21 12c-2.4 4 -5.4 6 -9 6c-3.6 0 -6.6 -2 -9 -6c2.4 -4 5.4 -6 9 -6c3.6 0 6.6 2 9 6"></path>
                        </svg>
                    </a>
                    <a class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                       href="../images/generated/hyper-realistic-modern-sofa-with-pastel-colors-and-white-background-cinematic-light.jpg"
                       download="">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                             stroke="currentColor" class="h-4">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                        </svg><!-- arrow-down-tray - outline - heroicons -->
                    </a>
                    <a class="image-result-delete flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                       href="#" onclick="return deleteImage(this)">
                        <svg width="10" height="9" viewBox="0 0 10 9" fill="var(--lqd-heading-color)"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M9.08789 1.49609L5.80664 4.75L9.08789 8.00391L8.26758 8.82422L4.98633 5.57031L1.73242 8.82422L0.912109 8.00391L4.16602 4.75L0.912109 1.49609L1.73242 0.675781L4.98633 3.92969L8.26758 0.675781L9.08789 1.49609Z"></path>
                        </svg>
                    </a>
                </div>
                <div
                    class="absolute bottom-0 w-full rounded-b-md bg-gradient-to-b from-transparent via-slate-900 via-70% to-slate-900 p-3 opacity-0 transition-all group-hover:opacity-100">
                    <span class="image_prompt line-clamp-2 text-xs font-bold text-white"></span>
                </div>
            </div>
        </template>

        <template id="video_result">
            <div class="video-result group relative">
                <figure
                    class="lqd-video-result-fig relative mb-3 aspect-square overflow-hidden rounded-lg shadow-md transition-all group-hover:-translate-y-1 group-hover:scale-105 group-hover:shadow-lg"
                    data-lqd-skeleton-el
                >
                    <video
                        class="lqd-video-result-video h-full w-full object-cover object-center"
                        loading="lazy"
                    >
                        <source
                            src=""
                            loading="lazy"
                            type="video/mp4"
                        >
                    </video>
                    <div
                        class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                        <a class="lqd-video-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="" download="" title="دانلود فیلم">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                            </svg><!-- arrow-down-tray - outline - heroicons -->
                        </a>
                        <a class="lqd-video-result-play flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="" data-fslightbox="video-gallery" title="پخش فیلم">
                            <svg class="h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="2"
                                 stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path
                                    d="M6 4v16a1 1 0 0 0 1.524 .852l13 -8a1 1 0 0 0 0 -1.704l-13 -8a1 1 0 0 0 -1.524 .852z"
                                    stroke-width="0" fill="currentColor"></path>
                            </svg>
                        </a>
                    </div>
                </figure>
            </div>
        </template>
    </div>
@endsection

@section('scripts')
    <script>
        $('#toggle-button').on('click', function () {
            const additionalItems = $('#additional-items');
            const button = $('#toggle-button');

            if (additionalItems.hasClass('hidden')) {
                additionalItems.hide().removeClass('hidden').fadeIn();  // Fade in the content
                button.text('بستن');
            } else {
                additionalItems.fadeOut(function () {  // Fade out the content, then hide it
                    additionalItems.addClass('hidden');
                });
                button.text('نمایش بیشتر');
            }
        });


        openai_id = '{!! __($first_template->id) !!}';
        let resizedImage;
        let imageWidth = -1;
        let imageHeight = -1;
        let postImageWidth = -1;
        let postImageHeight = -1;
        let show_results_slut = 'ai_image_generator'

        var resultVideoId = "";
        var intervalId = -1;
        var sourceImgUrl = "";
        var checking = false;
        let offset = 0;

        const currenturl = window.location.href;
        const server = currenturl.split('/')[0];

        $(document).ready(function () {
            let title = '{!! __($first_template->title) !!}';
            let type = '{!! __($first_template->slug) !!}';

            $('#generator-title').text(title)
            $('#post-type').val(type)

            $('.template-slug').click(function () {
                openai_id = $(this).data('id')
                show_results_slut = $(this).data('slug')
                offset = 0
                lazyLoadImages()
            })

            $('.image-result-delete').click(function (e) {
                e.preventDefault()
                e.stopPropagation()
                Swal.fire({
                    title: "هشدار!",
                    text: 'مطمئنی میخوای حذفش کنی؟',
                    icon: "warning",
                    confirmButtonText: "بله",
                })
            })
        })

        document.getElementById("img2img_src").addEventListener('change', resizeImage);
        document.getElementById("product_image").addEventListener('change', resizeImage);

        function deleteImage(elm) {
            Swal.fire({
                    title: "هشدار!",
                    text: 'مطمئنی میخوای حذفش کنی؟',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "نه! ولش کن",
                    confirmButtonText: "بله"
                },
            ).then((result) => {
                if (result.isConfirmed) {
                    const delete_url =
                        `${server}/dashboard/user/openai/documents/delete/image/${$(elm).data('slug')}`;

                    $.ajax({
                        type: "get",
                        url: delete_url,
                        success: function () {
                            Swal.fire({
                                title: "حله!",
                                text: 'عکسی که ساخته بودی حذف شد!',
                                icon: "success",
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "ممنون!"
                            }).then(() => {
                                location.reload()
                            });
                        },
                        error: function () {
                            Swal.fire({
                                title: "متاسفم!",
                                text: 'به دلیل مشکلی نشد حذف کنم!',
                                icon: "error",
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "ممنون!"
                            });
                        }
                    });
                }
            })
        }

        function dropHandler(ev, id) {
            // Prevent default behavior (Prevent file from being opened)
            ev.preventDefault();
            $('#' + id)[0].files = ev.dataTransfer.files;
            resizeImage();
            $('#' + id).prev().find(".file-name").text(ev.dataTransfer.files[0].name);
        }

        function dragOverHandler(ev) {
            ev.preventDefault();
        }

        function handleFileSelect(id) {
            $('#' + id).prev().find(".file-name").text($('#' + id)[0].files[0].name);
        }

        function resizeImage(ev) {
            var file;
            file = $(`#${ev.target.id}`)[0].files[0];
            if (file == undefined) return;
            var reader = new FileReader();

            reader.onload = function (event) {
                var img = new Image();

                img.onload = function () {
                    var canvas = document.createElement('canvas');
                    var ctx = canvas.getContext("2d");

                    imageWidth = this.width;
                    imageHeight = this.height;

                    canvas.width = this.width;
                    canvas.height = this.height;
                    var ctx = canvas.getContext("2d");
                    ctx.drawImage(img, 0, 0, this.width, this.height);

                    var dataurl = canvas.toDataURL("image/png");

                    var byteString = atob(dataurl.split(',')[1]);
                    var mimeString = dataurl.split(',')[0].split(':')[1].split(';')[0];
                    var ab = new ArrayBuffer(byteString.length);
                    var ia = new Uint8Array(ab);
                    for (var i = 0; i < byteString.length; i++) {
                        ia[i] = byteString.charCodeAt(i);
                    }
                    var blob = new Blob([ab], {
                        type: mimeString
                    });

                    resizedImage = new File([blob], file.name);
                }
                img.src = event.target.result;
            }

            reader.readAsDataURL(file);

        }

        function checkImageSize(file) {
            // Check if a file is selected
            if (file) {
                // Convert file size to MB
                return file.size / (1024 * 1024)
            }
        }

        function checkVideoDone() {
            'use strict';
            if (checking) return;
            checking = true;

            let formData = new FormData();
            formData.append('id', resultVideoId);
            formData.append('url', sourceImgUrl);
            formData.append('size', `${postImageWidth}x${postImageHeight}`);

            $.ajax({
                type: "post",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                },
                url: "/dashboard/user/openai/check/videoprogress",
                data: formData,
                contentType: false,
                processData: false,
                success: function (res) {
                    checking = false;
                    if (res.status === 'finished') {
                        clearInterval(intervalId);
                        intervalId = -1;
                        $('.main-video-area').removeClass('hidden');
                        $('.waiting-area').addClass('hidden');
                        const delete_url = `${server}/dashboard/user/openai/documents/delete/image/${res.video.slug}`;
                        const download_url = `${server}/dashboard/user/openai/documents/download/image/${res.video.slug}`;
                        $('.main-video').attr('src', `https://content.hosheman.com/${res.video.output}`)
                        $('.lqd-video-result-download').attr('href', download_url)
                        $('.lqd-video-result-play').attr('href', `https://content.hosheman.com/${res.video.output}`)
                        const videoContainer = document.querySelector('.results');
                        const videoResultTemplate = document.querySelector('#video_result').content.cloneNode(
                            true);
                        videoResultTemplate.querySelector('.video-result').setAttribute('data-id', res.video.id);
                        videoResultTemplate.querySelector('.lqd-video-result-video source').setAttribute('src',
                            `https://content.hosheman.com/${res.video.output}`);

                        videoResultTemplate.querySelector('.lqd-video-result-download').setAttribute('href', download_url);
                        videoResultTemplate.querySelector('.lqd-video-result-play').setAttribute('href',
                            `https://content.hosheman.com/${res.video.output}`);

                        videoContainer.insertBefore(videoResultTemplate, videoContainer.firstChild);

                        $('#video_generator_button').text('بزن بریم')
                        $('#video_generator_button').attr('disabled', false)

                    } else if (res.status == 'in-progress') {
                    }
                },
                error: function (data) {
                    checking = false;
                    clearInterval(intervalId);
                    $('.main-image-area').removeClass('hidden');
                    $('.waiting-area').addClass('hidden');
                    $('#openai_generator_button').text('بزن بریم !');
                    $('#openai_generator_button').attr('disabled', false);
                    toastr.error('خطایی در سرور وجود داره! لطفا با پشتیبانی تماس بگیر.');
                }
            });
        }

        function sendOpenaiGeneratorForm(elm, ev) {

            if ($(elm).data('slug') === 'ai_video' && !({{auth()->user()->remaining_videos}} > 0)) {
                toastr.error('تعداد ویدئوهای باقی مانده شما صفر میباشد.')
                return false
            }
            if ($(elm).data('slug') === 'ai_image_generator' && ({{ auth()->user()->remaining_images === 0 && auth()->user()->remaining_images !== -1 ? 'true' : 'false' }})) {
                toastr.error('تعداد عکس های باقی مانده شما صفر میباشد.')
                return false
            }

            if ($(elm).data('slug') === 'ai_product_image' && !({{auth()->user()->remaining_product_back }} > 0)) {
                toastr.error('تعداد عکس های باقی مانده شما برای ویرایش صفر میباشد. اشتراک تهیه کن.')
                return false
            }
            ev?.preventDefault();
            ev?.stopPropagation();

            let imageGenerator = 'dall-e'

            let slug = $(elm).data('slug')
            if (slug === 'ai_product_image') {
                if (resizedImage === undefined) {
                    toastr.warning('باید یک عکس انتخاب کنی!');
                    return false;
                }
            }

            if (slug === 'ai_video') {
                if (resizedImage === undefined) {
                    toastr.warning('باید یک عکس انتخاب کنی!');
                    return false;
                }
                if (!((imageWidth == 1024 && imageHeight == 576) || (imageWidth == 768 && imageHeight == 768) || (
                    imageWidth == 576 && imageHeight == 1024))) {
                    toastr.warning('سایز عکس باید  1024x576 یا 576x1024 یا  768x768 باشد');
                    return false;
                }
                postImageWidth = imageWidth;
                postImageHeight = imageHeight;
            }

            let formData = new FormData();
            formData.append('post_type', slug);
            formData.append('openai_id', openai_id);

            if (slug === 'ai_image_generator') {
                formData.append('image_generator', 'dall-e');

                if (imageGenerator === 'dall-e') {
                    formData.append('image_style', $("#image_style").val());
                    formData.append('image_lighting', $("#image_lighting").val());
                    formData.append('image_mood', $("#image_mood").val());
                    formData.append('size', $("#size").val());
                }

                @foreach (json_decode($first_template->questions) ?? [] as $question)
                if ("{{ $question->name }}" != "size")
                    formData.append("{{ $question->name }}", $("#{{ $question->name }}").val());
                @endforeach
            }

            if (slug === 'ai_video') {
                formData.append("image_src", resizedImage);
            }

            if (slug === 'ai_product_image') {
                formData.append("image", resizedImage);
                formData.append('product_description', $(`form textarea[id=product_description]`).val());
                formData.append('theme', $('input[name=imageStyle]:checked').val())
                formData.append('size', $("#product-size").val());
            }

            $(elm).text('لطفا منتظر بمون ...')
            $(elm).attr('disabled', true)
            $('.result-text').text('درحال آماده سازی ...')

            if (slug === 'ai_image_generator') {
                $.ajax({
                    type: "get",
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}",
                    },
                    url: "/dashboard/user/openai/free-image-numbers",
                    success: function (res) {
                        const freeImages = res.freeImages
                        const userActivePlan = '{!!auth()->user()->activePlan()!!}';
                        const userImages = '{!!auth()->user()->remaining_images!!}';

                        if (userActivePlan || userImages > 0) {
                            generateRequest(formData, slug)
                        } else {
                            switch (freeImages) {
                                case "0":
                                    generateRequest(formData, slug)
                                    break
                                case "1":
                                    setTimeout(function () {
                                        $('#buy-alert-area').removeClass('hidden').fadeIn(500)
                                        $('#buy-plan').removeClass('hidden')
                                        $('#time-alert').text('زمان تقریبی ساخت عکس 10 دقیقه است. برای سرعت بیشتر لطفا اشتراک تهیه کنید.')
                                    }, 5 * 1000)
                                    setTimeout(function () {
                                        generateRequest(formData, slug)
                                    }, 10 * 60 * 1000)
                                    break
                                case "2":
                                    setTimeout(function () {
                                        $('#buy-alert-area').removeClass('hidden')
                                        $('#buy-plan').removeClass('hidden')
                                        $('#time-alert').text('زمان تقریبی ساخت عکس 30 دقیقه است. برای سرعت بیشتر لطفا اشتراک تهیه کنید.')
                                    }, 5 * 1000)
                                    setTimeout(function () {
                                        generateRequest(formData, slug)
                                    }, 30 * 60 * 1000)
                                    break
                                default:
                                    setTimeout(function () {
                                        $('#buy-alert-area').removeClass('hidden')
                                        $('#buy-plan').removeClass('hidden')
                                        $('#time-alert').text('زمان تقریبی ساخت عکس 45 دقیقه است. برای سرعت بیشتر لطفا اشتراک تهیه کنید.')
                                    }, 5 * 1000)
                                    setTimeout(function () {
                                        generateRequest(formData, slug)
                                    }, 45 * 60 * 1000)
                                    break
                            }
                        }
                    }
                })
            } else {
                const imageSize = checkImageSize(resizedImage)
                if (imageSize >= 10){
                    toastr.error('حجم فایل باید کمتر از 10 مگابایت باشد.')
                    hideLoadingIndicators()
                } else
                    generateRequest(formData, slug)
            }
            return false;
        }

        function generateRequest(data, slug) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                },
                url: "{{url('/dashboard/user/openai/generate')}}",
                // url: "/test",
                data: data,
                contentType: false,
                processData: false,
                success: function (res) {
                    if (res.status !== 'success' && (res.message)) {
                        toastr.error(res.message);
                        hideLoadingIndicators();
                        return;
                    }

                    //show successful message
                    if (slug === 'ai_image_generator' || slug === 'ai_product_image') {
                        toastr.success(`عکسی که میخواستی آماده ست.`);
                        $('.main-image-area').removeClass('hidden');
                        $('.waiting-area').addClass('hidden');
                        $('.openai_generator_button').text('بزن بریم !');
                        $('.openai_generator_button').attr('disabled', false);
                    } else if (slug === 'ai_video') {
                        resultVideoId = res.id;
                    } else
                        toastr.success("{{ __('Generated Successfully!') }}");

                    setTimeout(function () {
                        if (slug === 'ai_image_generator' || slug === 'ai_product_image') {

                            const images = res.images;
                            const currenturl = window.location.href;
                            const server = currenturl.split('/')[0];
                            const imageContainer = document.querySelector('.results');
                            const imageResultTemplate = document.querySelector('#image_result').content
                                .cloneNode(true);

                            images.forEach((image) => {
                                const delete_url = `${server}/dashboard/user/openai/documents/delete/image/${image.slug}`;
                                const download_url = `${server}/dashboard/user/openai/documents/download/image/${image.slug}`;

                                imageResultTemplate.querySelector('.image-result').setAttribute('data-id', image.id);
                                imageResultTemplate.querySelector('.image-result-img')
                                    .setAttribute('src', `https://content.hosheman.com/${image.output}`);
                                imageResultTemplate.querySelector('.image_prompt')
                                    .textContent = image.main_input;
                                imageResultTemplate.querySelector('.view-image')
                                    .setAttribute('href', `https://content.hosheman.com/${image.output}`);
                                imageResultTemplate.querySelector('.image-result-download')
                                    .setAttribute('href', download_url);
                                imageResultTemplate.querySelector('.image-result-delete')
                                    .setAttribute('data-slug', image.slug);


                                $('.main-image').attr('src', `https://content.hosheman.com/${image.output}`)
                                $('.image-result-download').attr('href', download_url)
                                imageContainer.insertBefore(imageResultTemplate, imageContainer
                                    .firstChild);

                            })
                        } else if (slug === 'ai_video') {
                            sourceImgUrl = res.sourceUrl;
                            intervalId = setInterval(checkVideoDone, 10000);
                            hideLoadingIndicators();
                        }
                    }, 750);
                },
                error: function (data) {
                    $('.openai_generator_button').text('بزن بریم !');
                    $('.openai_generator_button').attr('disabled', false);
                    $('.result-text').text('عکس محصولت رو انتخاب کن و دکمه بزن بریم رو فشار بده. حواست باشه باید عکس بدون بکگراند باشه.')
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (index, value) {
                            toastr.error(value);
                        });
                    } else if (data.responseJSON.message) {
                        toastr.error(data.responseJSON.message);
                    }
                }
            });
        }

        function hideLoadingIndicators() {
            $('.openai_generator_button').text('بزن بریم')
            $('.openai_generator_button').attr('disabled', false)
            $('.result-text').text('عکس محصولت رو انتخاب کن و دکمه بزن بریم رو فشار بده. حواست باشه باید عکس بدون بکگراند باشه.')
        }

        function lazyLoadImages() {
            fetch(`{{ route('dashboard.user.openai.lazyloadimage')}}?offset=${offset}&post_type=${show_results_slut}`)
                .then(response => response.json())
                .then(data => {
                    const images = data.images;
                    const hasMore = data.hasMore;
                    $('.results').empty()
                    images.forEach(image => {
                        if (show_results_slut === 'ai_image_generator' || show_results_slut === 'ai_product_image') {
                            $('.results-title').text('عکس هایی که ساختی')
                            $('.results').append(`<div class="image-result group relative" data-id="${image.id}">
                <img
                    class="image-result-img aspect-[4/3] rounded-md border border-slate-200 object-cover dark:border-slate-800"
                    src="https://content.hosheman.com/${image.output}" alt="">
                <div
                    class="absolute bottom-7 start-4 flex translate-x-2 translate-y-2 gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                    <a href="https://content.hosheman.com/${image.output}" data-pswp-width="1000" data-pswp-height="1000"
                       class="view-image js-lightbox-toggle flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             stroke-width="2"
                             stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M10 12a2 2 0 1 0 4 0a2 2 0 0 0 -4 0"></path>
                            <path
                                d="M21 12c-2.4 4 -5.4 6 -9 6c-3.6 0 -6.6 -2 -9 -6c2.4 -4 5.4 -6 9 -6c3.6 0 6.6 2 9 6"></path>
                        </svg>
                    </a>
                    <a class="flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                       href="${server}/dashboard/user/openai/documents/download/image/${image.slug}">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                             stroke="currentColor" class="h-4">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                        </svg><!-- arrow-down-tray - outline - heroicons -->
                    </a>
                    <a class="image-result-delete flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                       href="#" onclick="return deleteImage(this)" data-slug="${image.slug}">
                        <svg width="10" height="9" viewBox="0 0 10 9" fill="var(--lqd-heading-color)"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M9.08789 1.49609L5.80664 4.75L9.08789 8.00391L8.26758 8.82422L4.98633 5.57031L1.73242 8.82422L0.912109 8.00391L4.16602 4.75L0.912109 1.49609L1.73242 0.675781L4.98633 3.92969L8.26758 0.675781L9.08789 1.49609Z"></path>
                        </svg>
                    </a>
                </div>
                <div
                    class="absolute bottom-0 w-full rounded-b-md bg-gradient-to-b from-transparent via-slate-900 via-70% to-slate-900 p-3 opacity-0 transition-all group-hover:opacity-100">
                    <span class="image_prompt line-clamp-2 text-xs font-bold text-white">${image.main_input}</span>
                </div>
            </div>`)

                        } else {
                            $('.results-title').text('ویدئو هایی که ساختی')
                            $('.results').append(`<div class="video-result group relative" data-id=${image.id}>
                <figure
                    class="lqd-video-result-fig relative mb-3 aspect-square overflow-hidden rounded-lg shadow-md transition-all group-hover:-translate-y-1 group-hover:scale-105 group-hover:shadow-lg"
                    data-lqd-skeleton-el
                >
                    <video
                        class="lqd-video-result-video h-full w-full object-cover object-center"
                        loading="lazy"
                    >
                        <source
                            src="https://content.hosheman.com/${image.output}"
                            loading="lazy"
                            type="video/mp4"
                        >
                    </video>
                    <div
                        class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                        <a class="lqd-video-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="${server}/dashboard/user/openai/documents/download/image/${image.slug}" title="دانلود فیلم">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                            </svg><!-- arrow-down-tray - outline - heroicons -->
                        </a>
                        <a class="lqd-video-result-play flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="https://content.hosheman.com/${image.output}" data-fslightbox="video-gallery" title="پخش فیلم">
                            <svg class="h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="2"
                                 stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path
                                    d="M6 4v16a1 1 0 0 0 1.524 .852l13 -8a1 1 0 0 0 0 -1.704l-13 -8a1 1 0 0 0 -1.524 .852z"
                                    stroke-width="0" fill="currentColor"></path>
                            </svg>
                        </a>
                    </div>
                </figure>
            </div>`)

                        }
                    });

                    // Update the offset for the next lazy loading request
                    offset += images.length;


                    if (hasMore) {
                        // Attach a scroll event listener to the window
                        window.addEventListener('scroll', handleScroll);
                    }
                });
        }

        function handleScroll() {
            const scrollY = window.scrollY;
            const windowHeight = window.innerHeight;
            const documentHeight = document.documentElement.scrollHeight;

            if (scrollY + windowHeight >= documentHeight) {
                // Remove the scroll event listener to avoid multiple triggers
                window.removeEventListener('scroll', handleScroll);
                lazyLoadImages();
            }
        }

        lazyLoadImages()

    </script>
@endsection

