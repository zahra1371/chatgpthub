<div class="tab-panel active hidden [&.active]:block" id="ai_speech_to_text">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <div class="flex items-center pb-2">
                    <div class="me-3 h-6">
                        <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                             class="h-full">
                            <path class="fill-blue-300"
                                  d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                            <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                            <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                        </svg>
                    </div>
                    <h5 class="text-lg font-bold text-slate-700 dark:text-white">{{__('AI Speech To Text')}}</h5>
                </div>
                <form action="" method="post">
                    @csrf
                    <div class="-mx-3 -my-2 flex flex-wrap">
                        <div class="py-2 w-full">
                            <label
                                class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                آپلود فایل صوتی </label>
                            <div class="-mx-3 flex flex-wrap">
                                <div class="flex w-full flex-col p-2">
                                    <label
                                        class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                        for="file">
                                        <div class="flex flex-col items-center justify-center py-6">
                                            <svg stroke-width="1.5" class="size-11 mb-4"
                                                 xmlns="http://www.w3.org/2000/svg" width="24"
                                                 height="24" viewBox="0 0 24 24" stroke="currentColor"
                                                 fill="none" stroke-linecap="round"
                                                 stroke-linejoin="round">
                                                <path
                                                    d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                                <path d="M9 15l3 -3l3 3"></path>
                                                <path d="M12 12l0 9"></path>
                                            </svg>
                                            <p class="mb-1 text-sm font-semibold">
                                                فایل صوتی رو انتخاب کن

                                            <p class="file-name mb-0 text-2xs">
                                                (فرمت باید mp3 یا mp4 باشه)
                                            </p>
                                        </div>

                                        <input class="hidden" id="file" type="file"
                                               accept=".mp3, .mp4">
                                    </label>
                                </div>

                                <div class="px-3 py-2">
                                    <button type="button"
                                            data-slug="ai_speech_to_text"
                                            onclick="sendOpenaiGeneratorForm(this);"
                                            class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                        {{__('Lets go')}} </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="w-full lg:w-3/5">
            <div class="flex h-full w-full flex-col py-6 px-6">
                @if(count($userOpenai)>0)
                    <table class="" id="speech-to-text">
                        <thead class="border-b">
                        <tr>
                            <th style="text-align: start">#</th>
                            <th style="text-align: start" class="px-6">نتیجه</th>
                        </tr>
                        </thead>

                        <tbody id="speech-to-text">
                        @foreach($userOpenai as $item)
                            <tr class="border-b">
                                <td class="w-9">
                                    <img src="https://cdn-icons-png.flaticon.com/128/13063/13063633.png" alt="">
                                </td>
                                <td class="py-3">
                                    <p class="px-6">{!! $item->output !!}</p>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div id="content-area"
                         class="flex h-full w-full flex-col items-center justify-center px-6 py-20 text-center">
                        <div class="mb-3 h-16">
                            <img class="h-full" src="{{asset('images/illustration/blank-audio.svg')}}" alt="">
                        </div>
                        <div class="font-medium text-slate-500 dark:text-slate-400">
                            <p class="content_text"> فایل صوتی رو آپلود کن و دکمه بزن بریم رو فشار بده</p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
