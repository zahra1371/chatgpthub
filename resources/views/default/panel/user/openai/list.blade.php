@php
    $auth = Auth::user();
    $plan = $auth->activePlan();
    $plan_type = 'regular';
    $upgrade = false;
    $has_words_credit = $auth->remaining_words > 0 || $auth->remaining_words == -1;
    $has_images_credit = $auth->remaining_images > 0 || $auth->remaining_images == -1;

    if ($plan != null) {
        $plan_type = strtolower($plan->plan_type);
    }


    if ($auth->type != 'admin' && $plan_type === 'regular')
        $upgrade = true;

//    $item_filters = $item->filters;
//
//    if (isFavorited($item->id)) {
//        $item_filters .= ',favorite';
//    }
@endphp

@extends('layout.app')

@section('title')
    هوش من |  {{__('ALL Writers List')}}
@endsection

@section('content')
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="mb-7 flex flex-col items-center">
                <h2 class="mb-2 text-xl font-bold text-slate-700 dark:text-white">{{__('ALL Writers List')}} </h2>
            </div>
            <div class="grid gap-6 xs:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4">
                @foreach($filters as $item)
                    @php
                        $url='images/'.$item->name.'.png'
                    @endphp
{{--                    @php--}}
{{--                        if ($item->type == 'text' || $item->type == 'code'){--}}
{{--                            if ($item->slug == 'ai_article_wizard_generator' && $has_words_credit) {--}}
{{--                                $overlay_link_href = route('dashboard.user.openai.articlewizard.new');--}}
{{--                            } elseif ($has_words_credit) {--}}
{{--                                $overlay_link_href = route('dashboard.user.openai.generator.workbook', $item->slug);--}}
{{--                            }--}}
{{--                        }--}}
{{--                        elseif ((($item->type == 'voiceover' || $item->type == 'audio') && $has_words_credit) || ($item->type == 'image' && $has_images_credit)) {--}}
{{--                                $overlay_link_href = route('dashboard.user.openai.generator', $item->slug);--}}
{{--                                $overlay_link_label = 'Create';--}}
{{--                        } else {--}}
{{--                                $overlay_link_href = '#';--}}
{{--                                $overlay_link_label = 'No Tokens Left';--}}
{{--                        }--}}
{{--                    @endphp--}}
                    <a href="{{!in_array($item->name,['advertisment','file','general','development'])?route('dashboard.user.openai.generator.workbook',$item->name):'#'}}" class="rounded-lg border border-slate-200 bg-white p-5 transition-all hover:-translate-y-1 dark:border-slate-800 dark:bg-slate-950">
                        <div class="mb-2 h-10">
                            <img src="{{asset($url)}}" alt="" class="h-10">
                        </div>
                        <h6 class="mb-2 text-lg font-bold text-slate-700 dark:text-white">{{__($item->fa_name)}} {{in_array($item->name,['advertisment','file','general','development'])?'(به زودی)':''}} </h6>
                        <p class="text-sm text-slate-500 dark:text-slate-400"> {{$item->description}} </p>
                    </a><!-- grid-item -->
                @endforeach
            </div><!-- grid -->
        </div><!-- container -->
    </div>
@endsection
