@extends('layout.app')

@section('title')
    هوش من |  {{__('Edit')}} {{__('File')}}
@endsection
@section('content')
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="-mx-3 mb-7 flex items-center justify-between">
                <div class="px-3">
                    <h2 class="mb-2 text-xl font-bold text-slate-700 dark:text-white"> {{__('Edit')}} {{__('File')}} </h2>
                </div>
            </div>
            <!-- head -->
            <div class="rounded-md border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950">
                <div class="border-b border-slate-200 px-6 py-4 dark:border-slate-800 flex">
                    <h2 class="text-xl font-bold text-slate-700 dark:text-white">
                        <span class="text-base font-normal text-slate-400"> {{__('Edit')}} - </span> {{$workbook->title}}
                    </h2>
                    <ul class="ms-auto flex gap-2">
                        <li>
                            <button class="workbook_download inline-flex items-center justify-center rounded-full bg-slate-200 p-2 text-slate-600 transition-all hover:bg-blue-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-blue-600 hover:dark:text-white"
                                    data-doc-type="doc" data-doc-name="{{$workbook->title}}" title="دانلود">
                                <svg stroke-width="1.5" class="size-5" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M21 12a9 9 0 1 0 -9 9"></path>
                                    <path d="M3.6 9h16.8"></path>
                                    <path d="M3.6 15h8.4"></path>
                                    <path d="M11.578 3a17 17 0 0 0 0 18"></path>
                                    <path d="M12.5 3c1.719 2.755 2.5 5.876 2.5 9"></path>
                                    <path d="M18 14v7m-3 -3l3 3l3 -3"></path>
                                </svg>
                            </button>
                        </li>
                        <li>
                            <button id="workbook_delete" data-slug="{{$workbook->slug}}" onclick="deleteDocument(this)" class="inline-flex items-center justify-center rounded-full bg-slate-200 p-2 text-slate-600 transition-all hover:bg-rose-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-rose-600 hover:dark:text-white">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"></path>
                                </svg><!-- trash - outline - heroicons -->
                            </button>
                        </li>
                        <li>
                            <button id="workbook_copy" title="کپی کردن" class="inline-flex items-center justify-center rounded-full bg-slate-200 p-2 text-slate-600 transition-all hover:bg-rose-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-rose-600 hover:dark:text-white">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-5">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M9 12h3.75M9 15h3.75M9 18h3.75m3 .75H18a2.25 2.25 0 0 0 2.25-2.25V6.108c0-1.135-.845-2.098-1.976-2.192a48.424 48.424 0 0 0-1.123-.08m-5.801 0c-.065.21-.1.433-.1.664 0 .414.336.75.75.75h4.5a.75.75 0 0 0 .75-.75 2.25 2.25 0 0 0-.1-.664m-5.8 0A2.251 2.251 0 0 1 13.5 2.25H15c1.012 0 1.867.668 2.15 1.586m-5.8 0c-.376.023-.75.05-1.124.08C9.095 4.01 8.25 4.973 8.25 6.108V8.25m0 0H4.875c-.621 0-1.125.504-1.125 1.125v11.25c0 .621.504 1.125 1.125 1.125h9.75c.621 0 1.125-.504 1.125-1.125V9.375c0-.621-.504-1.125-1.125-1.125H8.25ZM6.75 12h.008v.008H6.75V12Zm0 3h.008v.008H6.75V15Zm0 3h.008v.008H6.75V18Z"></path>
                                </svg>
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="px-6 pb-6 pt-5">
                    <div class="-mx-3 -my-2 flex flex-wrap items-center">
                        <div class="w-full px-3">
                            <div class="py-2">
                                <label for="workbook_title" class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">{{__('Name')}} {{__('Document')}}</label>
                                <input id="workbook_title" class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                       value="{{$workbook->title}}"/>
                            </div>
                        </div>
                        <div class="w-full px-3">
                            <div class="py-3">
                                <textarea class="tinymce border-0 font-body hidden default" id="workbook_text">
                                {!! $workbook->output !!}
                                </textarea>
                            </div>
                        </div>
                        <div class="w-full px-3 pb-2 pt-2">
                            <button class="inline-flex items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800"
                            type="button" id="workbook_button" onclick="editWorkbook('{{ $workbook->slug }}')"> {{__('Save')}} </button>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div>
@endsection

@section('scripts')
    <script src="{{asset('assets/js/workbook.js')}}"></script>
    <script src="{{ asset('assets/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/js/tinymce-theme-handler.js') }}"></script>
    <script>
        tinymce.init({
            selector: '.default',
            directionality:'rtl',
            promotion: false,
            menubar: false,
            branding: false,
        });

        $('#workbook_copy').click(function () {
            if (tinymce?.activeEditor) {
                tinymce.activeEditor.execCommand('selectAll', true);
                const content = tinymce.activeEditor.selection.getContent({
                    format: 'html',
                });
                navigator.clipboard.writeText(content);
                toastr.success('کپی شد.');
                return;
            }
        })

        function deleteDocument(elm){
            const currenturl = window.location.href;
            const server = currenturl.split('/')[0];
            Swal.fire({
                    title: "هشدار!",
                    text: 'مطمئنی میخوای حذفش کنی؟',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "نه! ولش کن",
                    confirmButtonText: "بله"
                },
            ).then((result) => {
                if (result.isConfirmed) {
                    const delete_url =
                        `${server}/dashboard/user/openai/documents/delete/${$(elm).data('slug')}`;

                    $.ajax({
                        type: "get",
                        url: delete_url,
                        success: function (data) {
                            Swal.fire({
                                title: "حله!",
                                text: 'محتوایی که ساخته بودی حذف شد!',
                                icon: "success",
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "ممنون!"
                            }).then(() => {
                                window.location=server+'/'+data.url
                            });
                        },
                        error: function () {
                            Swal.fire({
                                title: "متاسفم!",
                                text: 'به دلیل مشکلی نشد حذف کنم!',
                                icon: "error",
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "ممنون!"
                            });
                        }
                    });
                }
            })
        }

        $('body').on('click', '.workbook_download', event => {
            const button = event.currentTarget;
            const docType = button.dataset.docType;
            const docName = button.dataset.docName || 'document';

            tinymce.activeEditor.execCommand('selectAll', true);
            const content = tinymce.activeEditor.selection.getContent({
                format: 'html',
            });

            if ( docType === 'pdf' ) {
                return html2pdf()
                    .set({
                        filename: docName
                    })
                    .from(content)
                    .toPdf()
                    .save();
            }

            const html = `
<html ${this.doctype === 'doc'
                ? 'xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40"'
                : ''
            }>
<head>
	<meta charset="utf-8" />
	<title>${docName}</title>
</head>
<body>
	${content}
</body>
</html>`;

            const url = `${docType === 'doc'
                ? 'data:application/vnd.ms-word;charset=utf-8'
                : 'data:text/plain;charset=utf-8'
            },${encodeURIComponent(html)}`;

            const downloadLink = document.createElement('a');
            document.body.appendChild(downloadLink);
            downloadLink.href = url;
            downloadLink.download = `${docName}.${docType}`;
            downloadLink.click();

            document.body.removeChild(downloadLink);
        });
    </script>
@endsection
