@extends('layout.app')

@section('title')
    هوش من |  طرح ها
@endsection

@section('content')
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="-mx-3 mb-7 flex items-center justify-between">
                <div class="px-3">
                    <h2 class="mb-2 text-xl font-bold text-slate-700 dark:text-white"> {{__('Plans')}} </h2>
                </div>
            </div>
            <div class="-mx-3 mb-7">
                <div class="p-3 bg-rose-100 text-center">
                    <h2 class="mb-2 font-bold text-rose-600 dark:text-white"> طرح بیسیک به صورت ماهانه میباشد ولی بقیه
                        طرح ها تا پایان استفاده شما باقی میماند و محدودیت زمانی ندارد. </h2>
                    <h2 class="mb-2 font-bold text-rose-600 dark:text-white">برای ورود به درگاه پرداخت لطفا فیلترشکنت رو
                        خاموش کن. </h2>
                </div>
            </div>
            <!-- head -->
            <div class="-m-3 flex flex-wrap justify-center">
                <input type="hidden" id="planId">
                @foreach ($plans as $plan)
                    <div class="w-full p-3 xs:w-4/5 sm:w-1/2 xl:w-1/4">
                        @if($coupon_50 || $coupon_15)
                            @if($plan->name !== 'بیسیک')
                                @if($plan->name === 'دیزاینر پک')
                                    <div class="ribbon"><span>%{{number_format($coupon_50->discount)}} تخفیف</span>
                                    </div>
                                @else
                                    <div class="ribbon"><span>%{{number_format($coupon_15->discount)}} تخفیف</span>
                                    </div>
                                @endif
                            @endif
                        @endif

                        <div
                            class="h-full rounded-lg border border-slate-200 bg-white p-5 dark:border-slate-800 dark:bg-slate-950
                            @if($activesubid && $activesubid === $plan->id) shadow ring-2 ring-blue-300 @endif">
                            @if($plan->name === 'طلا')
                                <h2 class="w-max bg-gradient-to-r from-blue-600 to-pink-500 bg-clip-text text-xl font-bold text-transparent"> {{$plan->name}} </h2>
                            @else
                                <h2 class="text-xl font-bold text-slate-700 dark:text-white"> {{$plan->name}} </h2>
                            @endif
                            <div class="mt-2 flex items-baseline gap-x-2">
                                <div>

                                    @if($coupon_15 && $coupon_50)
                                        @if($plan->name !== 'بیسیک')
                                            @if($plan->name === 'دیزاینر پک')
                                                <span
                                                    class="text-2xl font-bold tracking-tight text-rose-500 dark:text-white"
                                                    style="text-decoration: line-through"> {{number_format($plan->price)}}</span>
                                                <span
                                                    class="text-xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format($plan->price - ($plan->price * $coupon_50->discount /100))}} {{ currency()->symbol }}</span>
                                            @else
                                                <span
                                                    class="text-2xl font-bold tracking-tight text-rose-500 dark:text-white"
                                                    style="text-decoration: line-through"> {{number_format($plan->price)}}</span>
                                                <span
                                                    class="text-xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format($plan->price - ($plan->price * $coupon_15->discount /100))}} {{ currency()->symbol }}</span>
                                            @endif
                                        @else
                                            <span
                                                class="text-2xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format($plan->price)}} {{ currency()->symbol }}</span>
                                        @endif
                                    @else
                                        <span
                                            class="text-2xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format($plan->price)}} {{ currency()->symbol }}</span>

                                    @endif

                                </div>
                                @if($plan->name === 'بیسیک')
                                    <span
                                        class="text-sm font-semibold leading-6 tracking-wide text-slate-500 dark:text-slate-400"> ({{__('Monthly')}}) </span>
                                @endif
                            </div>
                            <ul class="mt-4 flex flex-col gap-y-3 text-sm font-medium text-slate-500 dark:text-slate-400">
                                @foreach (explode(',', $plan->features) as $item)
                                    <li class="flex gap-x-3">
                                        <span
                                            class="h-5 w-5 flex-shrink-0 rounded-full bg-emerald-100 p-1 dark:bg-emerald-950">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                 stroke-width="1.5" stroke="currentColor" class="h-3 text-emerald-600">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      d="m4.5 12.75 6 6 9-13.5"/>
                                            </svg><!-- check - outline - heroicons  -->
                                        </span>
                                        <span>{{ $item }}</span>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="mt-6">
                                @if($activesubid && (int)$activesubid === $plan->id)
                                    <button onclick="cancelSubscription()"
                                            class="inline-flex rounded bg-emerald-600 text-white px-5 py-2 text-sm font-medium text-slate-700 transition-all dark:bg-slate-700 dark:text-slate-300">
                                        {{__('Cancel Subscription')}}
                                    </button>
                                @else
                                    <button @if(!$activesubid) data-modal="payment"
                                            @endif onclick="@if($activesubid) showHasPlan() @endif "
                                            data-planid="{{$plan->id}}"
                                            {{--                                    <button data-target="#modal" data-planid="{{$plan->id}}"--}}
                                            class="@if(!$activesubid) open-modal-btn @endif inline-flex rounded bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800 pay">
                                        {{__('Choose plan')}}
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
                <div id="payment" class="modal hidden fixed z-10 inset-0 overflow-y-auto"
                     style="background-color: rgba(96,96,96,0.6)">
                    <div class="relative z-10 mt-8" aria-labelledby="modal-title" role="dialog" aria-modal="true">
                        <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

                        <div class="fixed inset-0 z-10 w-screen overflow-y-auto">
                            <div
                                class="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                                <div
                                    class="lg:w-1/2 w-full mt-32 relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8">
                                    <div class="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
                                        <div class="sm:flex sm:items-start">
                                            <div class="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left">
                                                <h3 class="text-base font-semibold leading-6 text-gray-900"
                                                    id="modal-title">{{__('Pay')}} {{__('with')}}</h3>
                                                @foreach ($activeGateways as $gateway)
                                                    <a class="payment">
                                                        <div class="w-full px-3">
                                                            <div class="py-2">
                                                                <div
                                                                    class="flex items-center gap-x-3 rounded-md border border-dashed border-slate-200 p-3 dark:border-slate-800">
                                                                    <div class="flex flex-wrap items-center gap-3">
                                                                        <div data-gateway_code="{{$gateway->code}}"
                                                                             class="flex justify-between w-100 align-middle items-center h-[36px] m-0 p-0 final-payment">
                                                                            <img
                                                                                class="rounded-3xl px-3"
                                                                                src="{{ asset('images/zibal-logo.svg')}}"
                                                                                style="max-height:24px;"
                                                                                alt="{{ $gateway->title }}"
                                                                            />
                                                                            <h6 class="text-sm font-bold text-slate-600 dark:text-slate-200">
                                                                                {{$gateway->title}} </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>

                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- container -->
    </div>
@endsection

@section('scripts')
    <script>
        $('.pay').click(function () {
            $('#planId').val($(this).data('planid'))
        })

        $('.final-payment').click(function (e) {
            let planId = $('#planId').val()
            const baseUrl = window.location.origin
            let href = `${baseUrl}/dashboard/user/payment/subscribe/` + planId + '/' + $(this).data('gateway_code')
            $('.payment').attr('href', href)
        })

        @if(session('success'))
        toastr.success('پرداخت شما با موفقیت انجام شد.')
        @elseif(session('message') || session('error'))
        toastr.error('پرداخت شما موفقیت آمیز نبود. در صورتی که پول از حساب شما کسر شده و تا 48 ساعت آینده به حساب شما بازنگشت، با پشتیبانی تماس بگیرید.')
        @endif

        function showHasPlan() {
            toastr.error('یک طرح فعال داری، برای تهیه اشتراک جدید اول طرح فعال رو کنسل کن.')
        }

        function cancelSubscription() {
            Swal.fire({
                title: "آیا مطمئن هستید؟",
                text: "بعد از کنسل کردن طرح تمام کلمات و تصاویر باقی مانده صفر میشوند",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "بله کنسل کن",
                cancelButtonText: "انصراف"
            }).then(() => {
                let form = {
                    action: "{{ LaravelLocalization::localizeUrl(route('dashboard.user.payment.cancelActiveSubscription')) }}",
                    method: 'get'
                }
                ajax(form)
            });
        }
    </script>
@endsection
