@extends('layout.app')

@section('title')
    هوش من | خانه
@endsection
@section('content')
    <div class="justify-center gap-3 sm:gap-5 sm:pt-2 fixed" style="z-index: 1000; top: 20rem">
        <div class="relative">
            <a target="_blank" href="https://www.instagram.com/hosheman_com?igsh=MWhva25wZG55eXpnbw=="
               class="peer inline-flex rounded-full px-5 py-2 text-sm font-medium text-white transition-all">
                <img src="{{asset('images/insta.png')}}" width="35" alt="">
            </a>
        </div>
        <div class="relative">
            <a target="_blank" href="https://t.me/hoshe_man"
               class="peer inline-flex rounded-full px-5 py-2 text-sm font-medium text-white transition-all">
                <img src="{{asset('images/telegram.png')}}" width="35" alt="">
            </a>
        </div>
        <div class="relative">
            <a href="https://wa.me/989919427053"
               target="_blank"
               class="peer inline-flex rounded-full px-5 py-2 text-sm font-medium text-white transition-all">
                <img src="{{asset('images/whatsapp.png')}}" width="35" alt="">
            </a>
        </div>
    </div>

    <section
        class="relative overflow-hidden bg-gradient-to-br from-white to-blue-100 py-24 dark:from-slate-950 dark:to-blue-950">
        <div class="container px-3">
            <div class="mx-auto max-w-3xl text-center">
                <div
                    class="mb-3 inline-flex rounded-full bg-gradient-to-r from-blue-600 to-green-500 px-2 py-0.5 text-sm font-medium text-white">
                    هوش مصنوعی
                </div>
                <h1 class="text-2xl font-bold text-slate-800 dark:text-white sm:text-3xl md:text-4xl lg:text-[2.5rem]">
                    <span
                        class="mb-3 block text-4xl text-blue-400 md:text-5xl lg:mb-6 lg:text-6xl xl:text-7xl">هوش من</span>
                    <span class="leading-loose sm:leading-relaxed md:leading-[1.4]">تولید کننده</span>
                    <span
                        class="rounded-lg bg-white px-2 py-2 text-[.8em] text-orange-500 shadow-sm">متن</span> ،
                    <span
                        class="whitespace-nowrap rounded-lg bg-white px-2 py-2 text-[.8em] text-lime-600 shadow-sm">عکس</span>
                    و
                    <span
                        class="whitespace-nowrap rounded-lg bg-white px-2 py-2 text-[.8em] text-pink-600 shadow-sm">ویدئو</span>

                </h1>
                <h6 class="mt-8 inline-block w-max rounded-full bg-gradient-to-r from-purple-500 to-blue-500 bg-clip-text text-lg font-bold text-transparent">
                    به دنیای جذاب هوش مصنوعی خوش اومدی</h6>
            </div>
            <ul class="-mb-36 mt-4 flex justify-center gap-7">
                <li class="relative z-10">
                    <a class="-me-7 mt-16 flex -rotate-12 flex-col rounded-xl bg-white p-2 text-center">
                        <img class="w-56 rounded-t-xl border border-slate-100"
                             src="{{asset('images/landing-1.png')}}" alt=""/>
                    </a>
                </li>
                <li>
                    <a class="mt-8 flex flex-col rounded-xl bg-white p-2 text-center">
                        <img class="w-56 rounded-t-xl border border-slate-100"
                             src="{{asset('images/documents.png')}}" alt=""/>
                    </a>
                </li>
                <li class="relative z-10">
                    <a class="-ms-7 mt-16 flex rotate-12 flex-col rounded-xl bg-white p-2 text-center">
                        <img class="w-56 rounded-t-xl border border-slate-100"
                             src="{{asset('images/profile-image.png')}}"
                             alt=""/>
                    </a>
                </li>
            </ul>
        </div>
        <div class="absolute bottom-3 z-10 flex w-full justify-center sm:bottom-4">
            <div
                class="group relative inline-block rounded-full bg-white p-px text-sm/6 font-semibold text-slate-600 shadow-2xl shadow-slate-900">
                        <span class="absolute inset-0 overflow-hidden rounded-full">
                            <span
                                class="absolute inset-0 rounded-full bg-[image:radial-gradient(75%_100%_at_50%_0%,rgba(56,189,248,0.6)_0%,rgba(56,189,248,0)_75%)] opacity-0 transition-opacity duration-500 group-hover:opacity-100"></span>
                        </span>
                <span
                    class="absolute -bottom-0 left-[1.125rem] h-px w-[calc(100%-2.25rem)] bg-gradient-to-r from-emerald-400/0 via-emerald-400/90 to-emerald-400/0 transition-opacity duration-500 group-hover:opacity-40"></span>
            </div>
        </div><!-- container -->
    </section><!-- section -->
    <section class="bg-white py-16 dark:bg-slate-950 lg:py-20">
        <div class="container px-3">
            <div class="pt-0">
                <div class="-m-3 flex flex-wrap items-center justify-center md:-m-6 md:flex-row-reverse">
                    <div class="w-full p-3 sm:w-4/5 md:w-1/2 md:p-6 lg:w-2/5">
                        <img-comparison-slider
                            class="slider-with-shadows rounded-lg border border-slate-200 dark:border-slate-800">
                            <img slot="first" src="{{asset('images/backgrounds/wrong.jpg')}}" alt=""/>
                            <img slot="second" src="{{asset('images/backgrounds/sample.png')}}" alt=""/>
                            <div slot="handle"
                                 class="relative flex h-14 w-14 items-center justify-center gap-2 rounded-full border-2 border-white bg-slate-300 bg-opacity-60 p-2 shadow-sm backdrop-blur-sm">
                                <div
                                    class="h-0 w-0 border-y-8 border-e-[10px] border-b-transparent border-e-white border-t-transparent"></div>
                                <div
                                    class="h-0 w-0 border-y-8 border-s-[10px] border-b-transparent border-s-white border-t-transparent"></div>
                            </div>
                        </img-comparison-slider>
                    </div>
                    <div class="w-full p-3 sm:w-4/5 md:w-1/2 md:p-6 lg:w-2/5">
                        <div
                            class="mb-2 inline-flex rounded bg-gradient-to-r from-yellow-600 to-violet-500 px-2 py-0.5 text-[11px] font-medium text-white">
                            ساخت پس زمینه برای عکس
                        </div>
                        <h3 class="mb-4 text-3xl font-bold !leading-tight text-slate-800 dark:text-white sm:text-[2.5rem] md:text-3xl lg:text-[2.5rem]">
                            برای محصولاتت بکگراندهای جذاب بساز</h3>
                        <p class="text-sm/loose text-slate-500 dark:text-slate-400">بدون نیاز به هیچ تجهیزاتی، عکس
                            محصولت رو آپلود کن و فقط با یه کلیک پس زمینه جذاب بساز.</p>
                        <div class="mt-5">
                            <a href="{{route('dashboard.user.openai.generator.workbook','graphics')}}"
                               class="inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                الان تستش کن</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-12">
                <div class="-m-3 flex flex-wrap items-center justify-center md:-m-6">
                    <div class="w-full p-3 sm:w-4/5 md:w-1/2 md:p-6 lg:w-2/5">
                        <img-comparison-slider
                            class="slider-with-shadows rounded-lg border border-slate-200 dark:border-slate-800">
                            <img slot="first" src="{{asset('images/before.png')}}" alt=""/>
                            <img slot="second" src="{{asset('images/after.png')}}" alt=""/>
                            <div slot="handle"
                                 class="relative flex h-14 w-14 items-center justify-center gap-2 rounded-full border-2 border-white bg-slate-300 bg-opacity-60 p-2 shadow-sm backdrop-blur-sm">
                                <div
                                    class="h-0 w-0 border-y-8 border-e-[10px] border-b-transparent border-e-white border-t-transparent"></div>
                                <div
                                    class="h-0 w-0 border-y-8 border-s-[10px] border-b-transparent border-s-white border-t-transparent"></div>
                            </div>
                        </img-comparison-slider>
                    </div>
                    <div class="w-full p-3 sm:w-4/5 md:w-1/2 md:p-6 lg:w-2/5">
                        <div
                            class="mb-2 inline-flex rounded bg-gradient-to-r from-blue-600 to-pink-500 px-2 py-0.5 text-[11px] font-medium text-white">
                            ساخت عکس
                        </div>
                        <h3 class="mb-4 text-3xl font-bold !leading-tight text-slate-800 dark:text-white sm:text-[2.5rem] md:text-3xl lg:text-[2.5rem]">
                            با مدل قدرتمند DALLE-3 ایده هات رو به عکس تبدیل کن </h3>
                        <p class="text-sm/loose text-slate-500 dark:text-slate-400"> یک بوم جذاب برات آماده کردیم تا
                            ایده هات رو بنویسی و هوش مصنوعی برات خلق کنه </p>
                        <div class="mt-5">
                            <a target="_blank" href="{{route('dashboard.user.openai.generator.workbook','graphics')}}"
                               class="inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                اولین عکست رو بساز</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-12">
                <div class="flex flex-wrap items-center justify-center">
                    <div class="lg:w-4/5">
                        <div class="mx-auto text-center sm:w-2/3 md:w-1/2">
                            <h3 class="mb-3 text-[2.5rem] font-bold leading-tight text-slate-800 dark:text-white">
                                ربات های هوش من </h3>
                            <p class="text-sm/loose text-slate-500 dark:text-slate-400"> کلی ربات شخصی سازی شده با
                                پرامپت نویسی قوی واست آماده کردیم که دیگه دغدغه نداشته باشی. </p>
                        </div>
                        <div class="-m-3 flex flex-wrap justify-center pt-6">
                            <div class="w-full p-3 sm:w-4/5 md:w-1/2">
                                <a target="_blank" href="{{route('dashboard.user.openai.chat.chat','ai-chat-bot')}}"
                                   class="inline-flex">
                                    <img class="rounded-lg border border-slate-200 dark:border-slate-800"
                                         src="{{asset('images/bots-image.png')}}" alt=""/>
                                </a>
                            </div>
                            <div class="p-3 max-md:hidden md:w-1/2">
                                <a href="{{route('dashboard.user.openai.chat.chat','ai-chat-bot')}}"
                                   class="inline-flex h-full">
                                    <img class="rounded-lg border border-slate-200 dark:border-slate-800"
                                         src="{{asset('images/bots-details.png')}}" alt=""/>
                                </a>
                            </div>
                        </div>
                        <div class="mt-8 text-center">
                            <a target="_blank" href="{{route('login')}}"
                               class="inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">بزن
                                بریم!</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-12">
                <div class="flex flex-wrap items-center justify-center">
                    <div class="w-full lg:w-4/5">
                        <div class="mx-auto w-full pb-8 text-center md:w-3/4 lg:w-1/2">
                            <h3 class="mb-3 text-[2.5rem] font-bold leading-tight text-slate-800 dark:text-white">
                                هوش من ابزارهای مختلف برای هر شغلی داره</h3>
                            <div class="flex flex-wrap justify-center gap-2">
                                <div
                                    class="mb-2 inline-flex rounded bg-blue-200 px-2 py-0.5 text-[11px] font-medium text-blue-600">
                                    برنامه نویسی
                                </div>
                                <div
                                    class="mb-2 inline-flex rounded bg-pink-200 px-2 py-0.5 text-[11px] font-medium text-pink-600">
                                    صدا به متن
                                </div>
                                <div
                                    class="mb-2 inline-flex rounded bg-green-200 px-2 py-0.5 text-[11px] font-medium text-green-600">
                                    تولید مقاله
                                </div>
                            </div>
                        </div>
                        <div class="-m-4 flex flex-wrap justify-center text-center">
                            <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                                <a target="_blank" href=""
                                   class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                    <img
                                        class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-blue-100 group-hover:dark:shadow-blue-950"
                                        src="{{asset('images/code-gen-blank.png')}}" alt=""/>
                                    <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                        هوش برنامه نویس </h6>
                                </a>
                            </div>
                            <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                                <a target="_blank" href=""
                                   class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                    <img
                                        class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                        src="{{asset('images/audio.png')}}" alt=""/>
                                    <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                        هوش صدا به متن </h6>
                                </a>
                            </div>
                            <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                                <a target="_blank" href=""
                                   class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                    <img
                                        class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-green-100 group-hover:dark:shadow-green-950"
                                        src="{{asset('images/copywrite-gen.png')}}" alt=""/>
                                    <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                        هوش مقاله نویس </h6>
                                </a>
                            </div>
                        </div>

                        <div class="w-full py-10 text-center">
                            <h3 class="mb-3 text-[2.5rem] font-bold leading-tight text-slate-800 dark:text-white">
                                مشاغل هدف</h3>

                            <div class="max-lg:grid-cols-2 max-md:grid-cols-1 grid grid-cols-3 gap-4">
                                <div
                                    class="lqd-color-box flex items-center py-5 px-9 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-blue-600 bg-blue-200 rounded-2xl bg-opacity-[0.07] hover:shadow-[#cba15326]">
                                    <span
                                        class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg !bg-current"></span>
                                    <h3 class="text-xl text-inherit -tracking-tight">ادمین های اینستاگرام</h3>
                                </div>
                                <div
                                    class="lqd-color-box flex items-center py-5 px-9 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-pink-600 rounded-2xl bg-pink-200 bg-opacity-[0.07] hover:shadow-[#ab7fe621]">
                                    <span
                                        class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg !bg-current"></span>
                                    <h3 class="text-xl text-inherit -tracking-tight">طراحان محصول</h3>
                                </div>
                                <div
                                    class="lqd-color-box flex items-center py-5 px-9 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-green-600 rounded-2xl bg-green-200 bg-opacity-[0.07] hover:shadow-[#57cbc624]">
                                    <span
                                        class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg !bg-current"></span>
                                    <h3 class="text-xl text-inherit -tracking-tight">کارآفرینان</h3>
                                </div>
                                <div
                                    class="lqd-color-box flex items-center py-5 px-9 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-orange-500 rounded-2xl bg-orange-200 bg-opacity-[0.07] hover:shadow-[#7f8fe624]">
                                    <span
                                        class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg !bg-current"></span>
                                    <h3 class="text-xl text-inherit -tracking-tight">گرافیست ها</h3>
                                </div>
                                <div
                                    class="lqd-color-box flex items-center py-5 px-9 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-emerald-600 rounded-2xl bg-emerald-100 bg-opacity-[0.07] hover:shadow-[#6bac6524]">
                                    <span
                                        class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg !bg-current"></span>
                                    <h3 class="text-xl text-inherit -tracking-tight">دانش آموزان و معلمان</h3>
                                </div>
                                <div
                                    class="lqd-color-box flex items-center py-5 px-9 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-red-600 rounded-2xl bg-red-200 bg-opacity-[0.07] hover:shadow-[#ef793a1f]">
                                    <span
                                        class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg !bg-current"></span>
                                    <h3 class="text-xl text-inherit -tracking-tight">برنامه نویس ها</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->
    <section id="Features" class="bg-slate-100 py-16 dark:bg-slate-900 lg:py-20">
        <div class="container px-3 text-center">
            <div
                class="mb-2 inline-flex rounded-full bg-gradient-to-r from-green-600 to-pink-500 px-2 py-0.5 text-[11px] font-medium text-white">
                ویژگی ها
            </div>
            <div class="flex flex-wrap items-center justify-center">
                <div class="lg:w-4/5">
                    <div class="xxl:w-1/2 mx-auto w-full pb-8 text-center xl:w-2/3">
                        <h3 class="mb-3 text-[2.5rem] font-bold leading-tight text-slate-800 dark:text-white">
                            ویژگی های سایت هوش من
                        </h3>
                    </div>
                    <div class="-m-4 flex flex-wrap justify-center text-center">
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/pro-dashboard.jpg')}}" alt=""/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    داشبورد حرفه ای </h6>
                            </a>
                        </div>
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/language.png')}}" alt=""/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    پشتیبانی بیش از 50 زبان </h6>
                            </a>
                        </div>
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/payment.jpg')}}" alt=""/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    پرداخت امن با زیبال </h6>
                            </a>
                        </div>
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/referral.jpg')}}" alt=""/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    امکان دعوت از دوستان(به زودی) </h6>
                            </a>
                        </div>
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/save-docs.png')}}" alt=""/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    دانلود و ذخیره محتوای تولید شده </h6>
                            </a>
                        </div>
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/support.jpg')}}" alt=""/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    پشتیبانی سریع </h6>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->

    <section id="plans" class="overflow-hidden bg-white pb-2 pt-16 dark:bg-slate-900 md:pt-20 lg:pt-24 xl:pt-28">
        <div class="container px-3 text-center">
            <div
                class="mb-2 inline-flex rounded-full bg-gradient-to-r from-green-600 to-pink-500 px-2 py-0.5 text-[11px] font-medium text-white">
                طرح ها
            </div>
            <div class="flex flex-wrap items-center justify-center pb-8 lg:pb-12">
                <div class="mx-auto w-full text-center sm:w-4/5 md:w-4/5 lg:w-3/5 xl:w-1/2 2xl:w-2/5">
                    <h3 class="mb-3 text-3xl font-bold leading-tight text-slate-700 dark:text-white sm:text-[2.5rem]">
                        لیست طرح ها</h3>
                </div>
            </div>
            <div class="-m-3 flex flex-wrap justify-center md:-m-4">
                <div class="w-full p-3 md:p-4 lg:w-1/4 xl:w-1/4">
                    @if($coupon_15 && $coupon_15->code === 'friday')
                        <div class="ribbon"><span>%{{number_format($coupon_15->discount)}} تخفیف</span></div>
                    @endif
                    <div
                        class="rounded-2xl border border-slate-100 bg-white p-7 shadow dark:border-slate-950 dark:bg-slate-950">
                        <h2 class="text-3xl/snug font-bold text-blue-600">بیسیک</h2>
                        <div class="mb-6 mt-2 flex items-baseline gap-x-2">
                            <div>
                                @if($coupon_15 && $coupon_15->code === 'friday')
                                    <span class="text-2xl font-bold tracking-tight text-rose-500 dark:text-white"
                                          style="text-decoration: line-through"> {{number_format(69000)}} </span>
                                    <span class="text-xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format(69000 - (69000*$coupon_15->discount / 100))}} تومان </span>
                                @else
                                    <span class="text-2xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format(69000)}} تومان </span>
                                @endif
                            </div>
                            <span
                                class="text-sm font-semibold leading-6 tracking-wide text-slate-500 dark:text-slate-400"> ماهیانه</span>
                        </div>
                        <ul class="flex flex-col gap-y-3 text-sm font-medium text-slate-500 dark:text-slate-400">
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(5000)}}</strong> &nbsp;کلمه </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(5)}}</strong> &nbsp;عکس </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(0)}}</strong> &nbsp;ساخت عکس محصول </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>GPT-4O</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>DALL-E3</span>
                            </li>

                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>دسترسی به تمام ربات ها</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>دسترسی به تمام قالب ها</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>پشتیبانی سریع</span>
                            </li>
                        </ul>

                        <div class="mt-5">
                            <button
                                class="inline-flex w-full justify-center rounded-lg border border-slate-200 bg-white px-7 py-3 text-base font-bold text-slate-600 transition-all hover:border-blue-600 hover:bg-blue-600 hover:text-white dark:border-slate-800 dark:bg-slate-900 dark:text-slate-300 hover:dark:border-blue-600 hover:dark:bg-blue-600 hover:dark:text-white">
                                خرید بیسیک
                            </button>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3 md:p-4 lg:w-1/4 xl:w-1/4">
                    @if($coupon_15)
                        <div class="ribbon"><span>%{{number_format($coupon_15->discount)}} تخفیف</span></div>
                    @endif
                    <div
                        class="rounded-2xl border border-slate-100 bg-white p-7 shadow dark:border-slate-950 dark:bg-slate-950">
                        <h2 class="text-3xl/snug font-bold text-blue-600">پرو</h2>
                        <div class="mb-6 mt-2 flex items-baseline gap-x-2">
                            <div>
                                @if($coupon_15)
                                    <span class="text-2xl font-bold tracking-tight text-rose-500 dark:text-white"
                                          style="text-decoration: line-through"> {{number_format(530000)}} </span>
                                    <span class="text-xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format(530000 - (530000*$coupon_15->discount / 100))}} تومان </span>
                                @else
                                    <span class="text-2xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format(530000)}} تومان </span>
                                @endif
                            </div>
                        </div>
                        <ul class="flex flex-col gap-y-3 text-sm font-medium text-slate-500 dark:text-slate-400">
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(20000)}}</strong> &nbsp;کلمه </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(30)}}</strong> &nbsp;عکس </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(5)}}</strong> &nbsp;ساخت عکس محصول </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>GPT-4O</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>DALL-E3</span>
                            </li>

                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>دسترسی به تمام ربات ها</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>دسترسی به تمام قالب ها</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>پشتیبانی سریع</span>
                            </li>
                        </ul>

                        <div class="mt-5">
                            <button
                                class="inline-flex w-full justify-center rounded-lg border border-slate-200 bg-white px-7 py-3 text-base font-bold text-slate-600 transition-all hover:border-blue-600 hover:bg-blue-600 hover:text-white dark:border-slate-800 dark:bg-slate-900 dark:text-slate-300 hover:dark:border-blue-600 hover:dark:bg-blue-600 hover:dark:text-white">
                                خرید پرو
                            </button>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3 md:p-4 lg:w-1/4 xl:w-1/4">
                    @if($coupon_15)
                        <div class="ribbon"><span>%{{number_format($coupon_15->discount)}} تخفیف</span></div>
                    @endif
                    <div
                        class="rounded-2xl border border-slate-100 bg-white p-7 shadow ring-2 ring-blue-300 dark:border-slate-950 dark:bg-slate-950">
                        <h2 class="w-max bg-gradient-to-r from-blue-600 to-pink-500 bg-clip-text text-2xl/snug font-bold text-transparent">پرمیوم</h2>
                        <div class="mb-6 mt-2 flex items-baseline gap-x-2">
                            <div>
                                @if($coupon_15)
                                    <span class="text-2xl font-bold tracking-tight text-rose-500 dark:text-white"
                                          style="text-decoration: line-through"> {{number_format(1030000)}} </span>
                                    <span class="text-xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format(1030000 - (1030000*$coupon_15->discount / 100))}} تومان </span>
                                @else
                                    <span class="text-2xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format(1030000)}} تومان </span>
                                @endif
                            </div>
                        </div>
                        <ul class="flex flex-col gap-y-3 text-sm font-medium text-slate-500 dark:text-slate-400">
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(40000)}}</strong> &nbsp;کلمه </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(40)}}</strong> &nbsp;عکس </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(15)}}</strong> &nbsp;ساخت عکس محصول </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>GPT-4O</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>DALL-E3</span>
                            </li>

                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>دسترسی به تمام ربات ها</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>دسترسی به تمام قالب ها</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>پشتیبانی سریع</span>
                            </li>
                        </ul>

                        <div class="mt-5">
                            <button
                                class="inline-flex w-full justify-center rounded-lg bg-blue-600 px-7 py-3 text-base font-bold text-white transition-all hover:bg-blue-800">
                                خرید پرمیوم
                            </button>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3 sm:w-1/2 md:p-4 lg:w-1/4 xl:w-1/4">
                    @if($coupon_50)
                        <div class="ribbon"><span>%{{number_format($coupon_50->discount)}} تخفیف</span></div>
                    @endif
                    <div
                        class="rounded-2xl border border-slate-100 bg-white p-7 shadow dark:border-slate-950 dark:bg-slate-950 dark:ring-blue-900">
                        <h2 class="text-3xl/snug font-bold text-blue-600">
                            دیزاینر پک </h2>
                        <div class="mb-6 mt-2 flex items-baseline gap-x-2">
                            <div>
                                @if($coupon_50)
                                    <span class="text-2xl font-bold tracking-tight text-rose-500 dark:text-white"
                                          style="text-decoration: line-through"> {{number_format(3420000)}} </span>
                                    <span class="text-xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format(3420000 - (3420000*$coupon_50->discount / 100))}} تومان </span>
                                @else
                                    <span class="text-2xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format(3420000)}} تومان </span>
                                @endif
                            </div>
                        </div>
                        <ul class="flex flex-col gap-y-3 text-sm font-medium text-slate-500 dark:text-slate-400">
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(30000)}}</strong> &nbsp;کلمه </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(150)}}</strong> &nbsp;عکس </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(50)}}</strong> &nbsp;ساخت عکس محصول </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>GPT-4O</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>DALLE-3</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>دسترسی به همه ربات ها</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>دسترسی به همه قالب ها</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>پشتیبانی سریع</span>
                            </li>
                        </ul>
                        <div class="mt-7">
                            <button
                                class="inline-flex w-full justify-center rounded-lg border border-slate-200 bg-white px-7 py-3 text-base font-bold text-slate-600 transition-all hover:border-blue-600 hover:bg-blue-600 hover:text-white dark:border-slate-800 dark:bg-slate-900 dark:text-slate-300 hover:dark:border-blue-600 hover:dark:bg-blue-600 hover:dark:text-white">
                                خرید دیزاینر پک
                            </button>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3 md:p-4 lg:w-1/4 xl:w-1/4">
                    @if($coupon_15)
                        <div class="ribbon"><span>%{{number_format($coupon_15->discount)}} تخفیف</span></div>
                    @endif
                    <div
                        class="rounded-2xl border border-slate-100 bg-white p-7 shadow dark:border-slate-950 dark:bg-slate-950">
                        <h2 class="text-3xl/snug font-bold text-blue-600">نویسا پک</h2>
                        <div class="mb-6 mt-2 flex items-baseline gap-x-2">
                            <div>
                                @if($coupon_15)
                                    <span class="text-2xl font-bold tracking-tight text-rose-500 dark:text-white"
                                          style="text-decoration: line-through"> {{number_format(1920000)}} </span>
                                    <span class="text-xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format(1920000 - (1920000*$coupon_15->discount / 100))}} تومان </span>
                                @else
                                    <span class="text-2xl font-bold tracking-tight text-slate-700 dark:text-white"> {{number_format(1920000)}} تومان </span>
                                @endif
                            </div>
                            <span
                                class="text-sm font-semibold leading-6 tracking-wide text-slate-500 dark:text-slate-400"> ماهیانه</span>
                        </div>
                        <ul class="flex flex-col gap-y-3 text-sm font-medium text-slate-500 dark:text-slate-400">
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200"> بدون محدودیت</strong> &nbsp;کلمه </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(50)}}</strong> &nbsp;عکس </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>
                                            <strong
                                                class="text-slate-600 dark:text-slate-200">{{number_format(20)}}</strong> &nbsp;ساخت عکس محصول </span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>GPT-4O</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>DALLE-3</span>
                            </li>

                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>دسترسی به تمام ربات ها</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>دسترسی به تمام قالب ها</span>
                            </li>
                            <li class="flex gap-x-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="h-5 text-blue-500">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"/>
                                </svg>
                                <span>پشتیبانی سریع</span>
                            </li>
                        </ul>

                        <div class="mt-5">
                            <button
                                class="inline-flex w-full justify-center rounded-lg border border-slate-200 bg-white px-7 py-3 text-base font-bold text-slate-600 transition-all hover:border-blue-600 hover:bg-blue-600 hover:text-white dark:border-slate-800 dark:bg-slate-900 dark:text-slate-300 hover:dark:border-blue-600 hover:dark:bg-blue-600 hover:dark:text-white">
                                نویسا پک
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->

    <section id="faq" class="bg-slate-100 overflow-hidden py-16 dark:bg-slate-900 md:pt-20 lg:pt-24 xl:pt-28">
        <div class="container px-3 text-center">
            <div
                class="mb-2 inline-flex rounded-full bg-gradient-to-r from-green-600 to-pink-500 px-2 py-0.5 text-[11px] font-medium text-white">
                سوالات متداول
            </div>
            <div class="flex flex-wrap items-center justify-center pb-8 lg:pb-10">
                <div class="text- mx-auto text-center sm:w-2/3 md:w-3/5 lg:w-2/5">
                    <h3 class="mb-3 text-3xl font-bold leading-tight text-slate-700 dark:text-white"> سوالی داری؟ </h3>
                    <p class="text-sm/loose text-slate-500 dark:text-slate-400">اگر جواب سوالت اینجا نبود به پشتیبانی
                        پیام بده</p>
                </div>
            </div>
            <div class="-m-3 flex flex-wrap justify-center md:-m-4">
                <div class="w-full p-3 md:p-4 xl:w-3/4">
                    <div class="accordion flex flex-col gap-3">
                        @foreach($faq as $item)
                            <div
                                class="accordion-item @if($item->id ===1) active @endif group rounded-2xl border border-slate-100 bg-white shadow dark:border-slate-950 dark:bg-slate-950">
                                <button
                                    class="accordion-toggle flex w-full items-center justify-between px-6 py-4 text-start font-bold text-slate-600 dark:text-slate-200">
                                    <span
                                        class="block text-base font-bold text-slate-600 group-[.active]:text-slate-700 dark:text-slate-300 group-[.active]:dark:text-white"> {{$item->question}} </span>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                         class="ms-2 h-2.5 transition-all group-[.active]:rotate-180">
                                        <path fill="currentColor"
                                              d="M233.4 406.6c12.5 12.5 32.8 12.5 45.3 0l192-192c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L256 338.7 86.6 169.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l192 192z"/>
                                    </svg>
                                </button><!-- accordion-toggle -->
                                <div
                                    class="accordion-body px-6 pb-5 text-base text-slate-500 group-[.active]:block dark:text-slate-400">
                                    <div class="max-w-3xl">
                                        <p class="text-base/7"> {{$item->answer}} </p>
                                    </div>
                                </div><!-- accordion-body -->
                            </div><!-- accordion-item -->
                        @endforeach
                    </div><!-- accordion -->
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->

    <section class="overflow-hidden bg-slate-100 dark:bg-slate-900">
        <div class="container px-3">
            <div
                class="relative rounded-t-xl border border-b-0 border-gray-200 bg-gradient-to-b from-slate-100 to-white p-6 dark:border-gray-800 dark:from-slate-900 dark:to-slate-950 sm:p-10">
                <div class="mx-auto max-w-3xl text-center">
                    <h1 class="text-[2rem] font-bold text-slate-800 dark:text-white">
                            <span
                                class="mb-6 block text-2xl !leading-snug text-slate-800 dark:text-white sm:text-3xl md:text-4xl lg:text-[2rem] xl:text-5xl">با <span
                                    class="text-cyan-500">هوش من</span> <br> هوشمندانه کار کن!</span>
                    </h1>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->
@endsection
