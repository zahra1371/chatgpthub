<div
    class="fixed start-0 top-0 isolate z-[1020] w-full border-b border-slate-200 bg-white px-3 py-4 dark:border-slate-800 dark:bg-slate-950 xl:py-3">
    <div class="container px-3">
        <div class="w-100 flex items-center justify-between">
            <div class="flex items-center gap-x-2">
                <div class="-ms-1.5 xl:hidden">
                    <button
                        class="header-toggle inline-flex h-8 w-8 items-center justify-center overflow-hidden rounded-full text-slate-400 transition-all hover:bg-slate-200 hover:text-slate-600 hover:dark:bg-slate-800 hover:dark:text-slate-200 [&.active]:bg-slate-200 [&.active]:text-slate-600 [&.active]:dark:bg-slate-800 [&.active]:dark:text-slate-200">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                             stroke="currentColor" class="h-5">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"/>
                        </svg><!-- bars-3 - outline - heroicons -->
                    </button>
                </div>
                <a href="/" class="flex-shrink-0">
                    <div class="flex items-center">
                        <img class="h-14 dark:hidden" src="{{asset('images/logo.png')}}" alt="logo"/>
                    </div>
                </a>
            </div>
            <div
                class="main-header peer fixed start-0 top-0 z-[1020] flex h-screen w-64 flex-shrink-0 -translate-x-full flex-col border-e border-slate-200 bg-white py-4 dark:border-slate-800 dark:bg-slate-950 max-xl:overflow-auto max-xl:transition-all xl:static xl:h-auto xl:w-auto xl:!translate-x-0 xl:border-e-0 xl:py-0 xl:transition-none rtl:translate-x-full [&.header-visible]:translate-x-0">
                <ul class="menu-head flex flex-col gap-x-6 px-4 xl:flex-row xl:items-center">
                    <li class="menu-item group relative">
                        <a href="{{route("index")}}"
                           class="menu-link flex items-center py-2 text-sm font-medium text-slate-700 group-hover:text-blue-600  hover:text-blue-600 dark:text-slate-100 group-[.active>]:dark:text-blue-600 hover:dark:text-blue-600 xl:py-3">
                            <span>خانه</span>
                        </a>
                    </li>

                    <li class="menu-item group relative">
                        <a href="#Features"
                           class="menu-link flex items-center py-2 text-sm font-medium text-slate-700 group-hover:text-blue-600  hover:text-blue-600 dark:text-slate-100 group-[.active>]:dark:text-blue-600 hover:dark:text-blue-600 xl:py-3">
                            <span>{{__('Features')}}</span>
                        </a>
                    </li>
                    <li class="menu-item group relative">
                        <a href="#plans"
                           class="menu-link flex items-center py-2 text-sm font-medium text-slate-700 group-hover:text-blue-600  hover:text-blue-600 dark:text-slate-100 group-[.active>]:dark:text-blue-600 hover:dark:text-blue-600 xl:py-3">
                            <span>{{__('Plans')}}</span>
                        </a>
                    </li>
                    <li class="menu-item group relative">
                        <a href="#faq"
                           class="menu-link flex items-center py-2 text-sm font-medium text-slate-700 group-hover:text-blue-600  hover:text-blue-600 dark:text-slate-100 group-[.active>]:dark:text-blue-600 hover:dark:text-blue-600 xl:py-3">
                            <span>{{__('FAQ')}}</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div
                class="header-toggle pointer-events-none fixed inset-0 z-[1019] bg-slate-950 bg-opacity-50 opacity-0 peer-[.header-visible]:pointer-events-auto peer-[.header-visible]:opacity-100 xl:!opacity-0"></div>
            <!-- overlay -->
            <ul class="flex items-center gap-x-3 lg:gap-x-5">
                <li class="relative inline-flex">
                    <a href="{{route('login')}}"
                       class="inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                        <span class="xs:hidden">{{__('Sign in')}}</span>
                        <span class="hidden xs:inline">{{__('Sign in')}}</span>
                    </a>
                </li>
                <li class="relative inline-flex">
                    <a href="{{route('register')}}"
                       class="inline-flex rounded-full bg-orange-600 px-2 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                        <span class="xs:hidden">{{__('Sign up')}}</span>
                        <span class="hidden xs:inline">{{__('Sign up')}}</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- container -->
</div>
