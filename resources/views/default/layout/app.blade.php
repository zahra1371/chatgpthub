<!DOCTYPE html>
<html lang="zxx" id="pageroot" class="scroll-smooth">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="stream-url"
          content="{{ \App\Helpers\Classes\Helper::routeWithoutTranslate($streamUrl ?? route('dashboard.user.openai.stream')) }}">
    <meta name="description" content="هوش من، اولین پلتفرم هوش مصنوعی ایرانی">
    <!-- Fav Icon  -->
    <link rel="icon" type="image/svg+xml" href="{{asset('images/favicon.png')}}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{asset('assets/css/app.css?v1.0.0')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins/toastr.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins/datatables.min.css')}}">
    <script type="text/javascript">
        !function(){var i="JnNmdJ",a=window,d=document;function g(){var g=d.createElement("script"),s="https://www.goftino.com/widget/"+i,l=localStorage.getItem("goftino_"+i);g.async=!0,g.src=l?s+"?o="+l:s;d.getElementsByTagName("head")[0].appendChild(g);}"complete"===d.readyState?g():a.attachEvent?a.attachEvent("onload",g):a.addEventListener("load",g,!1);}();
    </script>
</head>

<body class="min-w-[320px] bg-slate-100 dark:bg-slate-900" dir="rtl">
<div id="root">
    <div class="flex min-h-screen max-w-full flex-col overflow-x-hidden pt-[calc(theme(space.16)+theme(space.1))]"
         id="#pagecontent">
        <div id="firstLoginModal" class="modal hidden fixed z-10 inset-0 overflow-y-auto"
             style="background-color: rgba(96,96,96,0.6)">
            <div class="relative z-10 mt-8" aria-labelledby="modal-title" role="dialog" aria-modal="true">
                <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

                <div class="fixed inset-0 z-10 w-screen overflow-y-auto">
                    <div
                        class="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                        <div
                            class="lg:w-1/2 w-full mt-32 relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8">
                            <div class="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
                                <div class="sm:flex sm:items-start">
                                    <div class="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left">
                                        <p> {{auth()->user()?auth()->user()->name:''}} عزیز، به سایت <span class="text-danger font-bold"> هوش من</span> خوش
                                            اومدی ❤️</p>
                                        <p>برای اینکه تجربه بهتری داشته باشی و بتونی سایت رو تست کنی برات 500 کلمه و بی نهایت تصویر رایگان در نظر
                                            گرفتیم.</p>
                                        <p>امیدوارم تجربه خوبی برات باشه.</p>
                                        <button class="inline-flex w-full items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800 mt-4" id="close-first-login" type="button" data-bs-dismiss="modal"
                                                aria-label="Close">بزن بریم!
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- header -->

        @if(auth()->user())
            @include('layout.panel.user.header')
        @else
            @include('layout.header')
        @endif

        @include('components.toast')
        <!-- end header -->

        @yield('content')

        <!-- footer -->
        @if(Request::is('/'))
            @include('layout.footer')
        @endif
        <!-- end footer -->
    </div><!-- page-content -->
</div><!-- root -->
<!-- JavaScript -->
<script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/functions/forms.js')}}"></script>
<script src="{{asset('assets/js/functions/myFunctions.js')}}"></script>
<script src="{{asset('assets/js/bundle.js?v1.0.0')}}"></script>
<script src="{{asset('assets/js/scripts.js?v1.0.0')}}"></script>
<script src="{{asset('assets/js/plugins/sweetalert2.js')}}"></script>
<script src="{{ asset('assets/js/plugins/toastr.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/datatables.min.js') }}"></script>


<script>
    $(document).ready(function () {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-bottom-center",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 500,
            "timeOut": 2000,
            "extendedTimeOut": 1000
        }
        @if(auth()->user() && !auth()->user()->first_login)
        $('#firstLoginModal').removeClass('hidden')
        @endif
        $('#close-first-login').click(function () {
            $.ajax({
                type: "get",
                url: '/dashboard/user/change-first-login',
                success: function (response) {
                    if (response.status === 200 || response.status === 100) {
                        window.location.reload()
                    } else if (response.status == 500)
                        swal('خطا!', response['msg'], 'error');
                },
                error: function (xhr) {

                }
            })
        })
    })
</script>

@yield('scripts')
</body>

</html>
