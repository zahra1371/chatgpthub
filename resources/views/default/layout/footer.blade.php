<div
    class="relative isolate border-t border-slate-200 bg-white px-6 py-4 dark:border-slate-800 dark:bg-slate-950">
    <div class="container px-3">
        <div class="flex items-center justify-between">
            <p class="text-xs font-medium text-slate-600 dark:text-white">   ساخته شده توسط تیم &nbsp; <a
                    class="text-slate-700 transition-all hover:text-blue-600 dark:text-white hover:dark:text-blue-600"
                    href="https://themeforest.net/user/themeyn/portfolio" target="_blank"> هوش من </a>
            </p>
            <a referrerpolicy='origin' target='_blank' href='https://trustseal.enamad.ir/?id=510090&Code=pNPFhPk8cp6gbxgOHPK0GceELPY0Jd6G'><img referrerpolicy='origin' src='https://trustseal.enamad.ir/logo.aspx?id=510090&Code=pNPFhPk8cp6gbxgOHPK0GceELPY0Jd6G' alt='' style='cursor:pointer' code='pNPFhPk8cp6gbxgOHPK0GceELPY0Jd6G'></a>
            <ul class="-mx-3 flex flex-wrap">
                <li>
                    <a href="#"
                       class="px-3 text-sm font-medium text-slate-600 transition-all hover:text-blue-600 dark:text-slate-100 hover:dark:text-blue-600">
                        قوانین </a>
                </li>
                <li>
                    <a href="#"
                       class="px-3 text-sm font-medium text-slate-600 transition-all hover:text-blue-600 dark:text-slate-100 hover:dark:text-blue-600">
                        درباره ما </a>
                </li>
            </ul>
        </div>
    </div>
</div>
