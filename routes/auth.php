<?php
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Auth\AuthenticationController;
use App\Http\Controllers\MailController;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]], function() {
    Route::middleware('guest')->group(function () {
        Route::get('register', [AuthenticationController::class, 'registerForm'])
            ->name('register');
        Route::post('register', [AuthenticationController::class, 'registerStore']);

        Route::get('login', [AuthenticationController::class, 'loginForm'])
            ->name('login');
        Route::post('login', [AuthenticationController::class, 'login']);

        Route::get('verify-mobile-code', [AuthenticationController::class, 'verifyForm'])
            ->name('verify.form');
		Route::post('verify-mobile-code', [AuthenticationController::class, 'verifyMobileCode'])
            ->name('verify');
        Route::get('resend-code', [AuthenticationController::class, 'resendCode'])
            ->name('resend-code');

    });

    Route::middleware('auth')->group(function () {

        Route::get('logout', [AuthenticationController::class, 'logout'])
            ->name('logout');
    });
});


if (file_exists(base_path('routes/custom_routes_auth.php'))) {
    include base_path('routes/custom_routes_auth.php');
}
