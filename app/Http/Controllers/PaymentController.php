<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Gateways\ZibalController;
use App\Models\Activity;
use App\Models\Currency;
use App\Models\CustomSettings;
use App\Models\Gateways;
use App\Models\GatewayProducts;
use App\Models\PaymentPlans;
use App\Models\Setting;
use App\Models\HowitWorks;
use App\Models\YokassaSubscriptions as YokassaSubscriptionsModel;
use App\Models\Subscriptions as SubscriptionsModel;
use App\Models\User;
use App\Models\UserAffiliate;
use App\Models\UserOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Laravel\Cashier\Subscription;
use Illuminate\Support\Facades\Log;


/**
 * Controls ALL Payment actions
 */
class PaymentController extends Controller
{

    /**
     * Checks subscription table if given plan is active on user (already subscribed)
     */
    function isActiveSubscription(): bool
    {
        $userId = Auth::user()->id;
        // Get current active subscription
        $activeSub = SubscriptionsModel::query()->where([['status', '=', 'active'], ['user_id', '=', $userId]])->orWhere([['status', '=', 'trialing'], ['user_id', '=', $userId]])->first();
        if ($activeSub != null) {
            $activesubid = $activeSub->id;
        } else {
            $activesubid = 0; //id can't be zero, so this will be easy to check instead of null
        }

        return $activesubid;
    }

    public function startSubscriptionProcess($planId, $gatewayCode)
    {
        $plan = PaymentPlans::query()->where('id', $planId)->first();
        if ($plan != null) {
            if (self::isActiveSubscription()) {
                return back()->with('warning', 'شما یک اشتراک فعال دارید. لطفاً قبل از خرید اشتراک جدید، آن را کنسل کنید.');
            }
            if ($gatewayCode == 'zibal') {
                return ZibalController::payment($planId, $plan);
            }
        }
        abort(404);
    }

    public static function cancelActiveSubscription()
    {
        try {
            $user = Auth::user();
            $userId = $user->id;
            // Get current active subscription
            $activeSub = SubscriptionsModel::query()->where([['status', '=', 'active'], ['user_id', '=', $userId]])->orWhere([['status', '=', 'trialing'], ['user_id', '=', $userId]])->first();
            if ($activeSub == null) {
                return response()->json(['status' => 404, 'message' => 'طرح وجود ندارد']);
            } else {
                $gatewayCode = $activeSub->order->payment_type;
            }

            if ($gatewayCode == 'Zibal') {
                return ZibalController::subscribeCancel();
            }
        } catch (\Exception $exception) {
            error_log("PaymentController::cancelActiveSubscription()\n" . $exception->getMessage());
            return back()->with('error', 'خطایی وجود دارد. لطفا بعدا تلاش کنید.');
        }
    }




    public static function getSubscriptionDaysLeft()
    {
        $userId = Auth::user()->id;
        // Get current active subscription
        $activeSub = SubscriptionsModel::where([['status', '=', 'active'], ['user_id', '=', $userId]])->orWhere([['status', '=', 'trialing'], ['user_id', '=', $userId]])->first();
        if ($activeSub != null) {
            $paid_with = $activeSub->paid_with;
            if ($paid_with == 'stripe') {
                return StripeController::getSubscriptionDaysLeft();
            }
            if ($paid_with == 'paypal') {
                return PaypalController::getSubscriptionDaysLeft();
            }
            if ($paid_with == 'twocheckout') {
                return TwoCheckoutController::getSubscriptionDaysLeft();
            }
            if ($paid_with == 'paystack') {
                return PaystackController::getSubscriptionDaysLeft();
            }
            if ($paid_with == 'iyzico') {
                return IyzicoController::getSubscriptionDaysLeft();
            }
        } else {
            $activeSub = YokassaSubscriptionsModel::where([['subscription_status', '=', 'active'], ['user_id', '=', $userId]])->first();
            if ($activeSub == null) {
                return null;
            } else return YokassaController::getSubscriptionDaysLeft();
        }
    }

    public static function getSubscriptionRenewDate()
    {
        $userId = Auth::user()->id;
        // Get current active subscription
        $activeSub = SubscriptionsModel::where([['status', '=', 'active'], ['user_id', '=', $userId]])->orWhere([['status', '=', 'trialing'], ['user_id', '=', $userId]])->first();
        if ($activeSub != null) {
            $paid_with = $activeSub->paid_with;
            if ($paid_with == 'stripe') {
                return StripeController::getSubscriptionRenewDate();
            }
            if ($paid_with == 'paypal') {
                return PaypalController::getSubscriptionRenewDate();
            }
            if ($paid_with == 'yokassa') {
                return YokassaController::getSubscriptionRenewDate();
            }
            if ($paid_with == 'paystack') {
                return PaystackController::getSubscriptionRenewDate();
            }
            if ($paid_with == 'iyzico') {
                return IyzicoController::getSubscriptionRenewDate();
            }
        } else {
            return null;
        }
    }

    public static function getSubscriptionStatus()
    {
        $userId = Auth::user()->id;
        // Get current active subscription
        $activeSub = SubscriptionsModel::where([['status', '=', 'active'], ['user_id', '=', $userId]])->orWhere([['status', '=', 'trialing'], ['user_id', '=', $userId]])->first();
        if ($activeSub != null) {
            switch ($activeSub->paid_with) {
                case 'stripe':
                    return StripeController::getSubscriptionStatus();
                    break;

                case 'paypal':
                    return PaypalController::getSubscriptionStatus();
                    break;

                case 'twocheckout':
                    return TwoCheckoutController::getSubscriptionStatus();
                    break;

                case 'paystack':
                    return PaystackController::getSubscriptionStatus();
                    break;

                case 'iyzico':
                    return IyzicoController::getSubscriptionStatus();
                    break;

                default:
                    return false;
                    break;
            }
        } else {
            $activeSub = YokassaSubscriptionsModel::where([['subscription_status', '=', 'active'], ['user_id', '=', $userId]])->first();
            if ($activeSub != null) return true;
            else return false;
        }
    }

    public static function checkIfTrial()
    {
        $userId = Auth::user()->id;
        // Get current active subscription
        $activeSub = SubscriptionsModel::where([['status', '=', 'active'], ['user_id', '=', $userId]])->orWhere([['status', '=', 'trialing'], ['user_id', '=', $userId]])->first();
        if ($activeSub != null) {
            switch ($activeSub->paid_with) {
                case 'stripe':
                    return StripeController::checkIfTrial();
                    break;

                case 'paypal':
                    return PaypalController::checkIfTrial();
                    break;

                case 'yokassa':
                    return YokassaController::checkIfTrial();
                    break;

                case 'paystack':
                    return PaystackController::checkIfTrial();
                    break;

                case 'iyzico':
                    return IyzicoController::checkIfTrial();
                    break;

                default:
                    return false;
                    break;
            }
        } else {
            return false;
        }
    }

    public static function deletePaymentPlan($id)
    {

        // Get plan
        $plan = PaymentPlans::where('id', $id)->first();
        if ($plan != null) {
            $planId = $plan->id;

            // Get related subscriptions
            $queryAnd = [['status', '=', 'active'], ['plan_id', '=', $planId]];
            $queryOr = [['status', '=', 'trialing'], ['plan_id', '=', $planId]];
            $subscriptions = SubscriptionsModel::where($queryAnd)->orWhere($queryOr)->get();

            // Remove subcriptions one by one
            if ($subscriptions != null) {
                foreach ($subscriptions as $subscription) {
                    $subsId = $subscription->id;
                    switch ($subscription->paid_with) {
                        case 'stripe':
                            $tmp = StripeController::cancelSubscribedPlan($planId, $subsId);
                            break;

                        case 'paypal':
                            $tmp = PaypalController::cancelSubscribedPlan($planId, $subsId);
                            break;

                        case 'yokassa':
                            $tmp = YokassaController::cancelSubscribedPlan($planId, $subsId);
                            break;

                        case 'twocheckout':
                            $tmp = TwoCheckoutController::cancelSubscribedPlan($planId, $subsId);
                            break;

                        case 'iyzico':
                            $tmp = IyzicoController::cancelSubscribedPlan($planId, $subsId);
                            break;
                    }
                }
            }

            // Delete Plan
            $plan->delete();
            return back()->with(['message' => 'تمام اشتراک های مربوط به این طرح لغو شده است. طرح حذف شد.', 'type' => 'success']);
        } else {
            return back()->with(['message' => 'طرح پیدا نشد.', 'type' => 'error']);
        }

    }


}
