<?php

namespace App\Http\Controllers\Gateways;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\Currency;
use App\Models\GatewayProducts;
use App\Models\Gateways;
use App\Models\OldGatewayProducts;
use App\Models\PaymentPlans;
use App\Models\Setting;
use App\Models\SubscriptionItems;
use App\Models\Subscriptions as SubscriptionsModel;
use App\Models\UserOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;

class ZibalController extends Controller
{
    public static function payment($planId, $plan)
    {
        $gateway = 'zibal';
        $amount = (int)str_replace(',', '', $plan->price);
        $coupon_15 = Coupon::query()->where('code','black-15')->where('status',1)->first();
        $coupon_50 = Coupon::query()->where('code','black-50')->where('status',1)->first();
        if ($coupon_15 && $coupon_50){
            if ($plan->name === 'دیزاینر پک')
                $amount = $amount - ($amount * $coupon_50->discount / 100);
            elseif ($plan->name !== 'بیسیک')
                $amount = $amount - ($amount * $coupon_15->discount / 100);
        }



        try {
            $invoice = (new Invoice)->amount($amount);
            return Payment::via($gateway)->purchase($invoice, function ($driver, $transactionId) use ($invoice, $amount, $gateway, $planId) {
                $payment = new UserOrder();
                $payment->order_id = (string)$transactionId;
                $payment->plan_id = $planId;
                $payment->user_id = auth()->user()->id;
                $payment->payment_type = 'Zibal';
                $payment->price = $amount;
                $payment->status = 'Waiting';
                $payment->country = $user->country ?? 'Unknown';
                $payment->save();
            })->pay()->render();
        } catch (\Exception $exception) {
            error_log("ZibalController::payment()\n" . $exception->getMessage());
            return redirect()->back()->with('error',  'در اتصال به درگاه خطایی رخ داده است.');
        }
    }

    public function paymentSuccess(Request $request)
    {
        if ($request->has('success') && !$request->success) {
            return redirect()->route('dashboard.user.payment.subscription')->with(
                'error' , 'پرداخت شما موفقیت آمیز نبود. در صورتی که پول از حساب شما کسر شده و تا 48 ساعت آینده به حساب شما بازنگشت، با پشتیبانی تماس بگیرید.',
            );
        } else {
            $order_id = ($request->has('success') && $request->success) ? $request->trackId : null;
            $user_order = UserOrder::query()->where('order_id', $order_id)->firstOrFail();
            $plan = PaymentPlans::query()->where('id', $user_order->plan_id)->first();
            $user = Auth::user();
            try {
                $settings = Setting::query()->first();
                $receipt = Payment::via('zibal')->amount((int)$user_order->price)->transactionId($order_id)->verify();
                $subscription = new SubscriptionsModel();
                $subscription->user_id = $user->id;
                $subscription->name = $plan->name;
                $subscription->order_id = $user_order->id;
                $subscription->status = 'active';
                $subscription->quantity = 1;
                $subscription->plan_id = $plan->id;
                $subscription->save();

//                $previousRequest = app('request')->create(url()->previous());
//                if ($previousRequest->has('coupon')) {
//                    $coupon = Coupon::where('code', $previousRequest->input('coupon'))->first();
//                    if ($coupon) {
//                        $coupon->usersUsed()->attach($user->id);
//                    }
//                }

                $user_order->status = 'Success';
                if ($user->affiliate_id){
                    $affiliate_earnings=($plan->price * $settings->affiliate_commission_percentage) / 100;
                    $user_order->affiliate_earnings = $affiliate_earnings;
                    $user->affiliateOf->affiliate_earnings=$affiliate_earnings;
                    $user->affiliateOf->save();
                }
                $user_order->save();

                $plan->total_words == -1 ? ($user->remaining_words = -1) : ($user->remaining_words = $plan->total_words);
                $plan->total_images == -1 ? ($user->remaining_images = -1) : ($user->remaining_images = $plan->total_images);
                $plan->total_videos == -1 ? ($user->remaining_videos = -1) : ($user->remaining_videos = $plan->total_videos);
                $plan->total_product_back == -1 ? ($user->remaining_product_back = -1) : ($user->remaining_product_back = $plan->total_product_back);

                $user->save();

                createActivity($user->id, __('Subscribed'), $plan->name . ' ' . __('Plan'), null);

                return redirect()->route('dashboard.user.payment.subscription')->with('success' , 'پرداخت شما موفقیت آمیز بود.');

            } catch (\Exception $exception) {
                dd($exception);
                $msg = "zibal::paymentSuccess(): Could not find required payment order!";
                error_log($msg);
                return redirect()->route('dashboard.user.payment.subscription')->with([
                    'message' => 'پرداخت شما موفقیت آمیز نبود. در صورتی که پول از حساب شما کسر شده و تا 48 ساعت آینده به حساب شما بازنگشت، با پشتیبانی تماس بگیرید.',
                ]);
            }
        }
    }

    public static function subscribeCancel(): \Illuminate\Http\JsonResponse
    {
        $user = Auth::user();
        $userId = $user->id;
        try {
            // Get current active subscription
            $activeSub = SubscriptionsModel::query()->where([['status', '=', 'active'], ['user_id', '=', $userId]])->orWhere([['status', '=', 'trialing'], ['user_id', '=', $userId]])->first();

            if ($activeSub != null) {
                $plan = PaymentPlans::query()->where('id', $activeSub->plan_id)->first();

                $activeSub->status = "cancelled";
                $activeSub->ends_at = \Carbon\Carbon::now();
                $activeSub->save();

                $recent_words = $user->remaining_words - $plan->total_words;
                $recent_images = $user->remaining_images - $plan->total_images;
                $recent_videos = $user->remaining_videos - $plan->total_videos;
                $recent_product_back = $user->remaining_product_back - $plan->total_product_back;


                $user->remaining_words = $recent_words < 0 ? 0 : $recent_words;
                $user->remaining_images = $recent_images < 0 ? 0 : $recent_images;
                $user->remaining_videos = $recent_videos < 0 ? 0 : $recent_videos;
                $user->remaining_product_back = $recent_product_back < 0 ? 0 : $recent_product_back;
                $user->save();

                createActivity($user->id, 'Cancelled', 'Subscription plan', null);

                return response()->json(['status'=>200,'message'=>'اشتراک شما با موفقیت لغو شد.','reload'=>true]);
            }

            return response()->json(['status'=>404,'message'=>'اشتراک فعال پیدا نشد.']);
        }catch (\Exception $exception){
            error_log("ZibalController::subscribeCancel()\n" . $exception->getMessage());
            return response()->json(['status'=>500,'message'=>'خطایی رخ داده است. لطفا بعدا دوباره امتحان کنید.']);
        }

    }

}
