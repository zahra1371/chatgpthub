<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\Classes\Helper;
use App\Http\Controllers\Controller;
use App\Jobs\SendInviteEmail;
use App\Models\Bank;
use App\Models\Country;
use App\Models\Coupon;
use App\Models\Folders;
use App\Models\Gateways;
use App\Models\Setting;
use App\Models\UserDocsFavorite;
use App\Models\Integration\Integration;
use App\Models\OpenAIGenerator;
use App\Models\OpenaiGeneratorChatCategory;
use App\Models\OpenaiGeneratorFilter;
use App\Models\PaymentPlans;
use App\Models\SettingTwo;
use App\Models\User;
use App\Models\UserAffiliate;
use App\Models\UserFavorite;
use App\Models\UserOpenai;
use App\Models\UserOpenaiChat;
use App\Models\UserOrder;
use App\Models\Voice\ElevenlabVoice;
use App\Services\GatewaySelector;
use enshrined\svgSanitize\Sanitizer;
use Http\Client\Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Password;
use Illuminate\View\View;
use Illuminate\Support\Facades\Response as Download;

class UserController extends Controller
{
    public function redirect(Request $request): RedirectResponse
    {
        $route = 'dashboard.user.index';

        if ($request->user()->isAdmin()) {
            $route = 'dashboard.admin.index';
        }

        return to_route($route);
    }

    public function index(): View
    {
        $documents = Auth::user()->openai()->whereNotIn('response', ['VIDEO', 'DE'])->with('generator')->orderBy('created_at', 'desc')->take(4)->get();
        return view('panel.user.dashboard', compact('documents'));
    }

    public function changeFirstLogin()
    {
        try {
            \auth()->user()->first_login = 1;
            \auth()->user()->save();
            return response()->json(['status' => 200]);
        } catch (Exception $exception) {
            return response()->json(['status' => 500, 'errors' => $exception]);
        }
    }

    public function openAIList(): View
    {
        abort_if(Helper::setting('feature_ai_writer') == 0, 404);

        $filters = OpenaiGeneratorFilter::query()->where(function ($query) {
            $query->where('user_id', auth()->user()->id)
                ->orWhereNull('user_id');
        })->orderBy('priority', 'Asc')->get();

        return view('panel.user.openai.list', compact('filters'));
    }

    public function openAIGeneratorWorkbook($slug)
    {
        try {
            $templates = OpenAIGenerator::query()->where('filters', $slug)->where('active', 1)->get();
            $first_template = OpenAIGenerator::query()->where('filters', $slug)->first();
            $languages = Country::query()->get();

            $userOpenai = '';
            if ($slug == 'ai_vision' || $slug == 'ai_pdf' || $slug == 'ai_chat_image') {

                $userId = Auth::user()->id;


                $category = OpenaiGeneratorChatCategory::whereSlug($slug)->firstOrFail();


                $list = UserOpenaiChat::where('user_id', Auth::id())->where('openai_chat_category_id', $category->id)->orderBy('updated_at', 'desc');
                $list = $list->get();
                $chat = $list->first();
                $aiList = OpenaiGeneratorChatCategory::all();
                $streamUrl = route('dashboard.user.openai.chat.stream');
                $lastThreeMessage = null;
                $chat_completions = null;
                if ($chat != null) {
                    $lastThreeMessageQuery = $chat->messages()->whereNot('input', null)->orderBy('created_at', 'desc')->take(2);
                    $lastThreeMessage = $lastThreeMessageQuery->get()->reverse();
                    $category = OpenaiGeneratorChatCategory::where('id', $chat->openai_chat_category_id)->first();
                    $chat_completions = str_replace(["\r", "\n"], '', $category->chat_completions) ?? null;

                    if ($chat_completions != null) {
                        $chat_completions = json_decode($chat_completions, true);
                    }
                }
            }

            if ($slug === 'graphics')
                $view = 'panel.user.openai.graphics.generate';
            else
                $view = 'panel.user.openai.generator_workbook';

            if ($first_template->slug === 'ai_speech_to_text') {
                $userOpenai = UserOpenai::query()->where('user_id', Auth::id())->where('openai_id', $first_template->id)->orderBy('created_at', 'desc')->get();
            }

            return view($view, compact('templates', 'first_template', 'languages', 'userOpenai'));
        } catch (\Exception $exception) {

        }
    }

    public function openAIFavoritesList()
    {
        return view('panel.user.openai.list_favorites');
    }

    // docsFavorite
    public function docsFavorite(Request $request)
    {
        $exists = isFavoritedDoc($request->id);
        if ($exists) {
            $favorite = UserDocsFavorite::where('user_openai_id', $request->id)->where('user_id', Auth::id())->firstOrFail();
            $favorite->delete();
            $action = 'unfavorite';
        } else {
            $favorite = new UserDocsFavorite();
            $favorite->user_id = Auth::id();
            $favorite->user_openai_id = $request->id;
            $favorite->save();
            $action = 'favorite';
        }

        return response()->json(['action' => $action]);
    }

    public function openAIFavorite(Request $request)
    {
        $exists = isFavorited($request->id);
        if ($exists) {
            $favorite = UserFavorite::where('openai_id', $request->id)->where('user_id', Auth::id())->firstOrFail();
            $favorite->delete();
            $action = 'unfavorite';
        } else {
            $favorite = new UserFavorite();
            $favorite->user_id = Auth::id();
            $favorite->openai_id = $request->id;
            $favorite->save();
            $action = 'favorite';
        }

        return response()->json(['action' => $action]);
    }

    public function openAIGenerator(Request $request, $slug)
    {
        $openai = OpenAIGenerator::whereSlug($slug)->firstOrFail();

        $userOpenai = $this->openai($request, null)
            ->where('openai_id', $openai->id)
            ->orderBy('created_at', 'desc')
            ->paginate(5);

        $elevenlabs = ElevenlabVoice::query()
            ->select('voice_id', 'name')
            ->where('status', 1)
            ->where('user_id', Auth::id())
            ->whereNotNull('voice_id')
            ->get();

        return view(
            'panel.user.openai.generator',
            compact('openai', 'userOpenai', 'elevenlabs')
        );
    }

    public function openAIGeneratorWorkbookSave(Request $request)
    {
        $workbook = UserOpenai::query()->where('slug', $request->workbook_slug)->where('user_id', auth()->user()->id)->firstOrFail();
        $workbook->output = $request->workbook_text;
        $workbook->title = $request->workbook_title;
        $workbook->save();

        return response()->json([], 200);
    }

    //Chat
    public function openAIChat()
    {
        $chat = Auth::user()->openaiChat;

        return view('panel.user.openai.chat', compact('chat'));
    }

    public static function sanitizeSVG($uploadedSVG)
    {

        $sanitizer = new Sanitizer();
        $content = file_get_contents($uploadedSVG);
        $cleanedData = $sanitizer->sanitize($content);
        $added = file_put_contents($uploadedSVG, $cleanedData);

        return $uploadedSVG;
    }

    //Profile user settings
    public function userSettings(Request $request)
    {
        $user = Auth::user();
        $orders = $user->orders()->with('plan')->paginate(10);
        $affiliates = $user->affiliates()->with('affiliateOf')->paginate(10);
        $countries = Country::query()->get();
        if ($request->ajax()) {
            return response()->json([
                'orders' => $orders,
                'links' => (string)$orders->links('pagination::tailwind')
            ]);
        }
        return view('panel.user.settings.index', compact('user', 'countries', 'orders', 'affiliates'));
    }

    public function userSettingsSave(Request $request)
    {
        try {
            $user = Auth::user();
            if ($request->type && $request->type === 'bank') {
                $validation = Validator::make($request->all(), [
                    'bank_name' => ['required', 'string', 'max:255'],
                    'card_number' => ['required', 'regex:/[2569]{1}[\d]{15}/'],
                    'account_number' => ['required', 'string', 'max:255'],
                    'sheba_number' => ['required', 'regex:/^(?=.{24}$)[0-9]*$/', 'unique:' . Bank::class],
                ]);

                if ($validation->fails())
                    return response()->json(['status' => 422, 'errors' => $validation->errors()]);

                \auth()->user()->bank()->create($request->all());
                createActivity($user->id, 'Create', 'Bank Information', null);
            }else{
                $user->name = $request->name;
                $user->surname = $request->surname;
                $user->phone = $request->phone;
                $user->country = $request->country;

                if ($request->hasFile('avatar')) {
                    $path = 'upload/images/avatar/';
                    $image = $request->file('avatar');

                    if ($image->getClientOriginalExtension() == 'svg') {
                        $image = self::sanitizeSVG($request->file('avatar'));
                    }

                    $image_name = Str::random(4) . '-' . Str::slug($user->fullName()) . '-avatar.' . $image->getClientOriginalExtension();

                    //Image extension check
                    $imageTypes = ['jpg', 'jpeg', 'png', 'svg', 'webp'];
                    if (!in_array(Str::lower($image->getClientOriginalExtension()), $imageTypes)) {
                        $data = [
                            'errors' => ['The file extension must be jpg, jpeg, png, webp or svg.'],
                        ];

                        return response()->json($data, 419);
                    }

                    $image->move($path, $image_name);

                    $user->avatar = $path . $image_name;
                }

                createActivity($user->id, 'Updated', 'Profile Information', null);
                $user->save();
            }
            return response()->json(['status' => 200, 'message' => 'با موفقیت انجام شد.', 'reload' => true]);

        } catch (\Exception $exception) {
            return response()->json('خطای سرور', 419);
        }
    }

    //Purchase
    public function subscriptionPlans()
    {

        //check if any payment gateway enabled
        $activeGateways = Gateways::query()->where('is_active', 1)->get();
        if ($activeGateways->count() > 0) {
            $is_active_gateway = 1;
        } else {
            $is_active_gateway = 0;
        }

        //check if any subscription is active
        $userId = Auth::user()->id;

        $activeSub = getCurrentActiveSubscription($userId);
        if ($activeSub != null) {
            $activesubid = $activeSub->plan_id;
        } else {
            $activesubid = null;
        }

        $openAiList = OpenAIGenerator::query()->get();
        $plans = PaymentPlans::query()->where('type', 'subscription')->where('active', 1)->orderBy('created_at','asc')->get();

        $coupon_15=Coupon::query()->where('code','black-15')->where('status',1)->first();
        $coupon_50=Coupon::query()->where('code','black-50')->where('status',1)->first();

        $view = 'panel.user.finance.subscriptionPlans';
        return view($view, compact('plans', 'openAiList', 'is_active_gateway', 'activeGateways', 'activesubid','coupon_15','coupon_50'));
    }

    //Invoice - Billing
    public function invoiceList()
    {
        $user = Auth::user();
        $list = $user->orders;

        return view('panel.user.orders.index', compact('list'));
    }

    public function invoiceSingle($order_id)
    {
        $invoice = UserOrder::where('order_id', $order_id)->where('user_id', auth()->user()->id)->firstOrFail();

        return view('panel.user.orders.invoice', compact('invoice'));
    }

    public function documentsAll(Request $request, $folderID = null)
    {
        $DOCS_PER_PAGE = 20;

        $listOnly = $request->listOnly;
        $filter = $request->filter ?? 'all';
        $sort = $request->sort ?? 'created_at';
        $sortAscDesc = $request->sortAscDesc ?? 'desc';

        $items = $this->openai($request, $folderID)
            ->where('folder_id', $folderID)
            ->orderBy($sort, $sortAscDesc)
            ->with('generator')
            ->paginate(10);

        if ($folderID !== null) {
            $currfolder = Folders::query()
                ->where(function (Builder $query) {
                    $query
                        ->where('created_by', auth()->id())
                        ->orWhere('team_id', auth()->user()->team_id);
                })
                ->findOrFail($folderID);
        } else {
            $currfolder = null;
        }

        if ($request->ajax()) {
            return response()->json([
                'items' => $items,
                'links' => (string)$items->links('pagination::tailwind')
            ]);
        }

        return view('panel.user.openai.documents', compact('items', 'currfolder', 'filter'))->render();

    }

    protected function openai(Request $request, $folderID = null)
    {
        $team = $request->user()->getAttribute('team');

        $myCreatedTeam = $request->user()->getAttribute('myCreatedTeam');

        return UserOpenai::query()
            ->with('generator')
            ->where(function (Builder $query) use ($team, $myCreatedTeam) {
                $query->where('user_id', auth()->id())
                    ->when($team || $myCreatedTeam, function ($query) use ($team, $myCreatedTeam) {
                        if ($team && $team?->is_shared) {
                            $query->orWhere('team_id', $team->id);
                        }
                        if ($myCreatedTeam) {
                            $query->orWhere('team_id', $myCreatedTeam->id);
                        }
                    });
            });
    }

    public function updateFolder(Request $request, $folder)
    {
        $request->validate([
            'newFolderName' => 'required|string|max:255',
        ]);

        $folder = Folders::findOrFail($folder);
        $folder->name = $request->input('newFolderName');
        $folder->save();

        return response()->json(['message' => __('Folder name updated successfully')]);
    }

    public function updateFile(Request $request, $slug)
    {
        $request->validate([
            'newFileName' => 'required|string|max:255',
        ]);

        $file = UserOpenai::where('slug', $slug)->where('user_id', auth()->user()->id)->firstOrFail();
        $file->generator->title = $request->input('newFileName');
        $file->generator->save();

        return response()->json(['message' => __('File name updated successfully')]);
    }

    public function deleteFolder(Request $request, $folder)
    {
        $folder = Folders::findOrFail($folder);
        $all = $request->all;
        if ($all) {
            foreach ($folder->userOpenais as $userOpenai) {
                $userOpenai->delete();
            }
        }
        $folder->delete();

        return response()->json(['message' => __('Folder deleted successfully')]);
    }

    public function newFolder(Request $request)
    {
        $request->validate([
            'newFolderName' => 'required|string|max:255',
        ]);

        $newFolder = new Folders();
        $newFolder->name = $request->newFolderName;
        $newFolder->created_by = auth()->user()->id;
        $newFolder->save();

        return back()->with(['message' => __('Added successfuly'), 'type' => 'success']);
    }

    public function moveToFolder(Request $request)
    {
        $folderID = $request->selectedFolderId;
        $fileSlug = $request->fileslug;

        $workbook = UserOpenai::where('slug', $fileSlug)->where('user_id', auth()->user()->id)->firstOrFail();
        $workbook->folder_id = $folderID;
        $workbook->save();

        return back()->with(['message' => __('Moved successfuly'), 'type' => 'success']);
    }

    public function documentsSingle($slug)
    {
        $workbook = UserOpenai::query()->where('slug', $slug)->where('user_id', auth()->user()->id)->firstOrFail();

        $openai = $workbook->generator;

        $integrations = Auth::user()->getAttribute('integrations');

        $checkIntegration = Integration::query()->whereHas('hasExtension')->count();

        return view('panel.user.openai.documents_workbook', compact('checkIntegration', 'workbook', 'openai', 'integrations'));
    }

    public function documentsDelete($slug)
    {
        $workbook = UserOpenai::query()->where('slug', $slug)->where('user_id', auth()->user()->id)->firstOrFail();
        try {
            if ($workbook->storage == UserOpenai::STORAGE_LOCAL) {
                $file = str_replace('/uploads/', '', $workbook->output);
                Storage::disk('public')->delete($file);
            } elseif ($workbook->storage == UserOpenai::STORAGE_AWS) {
                $file = str_replace('/', '', parse_url($workbook->output)['path']);
                Storage::disk('s3')->delete($file);
            } else {
                // Manual deleting depends on response
                if (str_contains($workbook->output, 'https://')) {
                    // AWS Storage
                    $file = str_replace('/', '', parse_url($workbook->output)['path']);
                    Storage::disk('s3')->delete($file);
                } else {
                    $file = str_replace('/uploads/', '', $workbook->output);
                    Storage::disk('public')->delete($file);
                }
            }
            $basefilename = basename($workbook->output);
            Storage::disk('thumbs')->delete($basefilename);
        } catch (\Throwable $th) {
            //throw $th;
        }
        $workbook->delete();
        return response()->json(['url' => 'dashboard/user/openai/documents/all']);
        return redirect()->route('dashboard.user.openai.documents.all')->with(['message' => __('Document deleted successfuly'), 'type' => 'success']);
    }

    public function documentsImageDelete($slug)
    {
        $workbook = UserOpenai::where('slug', $slug)->where('user_id', auth()->user()->id)->firstOrFail();
        Storage::disk('liara')->delete(str_replace('/hosheman/','',$workbook->output));
        $workbook->delete();
        return back()->with(['message' => __('Deleted successfuly'), 'type' => 'success']);
    }

    public function downloadImage($slug)
    {
        $file=UserOpenai::query()->where('slug',$slug)->first();
        return Storage::disk('liara')->download(str_replace('/hosheman/','',$file->output));

    }

    //Affiliates
    public function affiliatesList()
    {
        abort_if(Helper::setting('feature_affilates') == 0, 404);

        $user = Auth::user();
        $list = $user->affiliates;
        $list2 = $user->withdrawals;
        $totalEarnings = 0;
        foreach ($list as $affOrders) {
            $totalEarnings += $affOrders->orders->sum('affiliate_earnings');
        }
        $totalWithdrawal = 0;
        foreach ($list2 as $affWithdrawal) {
            $totalWithdrawal += $affWithdrawal->amount;
        }

        return view('panel.user.affiliate.index', compact('list', 'list2', 'totalEarnings', 'totalWithdrawal'));
    }

    public function affiliatesUsers(Request $request)
    {
        $userIds = User::where("affiliate_id", auth()->user()->id)->pluck('id');
        $query = UserAffiliate::whereIn("user_id", $userIds)->with('user');

        if ($request->has('search')) {
            $searchTerm = $request->input('search');
            $query->whereHas('user', function ($q) use ($searchTerm) {
                $q->where('name', 'like', '%' . $searchTerm . '%');
            });
        }

        if ($request->has('startDate') && $request->input('startDate')) {
            $query->whereDate('created_at', '>=', $request->input('startDate'));
        }

        if ($request->has('endDate') && $request->input('endDate')) {
            $query->whereDate('created_at', '<=', $request->input('endDate'));
        }

        $list = $query->paginate(10);

        return view('panel.user.affiliate.users', compact('list'));
    }

    public function affiliatesListSendInvitation(Request $request)
    {
        $user = Auth::user();

        $sendTo = $request->to_mail;

        dispatch(new SendInviteEmail($user, $sendTo));

        return response()->json([], 200);
    }

    public function affiliatesListSendRequest(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'amount' => ['required'],
            ]);

            if ($validation->fails())
                return response()->json(['status' => 422, 'errors' => $validation->errors()]);

            if (\auth()->user()->bank === null)
                return response()->json(['status' => 500, 'message' => 'بانکی برات ثبت نشده. اول بانکت رو ثبت کن.']);

            if (\auth()->user()->bank->status == 0)
                return response()->json(['status' => 500, 'message' => 'بانکت هنوز تایید نشده، باید منتظر باشی.']);

            if (\auth()->user()->bank->status == 2)
                return response()->json(['status' => 500, 'message' => 'بانکت رد شده. با پشتیبانی تماس بگیر.']);

            if (\auth()->user()->affiliate_earnings < $request->amount)
                return response()->json(['status' => 422, 'errors' => ['amount' => 'موجودیت کافی نیست.']]);

            $min_withdrawal=200000;
            if ($min_withdrawal > $request->amount)
                return response()->json(['status' => 422, 'errors' => ['amount' => 'درخواست شما کمتر از حداقل مقدار  برداشت است.(حداقل برداشت 200 هزار تومان)']]);

            \auth()->user()->withdrawals()->create([
                'amount'=>$request->amount,
                'user_id'=>\auth()->user()->id,
                'status'=>0,
            ]);

            \auth()->user()->affiliate_earnings =\auth()->user()->affiliate_earnings - $request->amount;
            \auth()->user()->save();

            createActivity(\auth()->user()->id, 'Sent', 'Affiliate Withdraw Request',null);

            return response()->json(['status' => 200, 'message' => 'درخواست برداشتت با موفقیت ثبت شد. منتظر تایید ادمین باش.', 'reload' => true]);

        }catch (Exception $exception){
            dd($exception);
            return response()->json(['status' => 500, 'message' => 'خطایی رخ داده است، لطفا بعدا امتحان کنید.']);
        }
    }

    public function apiKeysList()
    {
        abort_if(!Helper::appIsDemo() && Helper::setting('user_api_option') == 0, 404);
        $user = Auth::user();
        $list = $user->api_keys;

        $anthropic_api_keys = $user->anthropic_api_keys;

        return view('panel.user.apiKeys.index', compact('list', 'anthropic_api_keys'));
    }

    public function apiKeysSave(Request $request)
    {
        if (Helper::appIsDemo()) {
            return back()->with(['message' => __('This feature is disabled in Demo version.'), 'type' => 'error']);
        }
        $user = Auth::user();
        $user->api_keys = $request->api_keys;
        $user->anthropic_api_keys = $request->anthropic_api_keys;
        $user->save();

        return redirect()->back();
    }

    public function getFreeImagesNumber()
    {
        try {
            return response()->json(['status' => 'success', 'freeImages' => \auth()->user()->free_images_count]);
        }catch (\Exception $exception){
            dd($exception);
        }
    }
}
