<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Token;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;

class AuthenticationController extends Controller
{

    public function loginForm(): View
    {
        return view('auth.login', [
            'plan' => request('plan'),
        ]);
    }

    public function login(Request $request)
    {
        if ($request->has('email') && $request->email != null) {
            $validation = Validator::make($request->all(), [
                'mobile' => 'required|unique:users',
            ]);
        } else {
            $validation = Validator::make($request->all(), [
                'mobile' => 'required',
            ]);
        }

        if ($validation->fails())
            return response()->json(['status' => 422, 'errors' => $validation->errors()]);


        try {
            if ($request->email) {
                $user = User::query()->where('email', $request->email)->first();
                if (!$user)
                    return response()->json(['status' => 422, 'errors' => ['email' => 'کاربری با این ایمیل ثبت نام نکرده است.']]);
                else {
                    if ($user->mobile != null && $user->mobile != $request->mobile)
                        return response()->json(['status' => 422, 'errors' => ['email' => 'ایمیل و موبایل نامعتبر هستند.']]);
                    if ($user->mobile == null) {
                        $user->mobile = $request->mobile;
                        $user->save();
                    }
                }
            } else
                $user = User::query()->where('mobile', $request->mobile)->first();

            if (!$user)
                return response()->json(['status' => 422, 'errors' => ['mobile' => 'کاربری با این موبایل ثبت نام نکرده است.']]);

            $token = Token::query()->create([
                'user_id' => $user->id
            ]);
            $rememberMe = (!empty($request->remember_me)) ? TRUE : FALSE;
            if ($token->sendCode()) {
                session()->put("code_id", $token->id);
                session()->put("user_id", $user->id);
                session()->put("remember", $rememberMe);
                return response()->json(['status' => 200, 'message' => 'لطفا کد پیامک شده را وارد کنید.', 'url' => '/verify-mobile-code']);
            }
            $token->delete();
//            درستش کن باید سوئیت الرت بذاری بگی خطای سرور //
            return response()->json(['status' => 200, 'message' => 'لطفا کد پیامک شده را وارد کنید.', 'url' => '/login']);

        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function registerForm(Request $request): View
    {
        $code = $request->code;
        return view('auth.register', compact('code'), [
            'plan' => $request->get('plan')
        ]);
    }

    public function registerStore(Request $request)
    {

        $validation = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'unique:' . User::class],
        ]);

        if ($validation->fails())
            return response()->json(['status' => 422, 'errors' => $validation->errors()]);

        try {
            $affCode = null;
            if ($request->code != null) {
                $affUser = User::query()->where('affiliate_code', $request->code)->first();
                $affCode = $affUser?->id;
            }


            $user = User::query()->create([
                'name' => $request->name,
                'surname' => $request->surname,
                'mobile' => convertPersianNumbers($request->mobile),
                'remaining_words' => 500,
                'remaining_images' => -1,
                'remaining_videos' => 0,
                'remaining_product_back' => 0,
                'affiliate_code' => Str::upper(Str::random(19)),
                'affiliate_id' => $affCode,
            ]);

            $token = Token::query()->create([
                'user_id' => $user->id
            ]);

            if ($token->sendCode()) {
                session()->put("code_id", $token->id);
                session()->put("user_id", $user->id);
                return response()->json(['status' => 200, 'message' => 'لطفا کد پیامک شده را وارد کنید.', 'url' => '/verify-mobile-code']);
            }
            $token->delete();
            return redirect()->route('login.form')->withErrors([
                "Unable to send verification code"
            ]);
        } catch (\Exception $exception) {
            dd($exception);
        }

    }

    public function verifyForm(): View
    {
        $token = Token::query()->where('user_id', session()->get('user_id'))->find(session()->get('code_id'));
        $token_time = $token->isExpired() ? false : Carbon::now()->diffInSeconds($token->created_at);
        $mobile = $token->user->mobile;
        return view('auth.verify_mobile_code', compact('token_time', 'mobile'));
    }

    public function verifyMobileCode(Request $request)
    {
        $digits = array_filter($request->digits, static function ($var) {
            return $var !== null;
        });

        $request->merge(['digits' => $digits]);

        $validation = Validator::make($request->all(), [
            'digits' => 'present|array|size:6'
        ]);

        if ($validation->fails())
            return response()->json(['status' => 422, 'errors' => $validation->errors()]);

        try {
            $code = implode('', $request->digits);

            if (!session()->has('code_id') || !session()->has('user_id'))
                response()->json(['status' => 300, 'url' => 'login']);

            $token = Token::query()->where('user_id', session()->get('user_id'))->find(session()->get('code_id'));

            if (!$token || empty($token->id))
                return response()->json(['status' => 422, 'errors' => ['digits' => 'کد وجود ندارد.']]);

            if (!$token->isValid())
                return response()->json(['status' => 422, 'errors' => ['digits' => 'کد منقضی شده است.']]);

            if ($token->code !== $code)
                return response()->json(['status' => 422, 'errors' => ['digits' => 'کد اشتباه است.']]);

            $token->update([
                'used' => true
            ]);
            $user = User::find(session()->get('user_id'));
            $rememberMe = session()->get('remember');
            auth()->login($user, $rememberMe);
            $request->session()->regenerate();

            $url=session('url.intended')??RouteServiceProvider::HOME;
            return response()->json(['status' => 200, 'message' => 'با موفقیت وارد شدید.', 'url' => $url]);

        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function resendCode()
    {
        try {
            $token = Token::query()->create([
                'user_id' => session()->get('user_id')
            ]);

            if ($token->sendCode()) {
                session()->put("code_id", $token->id);
                session()->put("user_id", session()->get('user_id'));
                return response()->json(['status' => 200, 'message' => 'کد برای شما ارسال شد.', 'reload' => true]);
            }
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'message' => 'خطایی رخ داده است، لطفا بعدا امتحان کنید.']);
        }
    }

    public function logout(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
