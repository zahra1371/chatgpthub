<?php

namespace App\Http\Controllers;

use App\Jobs\SendConfirmationEmail;
use App\Models\Blog;
use App\Models\Clients;
use App\Models\Coupon;
use App\Models\CustomSettings;
use App\Models\Faq;
use App\Models\FrontendForWho;
use App\Models\FrontendFuture;
use App\Models\FrontendGenerators;
use App\Models\FrontendTools;
use App\Models\Hero;
use App\Models\HowitWorks;
use App\Models\OpenAIGenerator;
use App\Models\OpenaiGeneratorFilter;
use App\Models\PaymentPlans;
use App\Models\Setting;
use App\Models\SettingTwo;
use App\Models\FrontendSectionsStatusses;
use App\Models\Testimonials;
use App\Models\User;
use App\Models\UserOpenai;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;


class IndexController extends Controller
{
    public function index()
    {
        $faq = Faq::all();
        $currency = currency()->symbol;
        $coupon_15 = Coupon::query()->where('code', 'black-15')->where('status',1)->first();
        $coupon_50 = Coupon::query()->where('code', 'black-50')->where('status',1)->first();
        return view('index', compact('faq', 'currency', 'coupon_50','coupon_15'));
    }

    public function activate(Request $request)
    {
        $valid = $request->liquid_license_status;
        $liquid_license_domain_key = $request->liquid_license_domain_key;
        if ($valid == 'valid') {
            $client = new Client();

            try {
                $response = $client->request('GET', "https://portal.liquid-themes.com/api/license/" . $liquid_license_domain_key);
            } catch (\Exception $e) {
                return response()->json(["status" => "error", "message" => $e->getMessage()]);
            }

            $settings_two = SettingTwo::first();
            // $setting->status_for_now = 'active';
            $settings_two->liquid_license_domain_key = $liquid_license_domain_key;
            $settings_two->liquid_license_type = json_decode($response->getBody())->licenseType;
            $settings_two->save();
            return redirect()->route('dashboard.index');
        } else {
            echo 'Activation failed!';
        }
    }


    public function howitWorksDefaults()
    {
        $values = json_decode('{"option": TRUE, "html": ""}');
        $default_html = 'Want to see? <a class="text-[#FCA7FF]" href="https://codecanyon.net/item/magicai-openai-content-text-image-chat-code-generator-as-saas/45408109" target="_blank">' . __('Join') . ' Magic</a>';

        //Check display bottom line
        $bottomline = CustomSettings::where('key', 'howitworks_bottomline')->first();
        if ($bottomline != null) {
            $values["option"] = $bottomline->value_int ?? 1;
            $values["html"] = $bottomline->value_html ?? $default_html;
        } else {
            $bottomline = new CustomSettings();
            $bottomline->key = 'howitworks_bottomline';
            $bottomline->title = 'Used in How it Works section bottom line. Controls visibility and HTML value of line.';
            $bottomline->value_int = 1;
            $bottomline->value_html = $default_html;
            $bottomline->save();
            $values["option"] = 1;
            $values["html"] = $default_html;
        }

        return $values;
    }
}
