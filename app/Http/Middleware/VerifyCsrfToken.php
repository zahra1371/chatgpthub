<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    protected $maxSize = 50 * 1024 * 1024; // 50MB in bytes
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    //TODO FOR DEMO
    protected $except = [
        // '*',
        'pdf/getContent',
        'stripe/*',
        'webhooks/*',
        'dashboard/*',
        'dashboard/user/payment/iyzico/*',
    ];

}
