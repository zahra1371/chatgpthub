<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\PaymentPlans;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Laravel\Cashier\Subscription;

class CheckPlanTime extends Command
{
    protected $signature = 'app:check-plan-time-command';

    protected $description = 'Command description';

    public function handle(): void
    {
        $basicPlan=PaymentPlans::query()->where('name','بیسیک')->first();
        $userPlans=Subscription::query()->where('plan_id',$basicPlan->id)->where('status','active')->get();
        foreach ($userPlans as $userPlan){
            if ($userPlan->created_at < Carbon::now()->subDays(30)){
                $user=User::query()->where('id',$userPlan->user_id)->first();
                $user->remaining_words=0;
                $user->remaining_images=0;
                $user->remaining_videos=0;
                $user->save();
                $userPlan->status='finished';
                $userPlan->save();
            }
        }

        $this->info('plan time executed successfully!');;
    }
}
