<?php

namespace App\Console\Commands;

use App\Models\UserOpenai;
use App\Models\OpenAIGenerator;
use Carbon\Carbon;
use Illuminate\Console\Command;


class DeleteImagesAndVideos extends Command
{
    protected $signature = 'app:delete-images-command';

    protected $description = 'Command description';

    public function handle(): void
    {
        $openai=OpenAIGenerator::query()->whereIn('slug', ['ai_image_generator','ai_video'])->pluck('id');
        $userImages=UserOpenai::query()->whereIn('openai_id', $openai)
            ->where('storage','!=','deleted')
            ->where('created_at', '<=', Carbon::now()->subDays(14)->toDateTimeString())->get();
        foreach ($userImages as $userImage){
            $userImage->storage='deleted';
            $userImage->save();
        }

        $this->info('images delete executed successfully!');;
    }
}
