<?php

namespace App\Console\Commands;

use App\Models\UserOrder;
use Carbon\Carbon;
use Illuminate\Console\Command;


class deleteWaitingPayments extends Command
{
    protected $signature = 'app:delete-waiting-payments-command';

    protected $description = 'Command description';

    public function handle(): void
    {
        $payments=UserOrder::where('status','Waiting')
            ->where('created_at', '<=', Carbon::now()->subDays(2)->toDateTimeString())->get();
        foreach ($payments as $payment){
            $payment->delete();
        }

        $this->info('images delete executed successfully!');;
    }
}
