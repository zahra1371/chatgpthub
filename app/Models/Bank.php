<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable=[
        'user_id',
        'bank_name',
        'card_number',
        'account_number',
        'sheba_number',
        'status',
        'reject_reason'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeActive($q)
    {
        return $q->where('status',1);
    }

    // گرفتن متن مرتبط با status
    public function getStatusTextAttribute(): string
    {
        switch ($this->status){
            case 0: //new
                return 'جدید';
            case 1: // confirmed
                return 'تایید شده';
            case 2: //reject
                return 'رد شده';
        }
    }

    // گرفتن رنگ مرتبط با status
    public function getStatusColorAttribute(): string
    {
        switch ($this->status){
            case 0: //new
                return 'primary';
            case 1: // confirmed
                return 'success';
            case 2: //reject
                return 'danger';
        }
    }
}
