<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAffiliate extends Model
{
    use HasFactory;
    protected $table = 'user_affiliates';
    protected $fillable = ['user_id','amount', 'status','tracking_code','reject_reason'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    // گرفتن متن مرتبط با status
    public function getStatusTextAttribute(): string
    {
        switch ($this->status){
            case 0: //new
                return 'در حال بررسی';
            case 1: // confirmed
                return 'تایید شده';
            case 2: //reject
                return 'رد شده';
        }
    }

    public function getStatusColorAttribute(): string
    {
        switch ($this->status){
            case 0: //new
                return 'blue';
            case 1: // confirmed
                return 'emerald';
            case 2: //reject
                return 'rose';
        }
    }
}
