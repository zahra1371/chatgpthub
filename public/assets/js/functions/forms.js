function registerForm(form, button) {
    let data = $(form).serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {})

    let formData = {
        action: form.attr('action'),
        method: form.attr('method'),
        data,
        button: button.attr('id')
    }

    ajax(formData)
}

function loginForm(form, button) {
    let data = $(form).serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {})

    let formData = {
        action: form.attr('action'),
        method: form.attr('method'),
        data,
        button: button.attr('id')
    }

    ajax(formData)
}

function bankForm(form) {
    let data = $(form).serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {})

    let formData = {
        action: form.attr('action'),
        method: form.attr('method'),
        data
    }
    ajax(formData)
}

function ajax(form, func_name = null, params = {}) {
    let action = typeof form === 'object' ? form.action : form.attr('action');
    let method = typeof form === 'object' ? form.method : form.attr('method');
    let data = typeof form === 'object' ? form.data : form.serialize();
    let button = typeof form === 'object' && form.button ? form.button : null; // حتما باید خود باتن پاس داده بشه نه کلاسش دادا


    let button_text = $(`#${button}`).text();
    if (button) {
        $(`#${button}`).removeClass('bg-blue-600')
        $(`#${button}`).addClass('bg-blue-200')
        $(`#${button}`).attr('disabled', true)
        $(`#${button}`).text('درحال ارسال...')
    }

    $('.help-block').remove();
    $('span[class*="error-"]').text('');

    $.ajax({
        url: action,
        type: method,
        data: data,
        success: function (response) {
            // اگه برای برگشت از ajax خواستیم یه تابعی رو اجرا کنیم
            if (func_name != null)
                func_name(response, params);
            // if (response.msg) { //

            if (response.status === 200 || response.status === 100) {
                if (button != null) {
                    $(`#${button}`).removeClass('bg-blue-200')
                    $(`#${button}`).addClass('bg-blue-600')
                    $(`#${button}`).attr('disabled', true)
                    $(`#${button}`).text(button_text)
                }
                Swal.fire({
                    title: "انجام شد 😍",
                    text: response.message,
                    icon: "success",
                    confirmButtonText: "باشه",
                }).then(() => {
                    if (response.reload)
                        location.reload()
                    if (response.url) {
                        console.log(response.url,response.url.includes('http'))
                        if (response.url.includes('http'))
                            window.location.href = response.url
                        else
                            window.location.href = window.location.origin + response.url
                    }
                })
            } else if (response.status === 422) {
                if (button != null) {
                    $(`#${button}`).removeClass('bg-blue-200')
                    $(`#${button}`).addClass('bg-blue-600')
                    $(`#${button}`).attr('disabled', false)
                    $(`#${button}`).text(button_text)
                }
                showValidationErrors(response.errors)
            } else if (response.status == 500) {
                if (button != null) {
                    $(`#${button}`).removeClass('bg-blue-200')
                    $(`#${button}`).addClass('bg-blue-600')
                    $(`#${button}`).attr('disabled', false)
                    $(`#${button}`).text(button_text)
                }
                toastr.error(response.message);
            } else if (response.status === 300)
                window.location.href = window.location.origin + response.url
            // }
        },
        error: function (xhr) {
            if (button != null) {
                if (button) {
                    $(`#${button}`).removeClass('bg-blue-200')
                    $(`#${button}`).addClass('bg-blue-600')
                    $(`#${button}`).attr('disabled', false)
                    $(`#${button}`).text(button_text)
                }
            }
            if (func_name != null)
                func_name(xhr, params);
            Swal.fire({
                title: "خطا ☹️",
                text: 'متاسفم! خطایی در سرور بوجود اومده، لطفا بعدا امتحان کن.',
                icon: "error",
                confirmButtonText: "باشه",
            }).then(() => {
                location.reload()
            })
        }
    });
}

function showValidationErrors(errors) {
    $('.error').html('')
    Object.keys(errors).forEach((key, index) => {
        $(`.error-${key}`).text(errors[key])
    });
}
