$(document).ready(function () {
    $(document).keyup(function (e) {
        if (e.key === "Escape") {
            $('.modal').addClass('hidden');
        }
    });
});

document.addEventListener('DOMContentLoaded', (event) => {
    // Function to open a modal
    function openModal(modalId) {
        document.getElementById(modalId).classList.remove('hidden');
    }

    // Function to close a modal
    function closeModal(modalId) {
        document.getElementById(modalId).classList.add('hidden');
    }

    // Event listener for open modal buttons
    document.querySelectorAll('.open-modal-btn').forEach(button => {
        button.addEventListener('click', (event) => {
            const modalId = event.currentTarget.getAttribute('data-modal');
            openModal(modalId);
        });
    });

    // Event listener for close modal buttons
    document.querySelectorAll('.modal .close-modal-btn').forEach(button => {
        button.addEventListener('click', (event) => {
            const modal = event.currentTarget.closest('.modal');
            closeModal(modal.id);
        });
    });

    // Event listener for clicking outside the modal content
    document.querySelectorAll('.modal').forEach(modal => {
        modal.addEventListener('click', (event) => {
            if (event.target.classList.contains('modal-container')) {
                closeModal(modal.id);
            }
        });
    });
});

function copyText(text, index) {
    let isiOSDevice = navigator.userAgent.match(/ipad|ipod|iphone/i)
    if (isiOSDevice) {
        let input1=$('<input>').attr({
            type: 'hidden',
            value:text,
        }).appendTo('.copy')

        let input=input1[0]
        let editable = input.contentEditable
        let readOnly = input.readOnly

        input.contentEditable = true
        input.readOnly = false

        let range = document.createRange()
        range.selectNodeContents(input)

        let selection = window.getSelection()
        selection.removeAllRanges()
        selection.addRange(range)

        input.setSelectionRange(0, 999999)
        input.contentEditable = editable
        input.readOnly = readOnly
        document.execCommand('copy')
        copyToClipBoard(`tooltiptop-${index}`)
    } else {
        let textField = document.createElement('textarea')
        textField.innerText = text
        document.body.appendChild(textField)
        textField.select()
        textField.focus()
        document.execCommand('copy')
        textField.remove()
        $(index).text('کپی شد!')

        setTimeout(function () {
            $(index).text('کپی لینک')
        },1500)

    }
}


