function getTranslation(word) {
    switch (word){
        case 'Post Title Generator':
            return "تولید کننده عنوان پست"
        case 'Summarize Text':
            return "خلاصه نویس"
        case 'Product Description':
            return "توضیحات محصول"
        case 'Article Generator':
            return "مقاله نویس"
        case 'Product Name Generator':
            return "تولید کننده نام محصول"
        case 'Testimonial Review':
            return "توصیه نامه"
        case 'Problem Agitate Solution':
            return "حل مشکل"
        case 'Blog Section':
            return "وبلاگ نویس"
        case 'Blog Post Ideas':
            return "ایده وبلاگ"
        case 'Blog Intros':
            return "مقدمه وبلاگ"
        case 'Blog Conclusion':
            return "نتیجه وبلاگ"
        case 'Facebook Ads':
            return "تبلیغات فیسبوک"
        case 'Youtube Video Description':
            return "کپشن ویدئوی یوتیوب"
        case 'Youtube Video Title':
            return "عنوان برای یوتیوب"
        case 'Youtube Video Tag':
            return "تگ برای یوتیوب"
        case 'Instagram Captions':
            return "کپشن اینستاگرام"
        case 'Instagram Hashtags':
            return "هشتگ اینستاگرام"
        case 'Social Media Post Tweet':
            return "توییت پست رسانه های اجتماعی"
        case 'Social Media Post Business':
            return "متن رسانه های اجتماعی"
        case 'Facebook Headlines':
            return "تولید کننده عنوان پست"
        case 'Google Ads Headlines':
            return "تولید کننده عنوان پست"
        case 'Google Ads Description':
            return "تولید کننده عنوان پست"
        case 'Paragraph Generator':
            return "تولیدکننده پاراگراف"
        case 'Pros & Cons':
            return "جوانب مثبت و منفی"
        case 'Meta Description':
            return "توضیحات متا"
        case 'FAQ Generator (All Datas)':
            return "ساخت سوالات متداو"
        case 'Email Generator':
            return "ساخت متن ایمیل"
        case 'Email Answer Generator':
            return "ساخت پاسخ ایمیل"
        case 'Newsletter Generator':
            return "تولید کننده خبرنامه"
        case 'Grammar Correction':
            return "تصحیح گرامر"
        case 'TL;DR Summarization':
            return "خلاصه نویس حرفه ای"
        case 'AI Image Generator':
            return "ساخت عکس"
        case 'File Analyzer':
            return "تجزیه و تحلیل فایل"
        case 'Custom Generation':
            return "تولید کننده عنوان پست"
        case 'AI Speech to Text':
            return "تبدیل صدا به متن"
        case 'AI Web Chat':
            return "چت وب"
        case 'AI Video':
            return "ساخت ویدئو"
        case 'AI Code Generator':
            return "کدنویس"
        case 'AI Article Wizard Generator':
            return "مقاله نویس حرفه ای"
        case 'AI Vision':
            return "هوش مصنوعی دیداری"
        case 'AI Voiceover':
            return "صداگذاری"
        case 'AI YouTube':
            return "یوتیوبر"
        case 'Instagram Story':
            return "استوری اینستاگرام"
        case 'AI RSS':
            return "AI RSS"
        case 'Instagram Reel':
            return "ریلز اینستاگرام"
        case 'Chat Image':
            return "چت با عکس"
        case 'AI ReWriter':
            return "تربات بازنویس"
    }
}
