const generateBtn = document.getElementById("send_message_button");
const stopBtn = document.getElementById("stop_button");
const promptInput = document.getElementById("prompt");
let controller = null; // Store the AbortController instance
const streamUrl = $('meta[name=stream-url]').attr('content');
const stream_type = 'backend';


const generate = async (message_no, creativity, maximum_length, prompt, post_type) => {
    "use strict";
    const typingEl = document.querySelector('.tox-edit-area > .lqd-typing');

    const chunk = [];
    let streaming = true;
    var result = '';

    const nIntervId = setInterval(function () {
        if (chunk.length == 0 && !streaming) {
            $('.generator_button').text('بزن بریم');
            $('.generator_button').attr('disabled', false)
            document.querySelector('#app-loading-indicator')?.classList?.add('opacity-0');
            document.querySelector('#workbook_regenerate')?.classList?.remove('hidden');
            saveResponse(prompt, result, message_no)
            clearInterval(nIntervId);
        }

        const text = chunk.shift();
        if (text) {
            result += text;
            tinyMCE.get(`${post_type}-editor`).setContent(result, {format: 'raw'});
            typingEl?.classList?.add('lqd-is-hidden');
        }
    }, 20);

    if (stream_type === 'backend') {
        try{
            const eventSource = new EventSource(`${streamUrl}?message=${prompt}`);
            eventSource.addEventListener('data', function (event) {
                $('#content-area').addClass('hidden')
                if (tinymce.get(`${post_type}-editor`))
                    tinymce.get(`${post_type}-editor`).show()

                tinymce.init({
                    selector: '.default',
                    directionality: 'rtl',
                    promotion: false,
                    menubar: false,
                    branding: false,
                });
                const data = JSON.parse(event.data);
                if (data.message !== null) {
                    chunk.push(data.message.replace(/(?:\r\n|\r|\n)/g, ' <br> '));
                }
            });

            // finished eventSource
            eventSource.addEventListener('stop', function (event) {
                streaming = false;
                eventSource.close();
            });
        }catch (e) {
            console.log('eror is here',e)
        }
    }
};

function saveResponse(input, response, message_no) {
    "use strict";
    var formData = new FormData();
    formData.append('input', input);
    formData.append('response', response);
    formData.append('message_id', message_no);
    jQuery.ajax({
        url: '/dashboard/user/openai/low/generate_save',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
    });
    return false;
}

function calculateWords(sentence) {

    // Count words in the sentence
    let wordCount = 0;

    if (/^[\u4E00-\u9FFF]+$/.test(sentence)) {
        // For Chinese, count the number of characters as words
        wordCount = sentence.length;
    } else {
        // For other languages, split the sentence by word boundaries using regular expressions
        const words = sentence.split(/\b\w+\b/);
        wordCount = words.length;
    }

    return wordCount;
}
